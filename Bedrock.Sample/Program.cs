﻿using System;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Server;
using Bedrock.Shared;
using Bedrock.Shared.Storage;
//using Bedrock.Windows;

using IoT.Common;

using Utilities.IO;
using Utilities.Remoting;
using Utilities.Remoting.Tcp;

namespace Bedrock.Sample
{
    using Server = Server.Server;

    //using global::IoT.Common;
    //using IoT;
    using Python;
    using Python = Python.Python;

    using Storage = Bedrock.Shared.Storage.Storage;
    using Bedrock.IoT;

    class Lion : MarshalByRefObject
    {
        public int Value
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
            }
        }

        private int val = 1234;

        public event EventHandler<string> Event
        {
            add
            {
                Console.WriteLine("Lion.Event += delegate");
                _event += value;
            }
            remove
            {
                Console.WriteLine("Lion.Event -= delegate");
                _event -= value;
            }
        }
        private event EventHandler<string> _event;

        public Lion()
        {
            Console.WriteLine("Lion.ctor()");
        }

        public void Method()
        {
            Console.WriteLine("Lion.Method()");
        }
        public void Method(int value)
        {
            Console.WriteLine("Lion.Method({0})", value);
        }
        public void Method(float value)
        {
            Console.WriteLine("Lion.Method({0})", value);
        }
        public void Method(byte[] buffer)
        {
            Console.WriteLine("Lion.Method(byte[{0}])", buffer.Length);

            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = (byte)((buffer[i] * 2) % 256);
        }
        public void Method(string value, string optional = "dede")
        {
            Console.WriteLine("Lion.Method(\"{0}\", \"{1}\")", value, optional);
        }
        public void Method<T>(T value)
        {
            Console.WriteLine("Lion.Method({0})", value);
        }

        public int Function(int value)
        {
            return value * 2;
        }
        public float Function(float value)
        {
            return value / 2;
        }
        public T Function<T>(T value)
        {
            return value;
        }

        public void Throw()
        {
            throw new Exception();
        }
        public void Throw(string message)
        {
            throw new Exception(message);
        }
        public void Throw<T>(string message) where T : Exception
        {
            throw Activator.CreateInstance(typeof(T), new object[] { message }) as T;
        }

        public void TriggerEvent(string data)
        {
            Console.WriteLine($"Lion.TriggerEvent({data})");
            _event?.Invoke(this, data);
        }

        public override string ToString() => $"My val is {val}";
    }

    class Program
    {
        public static int Square(int x) => x * x;

        static void Main(string[] args)
        {
            Log.Verbosity = LogVerbosity.Trace;

            //MainPython();
            MainBedrock();
            //MainDokan();
            //MainRemoting();
            //MainMuxing();
            //MainModel();

            if (Debugger.IsAttached)
                Console.ReadKey(true);
        }

        static void MainPython()
        {
            Python.Py_NoSiteFlag = true;
            Python.Py_Initialize();

            PythonModule.Builtin.Dictionary["Bedrock"] = new ClrNamespace("Bedrock");
            PythonModule.Builtin.Dictionary["IoT"] = new ClrNamespace("IoT");
            //PythonModule.Builtin.Dictionary["ClrNamespace"] = TypeManager.ToPython(typeof(ClrNamespace));

            PythonModule module = PythonModule.FromFile(@"..\BedRock.Server\Services\RandomSensor.py");
            PythonClass type = module.GetAttribute("RandomSensor") as PythonClass;

            //Type clrType = TypeManager.FromPython(type, typeof(SensorService));
            //object instance = Activator.CreateInstance(clrType);
            //SensorService test = instance as SensorService;

            //test.Unit.ToString();

            //instance.MyProp = 1234;
            //instance.MyProp = "Test";
            //instance.MyProp = true;

            //test.Value.ToString();
            //test.TurnOn();

            //test.ToString();








            /*dynamic sys = PythonModule.Import("sys");
            dynamic path = sys.path;


            PythonClass lionClass = TypeManager.ToPython(typeof(Lion));
            Python.PyObject_SetAttrString(sys, "lionClass", lionClass);

            var dede = lionClass.Instantiate();

            //Python.PyObject_SetAttrString(sys, "lionInstance", Python.Py_None);


            Python.PyRun_SimpleString("import sys");
            Python.PyRun_SimpleString("sys.toto = sys.lionClass()");
            Python.PyRun_SimpleString("print sys.toto");

            

            var toto = sys.GetAttribute("toto");

            /*PythonList path = (PythonList)sys.GetAttribute("path");

            // Initialize main module
            PythonModule module = new PythonModule("__main__");
            PythonDictionary globals = (PythonDictionary)module.GetAttribute("__dict__");
            
            // Execute some scripts
            var testModule = PythonModule.FromFile("Program.py");
            var test = testModule.GetAttribute("Test");
            PythonObject testInstance = Python.PyObject_CallObject(test, new PythonTuple(0));

            var plop = testInstance.GetAttribute("Plop");

            using (PythonException.Checker)
                Python.PyObject_CallObject(plop, IntPtr.Zero);*/


            //var testClass = module.GetAttribute("Test");
        }
        static async void MainBedrock()
        {
            await Task.Delay(2000);

            DbConnection dbConnection = new SQLiteConnection("Data Source=Bedrock.db;Version=3;");
            dbConnection.Open();

            Storage storage = new AdoNetStorage(dbConnection);
            storage.Load();

            if (storage.Devices.Count == 0)
            {
                storage.FillDebugData(true);
                storage.Save();
                storage.Load();
            }

            GC.KeepAlive(typeof(HttpConnection));

            ServerInfo serverInfo = storage.Devices.First(d => d.Name == "Julien-Bureau") as ServerInfo;
                                    
            using (Connection connection = await serverInfo.Connect())
            {
                DeviceHub server = await connection.GetServer();

                string serverName = server.Name;
                string serverDescription = server.Description;

                ClipboardService service = server.Services.First(s => s is ClipboardService) as ClipboardService;

                string serviceName = service.Name;
                string serviceDescription = service.Description;

                service.Data = "Test";
                //service.Copy += Service_Copy;
            }



            /*DeviceInfo serverInfo = DeviceInfo.Get(server);
            await serverInfo.Update();

            DeviceSearcher searcher = new DeviceSearcher(new[] { serverInfo });

            searcher.NewDevice += (_, d) => Console.WriteLine(d.Name);
            searcher.NewService += (_, s) => Console.WriteLine(s.Name);
            searcher.NewConnection += (_, c) => Console.WriteLine(c);

            await searcher.Search();

            Dictionary<DeviceInfo, List<PathInfo>> devicePaths = PathInfo.BuildPaths(new[] { serverInfo }, searcher.Connections).ToDictionary();

            foreach (var pair in devicePaths)
            {
                DeviceInfo device = pair.Key;
                List<PathInfo> paths = pair.Value;

                device.UpdatePaths(paths, true);
            }*/


            return;

            Thread.Sleep(1500);
            
            //Device device = server.Devices.FirstOrDefault();
            //PowerService service = device.Services.OfType<PowerService>().FirstOrDefault();

            //service.PowerOn();
        }

        private static void Service_Copy(object sender, ClipboardData e)
        {
            throw new NotImplementedException();
        }

        static async void MainDokan()
        {
            // Basic storage test
            /*DirectoryInfo directoryInfo = new DirectoryInfo(@"D:\Temp");
            Storage storage = new BasicStorage(directoryInfo);

            // Expose it with remoting
            RemotingServer server = new TcpRemotingServer(8888);
            server.AddObject("Storage", storage);
            server.Start();

            RemotingClient client = new TcpRemotingClient(8888);
            storage = await client.GetObject<Storage>("Storage");/**//*

            // Mount Dokan drive
            DokanDrive dokan = new DokanDrive(storage.Root);
            dokan.Mount('Z');

            Console.ReadKey(true);

            // Unmount Dokan drive
            dokan.Unmount();

            Environment.Exit(0);*/
        }
        static async void MainRemoting()
        {
            Lion lion = new Lion();
            BasicDeviceHub device = new BasicDeviceHub() { Name = "MyBasicDeviceHub" };

            device.AddDevice(new BasicDevice() { Name = "MyBasicDevice" });

            RemotingServer server = new TcpRemotingServer(8888);
            server.AddObject("Lion", lion);
            server.AddObject("Server", device);
            server.Start();

            RemotingClient client = new TcpRemotingClient(8888);
            Lion remoteLion = await client.GetObject<Lion>("Lion");
            //DeviceHub remoteDevice = await client.GetObject<DeviceHub>("Server");

            remoteLion.Method(1234);

            remoteLion.Event += (s, e) => Console.WriteLine("Received " + e);
            
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                remoteLion.TriggerEvent($"Event {i}");
            }

            Console.WriteLine("-- Done --");
            Thread.CurrentThread.Join();

            /*byte[] buffer = new byte[16];
            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = (byte)(i % 256);

            remoteLion.Method(buffer);
            //lion.Method(buffer);

            //Guid guid = remoteDevice.Id;
            //string name = remoteDevice.Name;
            Device[] devices = remoteDevice.Devices;

            string name = devices.First().Name;*/
        }
        static void MainMuxing()
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Loopback, 6547);
            tcpListener.Start();

            TcpClient tcpClient = new TcpClient();
            tcpClient.Connect(IPAddress.Loopback, 6547);
            NetworkStream clientStream = tcpClient.GetStream();

            TcpClient tcpListenerClient = tcpListener.AcceptTcpClient();
            NetworkStream serverStream = tcpListenerClient.GetStream();


            MuxerStream clientMuxerStream = new MuxerStream(clientStream);
            MuxerStream serverMuxerStream = new MuxerStream(serverStream);

            CanalStream clientCanal1Stream = clientMuxerStream.GetCanal("Canal1");
            CanalStream serverCanal1Stream = serverMuxerStream.GetCanal("Canal1");

            using (BinaryWriter writer = new BinaryWriter(clientCanal1Stream, Encoding.Default, true))
                writer.Write(123456);
            using (BinaryWriter writer = new BinaryWriter(serverCanal1Stream, Encoding.Default, true))
                writer.Write(654321);
            using (BinaryReader reader = new BinaryReader(clientCanal1Stream, Encoding.Default, true))
                Console.WriteLine(reader.ReadInt32());
            using (BinaryReader reader = new BinaryReader(serverCanal1Stream, Encoding.Default, true))
                Console.WriteLine(reader.ReadInt32());

            CanalStream serverCanal2Stream = serverMuxerStream.GetCanal("Canal2");
            CanalStream clientCanal2Stream = clientMuxerStream.GetCanal("Canal2");


        }
        static async void MainModel()
        {
            //ObjectDatabase database = new SQLiteConnection();


        }
    }
}