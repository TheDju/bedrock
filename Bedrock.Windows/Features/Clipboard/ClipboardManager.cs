﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Bedrock.Common;
using Bedrock.Shared;

using DokanNet;
using Microsoft.Win32;

namespace Bedrock.Windows
{
    public class ClipboardManager : IDisposable
    {
        public ClipboardData Data
        {
            get
            {
                return data;
            }
            set
            {
                if (value.Value == data.Value)
                    return;

                lock (mutex)
                {
                    data = value;

                    if (data.Value == null)
                        Clipboard.Clear();
                    else
                        switch (data.Type)
                        {
                            case ClipboardDataType.Text: Clipboard.SetText(data.Value.ToString()); break;
                        }
                }

            }
        }

        private ClipboardData data;
        private KeyboardHook keyboardHook;
        private Thread watcherThread;
        private object mutex = new object();

        public ClipboardManager()
        {
            // Hooks
            keyboardHook = new KeyboardHook();
            keyboardHook.KeyDown += KeyboardHook_KeyDown;

            // Watcher loop
            watcherThread = new Thread(WatcherThread_Loop);
            watcherThread.SetApartmentState(ApartmentState.STA);
            watcherThread.Start();
        }

        private void KeyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.Control)
                return;

            if (e.KeyCode == Keys.V)
            {
                /*if (Paste != null)
                {
                    Thread t = new Thread(() =>
                    {
                        Thread.Sleep(10);
                        Paste();
                    });
                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();
                }*/
            }
        }
        private void WatcherThread_Loop()
        {
            try
            {
                while (true)
                {
                    lock (mutex)
                    {
                        if (Clipboard.ContainsText())
                        {
                            string text = Clipboard.GetText();
                            if (data.Type != ClipboardDataType.Text || data.Value?.ToString() != text)
                            {
                                data = new ClipboardData(ClipboardDataType.Text, text);

                                /*ServiceInfo[] clipboardServices = DeviceManager.Services.Where(s => typeof(ClipboardService).IsAssignableFrom(s.Type)).ToArray();
                                foreach (ServiceInfo serviceInfo in clipboardServices)
                                {
                                    try
                                    {
                                        DeviceManager.UpdateServiceInfo(serviceInfo).Wait();
                                    }
                                    catch
                                    {
                                        continue;
                                    }

                                    ClipboardService service = serviceInfo.Service as ClipboardService;
                                    service.Data = data;
                                }*/
                            }
                        }

                        // TODO: Image, ...
                    }

                    Thread.Sleep(200);
                }
            }
            catch (ThreadAbortException) { }
        }

        public void Dispose()
        {
            watcherThread.Abort();
        }
    }
}