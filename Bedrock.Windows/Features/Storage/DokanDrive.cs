﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text.RegularExpressions;
using System.Threading;

using DokanNet;

namespace Bedrock.Windows
{
    using Common;

    public class DokanStorageContext
    {
        public Entry Entry { get; set; }
        public File File
        {
            get
            {
                return Entry as File;
            }
        }
        public Directory Directory
        {
            get
            {
                return Entry as Directory;
            }
        }

        public Stream Stream { get; set; }
    }

    public class DokanDrive : IDokanOperations
    {
        public Directory Directory { get; }

        private Thread dokanThread;
        private char? mountPoint = null;

        public DokanDrive(Directory directory)
        {
            Directory = directory;
        }

        public void Mount(char mountPoint)
        {
            if (this.mountPoint != null)
                throw new InvalidOperationException("This drive is already mounted");

            Dokan.Unmount(mountPoint);
            Dokan.RemoveMountPoint(mountPoint.ToString());

            this.mountPoint = mountPoint;

            dokanThread = new Thread(() =>
            {
                try
                {
                    DokanOptions options = DokanOptions.NetworkDrive;
                    int threadCount = 5;

#if DEBUG
                    options |= DokanOptions.DebugMode;
                    threadCount = 1;
#endif

                    (this as IDokanOperations).Mount(mountPoint + ":\\", options, threadCount);
                }
                catch (ThreadAbortException)
                {
                    Thread.ResetAbort();
                }
            });
            dokanThread.Start();
        }
        public void Unmount()
        {
            if (mountPoint == null)
                throw new InvalidOperationException("This drive is not mounted yet");

            dokanThread?.Abort();

            Dokan.Unmount((char)mountPoint);
            Dokan.RemoveMountPoint(mountPoint.ToString());
        }

        #region IDokanOperations

        NtStatus IDokanOperations.GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features, out string fileSystemName, DokanFileInfo info)
        {
            Log.Trace("DokanStorage.GetVolumeInformation()");

            volumeLabel = "Bedrock";
            features = FileSystemFeatures.CasePreservedNames | FileSystemFeatures.CaseSensitiveSearch | FileSystemFeatures.ReadOnlyVolume;
            fileSystemName = null;

            return NtStatus.Success;
        }
        NtStatus IDokanOperations.GetDiskFreeSpace(out long freeBytesAvailable, out long totalNumberOfBytes, out long totalNumberOfFreeBytes, DokanFileInfo info)
        {
            Log.Trace("DokanStorage.GetDiskFreeSpace()");

            freeBytesAvailable = 0;
            totalNumberOfBytes = 0;
            totalNumberOfFreeBytes = 0;

            return NtStatus.Success;
        }
        NtStatus IDokanOperations.Mounted(DokanFileInfo info)
        {
            Log.Trace("DokanStorage.Mounted()");
            return NtStatus.VolumeMounted;
        }
        NtStatus IDokanOperations.Unmounted(DokanFileInfo info)
        {
            Log.Trace("DokanStorage.Unmounted()");
            return NtStatus.Success;
        }

        void IDokanOperations.Cleanup(string path, DokanFileInfo info)
        {
            Log.Trace("DokanStorage.Cleanup()");
        }

        NtStatus IDokanOperations.CreateFile(string path, DokanNet.FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes, DokanFileInfo info)
        {
            path = path.Replace("\\", "/").Substring(1);
            Log.Trace($"DokanStorage.CreateFile(\"{path}\")");

            Entry entry = null;

            if (Directory == null) ;
            else if (path == string.Empty)
                entry = Directory;
            else
                entry = Directory.GetDirectory(path) ?? Directory.GetFile(path) as Entry;

            if (entry == null)
                return NtStatus.ObjectPathNotFound;

            info.IsDirectory = entry is Directory;
            info.Context = new DokanStorageContext()
            {
                Entry = entry
            };

            return NtStatus.Success;
        }
        NtStatus IDokanOperations.GetFileInformation(string path, out FileInformation fileInfo, DokanFileInfo info)
        {
            path = path.Replace("\\", "/");
            Log.Trace($"DokanStorage.GetFileInformation(\"{path}\")");

            fileInfo = new FileInformation();

            DokanStorageContext context = info.Context as DokanStorageContext;
            if (context == null)
                return NtStatus.InvalidHandle;

            fileInfo.Attributes = context.Directory != null ? FileAttributes.Directory : FileAttributes.Normal;
            fileInfo.FileName = context.Entry.Name;
            fileInfo.LastWriteTime = DateTime.Now;
            fileInfo.LastAccessTime = DateTime.Now;
            fileInfo.CreationTime = DateTime.Now;

            if (context.File != null)
                fileInfo.Length = (long)context.File.Size;

            return NtStatus.Success;
        }

        NtStatus IDokanOperations.ReadFile(string path, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info)
        {
            path = path.Replace("\\", "/");
            Log.Trace($"DokanStorage.ReadFile(\"{path}\", {offset}, {buffer.Length})");

            bytesRead = 0;

            DokanStorageContext context = info.Context as DokanStorageContext;
            if (context == null)
                return NtStatus.InvalidHandle;

            try
            {
                if (context.Stream == null)
                    context.Stream = context.File.Open(System.IO.FileAccess.Read);

                if (context.Stream.Position != offset)
                {
                    if (!context.Stream.CanSeek)
                        return NtStatus.Error;

                    context.Stream.Seek(offset, SeekOrigin.Begin);
                }

                bytesRead = context.Stream.Read(buffer, 0, buffer.Length);
            }
            catch
            {
                return NtStatus.Error;
            }

            return NtStatus.Success;
        }
        NtStatus IDokanOperations.WriteFile(string path, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        void IDokanOperations.CloseFile(string path, DokanFileInfo info)
        {
            path = path.Replace("\\", "/");
            Log.Trace($"DokanStorage.CloseFile(\"{path}\")");

            DokanStorageContext context = info.Context as DokanStorageContext;
            context?.Stream?.Close();
        }

        NtStatus IDokanOperations.FindFilesWithPattern(string path, string searchPattern, out IList<FileInformation> files, DokanFileInfo info)
        {
            path = path.Replace("\\", "/");
            Log.Trace($"DokanStorage.FindFilesWithPattern(\"{path}\", \"{searchPattern}\")");

            files = new List<FileInformation>();

            DokanStorageContext context = info.Context as DokanStorageContext;
            if (context == null)
                return NtStatus.InvalidHandle;
            if (context.Directory == null)
                return NtStatus.ObjectPathNotFound;

            // Build regex from pattern
            searchPattern = searchPattern.Replace(".", "{Point}")
                                         .Replace("*", ".*")
                                         .Replace("{Point}", "\\.");
            Regex regex = new Regex(searchPattern);

            foreach (Directory child in context.Directory.Directories)
            {
                if (!regex.IsMatch(child.Name))
                    continue;

                FileInformation fileInformation = new FileInformation()
                {
                    FileName = child.Name,
                    Attributes = FileAttributes.Directory,

                    CreationTime = DateTime.Now,
                    LastAccessTime = DateTime.Now,
                    LastWriteTime = DateTime.Now
                };

                files.Add(fileInformation);
            }

            foreach (File child in context.Directory.Files)
            {
                if (!regex.IsMatch(child.Name))
                    continue;

                DateTime date = child.Date;

                FileInformation fileInformation = new FileInformation()
                {
                    FileName = child.Name,
                    Attributes = FileAttributes.Normal,
                    Length = (long)child.Size,

                    CreationTime = date,
                    LastAccessTime = date,
                    LastWriteTime = date
                };

                files.Add(fileInformation);
            }

            return NtStatus.Success;
        }
        NtStatus IDokanOperations.FindStreams(string path, out IList<FileInformation> streams, DokanFileInfo info)
        {
            path = path.Replace("\\", "/");
            Log.Trace($"DokanStorage.FindStreams(\"{path}\")");

            streams = new List<FileInformation>();
            return NtStatus.Success;
        }

        // Not implemented
        NtStatus IDokanOperations.LockFile(string path, long offset, long length, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.UnlockFile(string path, long offset, long length, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.MoveFile(string oldName, string newName, bool replace, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.DeleteFile(string path, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.DeleteDirectory(string path, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.FindFiles(string path, out IList<FileInformation> files, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.FlushFileBuffers(string path, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.GetFileSecurity(string path, out FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            security = null;
            return NtStatus.Error;
        }
        NtStatus IDokanOperations.SetAllocationSize(string path, long length, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.SetEndOfFile(string path, long length, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.SetFileAttributes(string path, FileAttributes attributes, DokanFileInfo info)
        {
            return NtStatus.Error;
        }
        NtStatus IDokanOperations.SetFileSecurity(string path, FileSystemSecurity security, AccessControlSections sections, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }
        NtStatus IDokanOperations.SetFileTime(string path, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime, DokanFileInfo info)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}