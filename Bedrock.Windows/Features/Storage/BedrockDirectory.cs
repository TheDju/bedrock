﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Windows
{
    public class BedrockDirectory : Directory
    {
        public class BedrockServiceCollection : ICollection<ServiceInfo>
        {
            public int Count
            {
                get
                {
                    throw new NotImplementedException();
                }
            }
            public bool IsReadOnly
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            private BedrockDirectory bedrockDirectory;

            public BedrockServiceCollection(BedrockDirectory bedrockDirectory)
            {
                this.bedrockDirectory = bedrockDirectory;
            }

            public void Add(ServiceInfo item)
            {
                bedrockDirectory.serviceDirectories.Add(item.Id, new DokanServiceDirectory(bedrockDirectory, item));
            }
            public void Clear()
            {
                bedrockDirectory.serviceDirectories.Clear();
            }
            public bool Contains(ServiceInfo item)
            {
                return bedrockDirectory.serviceDirectories.ContainsKey(item.Id);
            }
            public void CopyTo(ServiceInfo[] array, int arrayIndex)
            {
                ServiceInfo[] services = bedrockDirectory.serviceDirectories.Select(p => p.Value.serviceInfo).ToArray();
                services.CopyTo(array, arrayIndex);
            }
            public bool Remove(ServiceInfo item)
            {
                return bedrockDirectory.serviceDirectories.Remove(item.Id);
            }

            public IEnumerator<ServiceInfo> GetEnumerator()
            {
                return bedrockDirectory.serviceDirectories.Select(p => p.Value.serviceInfo).GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        public override string Name
        {
            get
            {
                return "Bedrock";
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public override Directory Parent
        {
            get
            {
                return null;
            }
        }
        public override Storage Storage
        {
            get
            {
                return null;
            }
        }
        public override IEnumerable<Directory> Directories
        {
            get
            {
                foreach (Directory directory in serviceDirectories.Values)
                    yield return directory;
            }
        }
        public override IEnumerable<File> Files
        {
            get
            {
                yield break;
            }
        }

        public BedrockServiceCollection Services { get; }

        internal Dictionary<Guid, DokanServiceDirectory> serviceDirectories = new Dictionary<Guid, DokanServiceDirectory>();

        public BedrockDirectory()
        {
            Services = new BedrockServiceCollection(this);
        }

        public override Directory CreateDirectory(string name)
        {
            throw new NotSupportedException();
        }
        public override File CreateFile(string name)
        {
            throw new NotSupportedException();
        }
        public override void DeleteDirectory(Directory directory)
        {
            throw new NotSupportedException();
        }
        public override void DeleteFile(File file)
        {
            throw new NotSupportedException();
        }
    }
}