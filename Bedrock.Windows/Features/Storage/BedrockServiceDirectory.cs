﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Shared;
using DokanNet;

namespace Bedrock.Windows
{
    public class DokanServiceDirectory : Directory
    {
        public override string Name
        {
            get
            {
                return serviceInfo.Name;
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public override Directory Parent
        {
            get
            {
                return parent;
            }
        }
        public override Storage Storage
        {
            get
            {
                return parent.Storage;
            }
        }

        public override IEnumerable<Directory> Directories
        {
            get
            {
                return mountedStorage.Root.Directories;
            }
        }
        public override IEnumerable<File> Files
        {
            get
            {
                return mountedStorage.Root.Files;
            }
        }

        private Directory parent;
        internal ServiceInfo serviceInfo;

        private Storage mountedStorage;

        internal DokanServiceDirectory(Directory parent, ServiceInfo serviceInfo)
        {
            this.parent = parent;
            this.serviceInfo = serviceInfo;

            mountedStorage = (serviceInfo.Service as StorageService)?.Storage;
        }

        public override Directory CreateDirectory(string name)
        {
            return mountedStorage.Root.CreateDirectory(name);
        }
        public override File CreateFile(string name)
        {
            return mountedStorage.Root.CreateFile(name);
        }
        public override void DeleteDirectory(Directory directory)
        {
            mountedStorage.Root.DeleteDirectory(directory);
        }
        public override void DeleteFile(File file)
        {
            mountedStorage.Root.DeleteFile(file);
        }
    }
}