﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Shared;
using DokanNet;

namespace Bedrock.Windows
{
    public class BedrockDeviceDirectory : Directory
    {
        public override string Name
        {
            get
            {
                return deviceInfo.Name;
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public override Directory Parent
        {
            get
            {
                return parent;
            }
        }
        public override Storage Storage
        {
            get
            {
                return parent.Storage;
            }
        }

        public override IEnumerable<Directory> Directories
        {
            get
            {
                foreach (ServiceInfo serviceInfo in deviceInfo.Services)
                    yield return new DokanServiceDirectory(this, serviceInfo);
            }
        }
        public override IEnumerable<File> Files
        {
            get
            {
                return Enumerable.Empty<File>();
            }
        }

        private Directory parent;
        private DeviceInfo deviceInfo;

        internal BedrockDeviceDirectory(Directory parent, DeviceInfo deviceInfo)
        {
            this.parent = parent;
            this.deviceInfo = deviceInfo;
        }

        public override Directory CreateDirectory(string name)
        {
            throw new NotSupportedException();
        }
        public override File CreateFile(string name)
        {
            throw new NotSupportedException();
        }
        public override void DeleteDirectory(Directory directory)
        {
            throw new NotSupportedException();
        }
        public override void DeleteFile(File file)
        {
            throw new NotSupportedException();
        }
    }
}