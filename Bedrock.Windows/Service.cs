﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Serialization.Formatters;
using System.Security.Principal;
using System.ServiceProcess;
using Bedrock.Common;
using Utilities.Remoting;
using Utilities.Remoting.Http;
using Utilities.Remoting.Tcp;

namespace Bedrock.Windows
{
    [RunInstaller(true)]
    public sealed class BedrockServiceProcessInstaller : ServiceProcessInstaller
    {
        public BedrockServiceProcessInstaller()
        {
            Account = ServiceAccount.User;
            Username = WindowsIdentity.GetCurrent().Name;
        }
    }

    [RunInstaller(true)]
    public sealed class BedrockServiceInstaller : ServiceInstaller
    {
        public BedrockServiceInstaller()
        {
            Description = "Bedrock Service";
            DisplayName = Program.ServiceName;
            ServiceName = Program.ServiceName;
            StartType = ServiceStartMode.Automatic;
        }
    }

    public class BedrockService : ServiceBase
    {
        #region Service control

        public static bool Installed
        {
            get
            {
                ServiceController service = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == Program.ServiceName);
                return service != null;
            }
        }
        public static bool Running
        {
            get
            {
                ServiceController service = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == Program.ServiceName);
                return service != null && (service.Status == ServiceControllerStatus.Running || service.Status == ServiceControllerStatus.StartPending);
            }
        }

        public static void Install()
        {
            if (Installed)
                return;

            IDictionary saveState = new Hashtable();

            using (AssemblyInstaller installer = new AssemblyInstaller(Assembly.GetExecutingAssembly(), new string[0]))
            {
                installer.UseNewContext = true;

                try
                {
                    installer.Install(saveState);
                    installer.Commit(saveState);
                }
                catch
                {
                    try
                    {
                        installer.Rollback(saveState);
                    }
                    catch { }

                    throw;
                }
            }
        }
        public static void Uninstall()
        {
            if (!Installed)
                return;

            IDictionary saveState = new Hashtable();

            using (AssemblyInstaller installer = new AssemblyInstaller(Assembly.GetExecutingAssembly(), new string[0]))
            {
                installer.UseNewContext = true;
                installer.Uninstall(saveState);
            }
        }
        public static void Start(params string[] args)
        {
            ServiceController service = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == Program.ServiceName);
            if (service == null)
                throw new Exception(Program.ServiceName + " service is not installed on this computer");

            if (service.Status != ServiceControllerStatus.Running && service.Status != ServiceControllerStatus.StartPending)
                service.Start(args);
        }
        public static void Stop()
        {
            ServiceController service = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == Program.ServiceName);
            if (service == null)
                throw new Exception(Program.ServiceName + " service is not installed on this computer");

            if (service.Status != ServiceControllerStatus.Stopped && service.Status != ServiceControllerStatus.StopPending)
                service.Stop();
        }

        #endregion

        public Dictionary<string, string> Parameters { get; private set; }

        public FileInfo LogFile { get; private set; }
        public LogVerbosity LogVerbosity
        {
            get
            {
                return Log.Verbosity;
            }
            set
            {
                Log.Verbosity = value;
            }
        }
        public WindowsServer Device { get; private set; }

        private ObjRef serviceRef;
        private RemotingServer remotingServer;

        public BedrockService()
        {
            ServiceName = Program.ServiceName;

            CanHandlePowerEvent = true;
            CanHandleSessionChangeEvent = true;
            CanPauseAndContinue = true;
            CanShutdown = true;
            CanStop = true;
        }

        protected override void OnStart(string[] args)
        {
            Parameters = args.Where(p => p.StartsWith("/"))
                             .Select(p => p.TrimStart('/'))
                             .Select(p => new { Parameter = p.Trim(), Separator = p.Trim().IndexOf(':') })
                             .ToDictionary(p => p.Separator == -1 ? p.Parameter.ToLower() : p.Parameter.Substring(0, p.Separator).ToLower(), p => p.Separator == -1 ? null : p.Parameter.Substring(p.Separator + 1), StringComparer.InvariantCultureIgnoreCase);

            if (Parameters.ContainsKey("debug"))
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
                else
                    Debugger.Launch();
            }

            // Handle parameters
            if (Parameters.ContainsKey("log"))
                LogFile = new FileInfo(Parameters["log"]);
            else
                LogFile = new FileInfo(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Bedrock.Service.log"));

            // Redirect logging
#if DEBUG
            Log.TraceStream = new FileLogWriter(LogFile);
            Log.DebugStream = new FileLogWriter(LogFile);
#endif
            Log.InfoStream = new FileLogWriter(LogFile);
            Log.WarningStream = new FileLogWriter(LogFile);
            Log.ErrorStream = new FileLogWriter(LogFile);

            // Start service
            Log.Info(Environment.NewLine);
            Log.Info("Starting " + Program.ServiceName + " service");

            base.OnStart(args);

            // Share service via .NET remoting
            Log.Debug("Enabling remote monitoring");

            BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = TypeFilterLevel.Full;

            IChannel channel = new IpcServerChannel(Program.ServiceName, Program.ServiceName + ".Service", serverProvider);
            ChannelServices.RegisterChannel(channel);

            serviceRef = RemotingServices.Marshal(this, nameof(BedrockService));

            Log.Info("Service started");

            // Start a Bedrock server
            Log.Debug("Starting " + Program.ServiceName + " server");

            Device = new WindowsServer() { Name = "Windows" };
            Device.Start();

            //Device = new WindowsDevice() { Name = "Windows" };
            //Device.AddService(new WindowsClipboardService());
            //Device.AddService(new WindowsPowerService());

            Log.Debug("Started {0} ({1}) server", Device.Name, Device.Id);

            Log.Info("Service started");
        }
        protected override void OnStop()
        {
            Log.Info("Stopping " + Program.ServiceName + " service");

            base.OnStop();
            Environment.Exit(0);
        }
        protected override void OnPause()
        {
            base.OnPause();
        }
        protected override void OnContinue()
        {
            base.OnContinue();
        }
        protected override void OnShutdown()
        {
            base.OnShutdown();
        }
        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);
        }
        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
        {
            return base.OnPowerEvent(powerStatus);
        }
        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            base.OnSessionChange(changeDescription);
        }
    }
}