@echo off

net stop Bedrock 2>NUL

tasklist /FI "IMAGENAME eq Bedrock.Windows.exe" 2>NUL | find /I /N "Bedrock.Windows.exe">NUL
if "%ERRORLEVEL%"=="0" taskkill /f /im Bedrock.Windows.exeexe

exit 0