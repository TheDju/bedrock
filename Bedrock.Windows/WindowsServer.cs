﻿using System;
using System.Collections.Generic;

using Utilities.Remoting;

using Bedrock.Common;
using Bedrock.Server;

namespace Bedrock.Windows
{
    public class WindowsServer : Bedrock.Server.Server
    {
        public override IEnumerable<Service> Services => services;
        public override IEnumerable<Device> Devices => devices;

        public override IEnumerable<AccessPoint> AccessPoints => accessPoints;
        public override RemotingAccessPolicy AccessPolicy => RemotingAccessPolicy.Allowed;

        private List<AccessPoint> accessPoints = new List<AccessPoint>();
        private List<Service> services = new List<Service>();
        private List<Device> devices = new List<Device>();

        public WindowsServer()
        {
            // Setup access points
            accessPoints.Add(new HttpAccessPoint(this) { Port = 2480 });

            // Add basic services
            TryAddService(() => new WindowsClipboardService());
            TryAddService(() => new WindowsPowerService());
            TryAddService(() => new PlatformShellService());
        }

        private void TryAddService(Func<Service> serviceBuilder)
        {
            try
            {
                Service service = serviceBuilder();
                services.Add(service);
            }
            catch { }
        }
        private void TryAddDevice(Func<Device> deviceBuilder)
        {
            try
            {
                Device device = deviceBuilder();
                devices.Add(device);
            }
            catch { }
        }
    }
}