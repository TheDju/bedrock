﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Serialization.Formatters;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Bedrock.Common;

namespace Bedrock.Windows
{
    public class WindowsDevice : DeviceHub
    {
        public override IEnumerable<Service> Services
        {
            get
            {
                return services;
            }
        }
        public override IEnumerable<Device> Devices
        {
            get
            {
                return devices;
            }
        }

        private List<Service> services = new List<Service>();
        private List<Device> devices = new List<Device>();

        public void AddService(Service service)
        {
            services.Add(service);

            Log.Info("Added service '{0}' ({1})", service.Name, service.Id);
        }
        public void AddDevice(Device device)
        {
            devices.Add(device);

            Log.Info("Added {0} ({1}) device", device.Name, device.Id);
        }
    }
}