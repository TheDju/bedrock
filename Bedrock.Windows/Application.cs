﻿using System;
using System.Data.Common;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bedrock.Common;
using Bedrock.Shared;
using Bedrock.Shared.Storage;
using Bedrock.Windows.Properties;

using Utilities.Remoting;
using Utilities.Remoting.Http;
//using Utilities.Remoting.Tcp;

namespace Bedrock.Windows
{
    public class NotificationReceiver : MarshalByRefObject
    {
        internal event EventHandler Notification;

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void OnNotification(object sender, EventArgs e)
        {
            Notification?.Invoke(sender, e);
        }
    }

    public class BedrockApplication : ApplicationContext
    {
        public NotifyIcon Icon { get; private set; }
        public ContextMenu Menu { get; private set; }
        public BedrockService Service { get; private set; }
        internal Shared.Storage.Storage Storage { get; private set; }

        public DokanDrive DokanDrive { get; private set; }
        public ClipboardManager ClipboardManager { get; private set; }

        private MenuItem startServiceButton, stopServiceButton;
        private NotificationReceiver notificationReceiver = new NotificationReceiver();

        public BedrockApplication()
        {
            InitializeComponent();
            InitializeServices();
            
            // Start service if needed
            if (Program.Parameters.ContainsKey("start"))
                Task.Run(() => StartService()).ContinueWith(t => ConnectToService(false));
            else
                Task.Run(() => ConnectToService(false));

            // Initialize storage
            DbConnection dbConnection = new SQLiteConnection("Data Source=Bedrock.db;Version=3;");
            dbConnection.Open();

            Storage = new AdoNetStorage(dbConnection);
            Storage.Load();

#if DEBUG
            if (Storage.Devices.Count == 0)
            {
                Storage.FillDebugData(true);
                Storage.Save();
            }
#endif

            /*DeviceHub device = deviceTask.Result;
            DeviceInfo deviceInfo = DeviceInfo.Get(device);
            deviceInfo.Update().Wait();

            Program.Devices.Add(deviceInfo);

            SelectDeviceWindow selectDeviceWindow = new SelectDeviceWindow();
            selectDeviceWindow.Title = "Select a device :)";
            selectDeviceWindow.ShowDialog();*/
        }

        private void InitializeComponent()
        {
            Icon = new NotifyIcon();
            Menu = new ContextMenu();

            Icon.Icon = Resources.Icon;
            Icon.Text = Program.ServiceName;
            Icon.ContextMenu = Menu;
            Icon.Visible = true;
            Icon.DoubleClick += SettingsButton_Click;

            Menu.Popup += Menu_Popup;
            startServiceButton = new MenuItem("Start", (s, e) => { });
            stopServiceButton = new MenuItem("Stop", (s, e) => { });

            startServiceButton.Enabled = true;
            stopServiceButton.Enabled = false;
        }
        private async void InitializeServices()
        {
            await Task.Run(() =>
            {
                // Storage service
                /*DokanDrive = new DokanDrive();

                ServiceInfo[] storageServices = DeviceManager.Services.Where(s => typeof(StorageService).IsAssignableFrom(s.Type)).ToArray();
                foreach (ServiceInfo serviceInfo in storageServices)
                {
                    await DeviceManager.UpdateServiceInfo(serviceInfo);
                    DokanDrive.AddStorage(serviceInfo.Service as StorageService);
                }

                DokanDrive.Mount();//*/

                // Clipboard service
                ClipboardManager = new ClipboardManager();
            });
        }

        private void Menu_Popup(object sender, EventArgs ea)
        {
            try
            {
                if (Service != null)
                    Service.ToString();
            }
            catch (Exception ex)
            {
                Service = null;
            }

            if (Service == null && BedrockService.Running)
                ConnectToService(false);

            Menu.MenuItems.Clear();

            Menu.MenuItems.AddRange(new[]
            {
                new MenuItem("Test", TestButton_Click),
                new MenuItem("Settings", SettingsButton_Click),
                new MenuItem("-"),
                new MenuItem("Exit", ExitButton_Click)
            });
        }

        private void TestButton_Click(object sender, EventArgs e)
        {
        }
        private void SettingsButton_Click(object sender, EventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.Show();
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            if (Program.Parameters.ContainsKey("start"))
                StopService();

            ClipboardManager.Dispose();

            Icon.Visible = false;
            ExitThread();
        }

        private void StartService()
        {
            if (!BedrockService.Installed)
            {
                if (!Program.Parameters.ContainsKey("quiet"))
                    Icon.ShowBalloonTip(5000, Program.ServiceName, Program.ServiceName + " service is not installed on this computer", ToolTipIcon.Error);

                return;
            }

            if (Program.Parameters.ContainsKey("debug"))
                BedrockService.Start("/debug");
            else
                BedrockService.Start();
            
            startServiceButton.Enabled = false;
            stopServiceButton.Enabled = true;

            Thread.Sleep(2000);
        }
        private void StopService()
        {
            if (!BedrockService.Installed)
            {
                if (!Program.Parameters.ContainsKey("quiet"))
                    Icon.ShowBalloonTip(5000, Program.ServiceName, Program.ServiceName + " service is not installed on this computer", ToolTipIcon.Error);

                return;
            }

            BedrockService.Stop();

            startServiceButton.Enabled = true;
            stopServiceButton.Enabled = false;
        }
        private async void ConnectToService(bool canThrow = true)
        {
            string uri = string.Format("ipc://{0}/{1}", Program.ServiceName + ".Service", nameof(BedrockService));

            try
            {
                Service = (BedrockService)Activator.GetObject(typeof(BedrockService), uri);
                Service.ToString();

                if (!Program.Parameters.ContainsKey("quiet"))
                    Icon.ShowBalloonTip(4000, Program.ServiceName, "Connected to " + Program.ServiceName + " service", ToolTipIcon.Info);
            }
            catch (Exception e)
            {
                Service = null;

                if (canThrow && Debugger.IsAttached)
                    Debugger.Break();
                else if (!Program.Parameters.ContainsKey("quiet"))
                    Icon.ShowBalloonTip(4000, Program.ServiceName, "Could not connect to " + Program.ServiceName + " service. " + e.Message, ToolTipIcon.Error);
            }
        }
    }
}