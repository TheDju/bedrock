﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Windows
{
    public partial class ServicesWindow : Window, INotifyPropertyChanged
    {
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hwnd, int index);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hwnd, int index, int newStyle);
        [DllImport("user32.dll")]
        private static extern bool SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter, int x, int y, int width, int height, uint flags);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, uint msg, IntPtr wParam, IntPtr lParam);

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_DLGMODALFRAME = 0x0001;
        private const int SWP_NOSIZE = 0x0001;
        private const int SWP_NOMOVE = 0x0002;
        private const int SWP_NOZORDER = 0x0004;
        private const int SWP_FRAMECHANGED = 0x0020;
        private const uint WM_SETICON = 0x0080;

        public ObservableCollection<DeviceInfo> Devices { get; set; }
        public DeviceInfo SelectedDevice { get; set; }

        public string Message { get; set; } = "Select the device in the list below";
        
        private DeviceSearcher deviceSearcher;

        public ServicesWindow()
        {
            InitializeComponent();

            Devices = new ObservableCollection<DeviceInfo>(Program.Devices);
            DataContext = this;
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            IntPtr hwnd = new WindowInteropHelper(this).Handle;

            int extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_DLGMODALFRAME);

            SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);

            SendMessage(hwnd, WM_SETICON, new IntPtr(1), IntPtr.Zero);
            SendMessage(hwnd, WM_SETICON, IntPtr.Zero, IntPtr.Zero);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            deviceSearcher = new DeviceSearcher(Program.Devices);
            deviceSearcher.NewDevice += DeviceSearcher_NewDevice;
            deviceSearcher.Search();
        }

        private void DeviceSearcher_NewDevice(DeviceSearcher searcher, DeviceInfo device)
        {
            device.Updated += Device_Updated;
            Dispatcher.Invoke(() => Devices.Add(device));
        }
        private void Device_Updated(object sender, EventArgs e)
        {
            NotifyPropertyChanged(nameof(Devices));
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName]string property = null)
        {
            if (property != null && PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}