﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Interop;

using Microsoft.Win32;

using Bedrock.Windows.Properties;

namespace Bedrock.Windows
{
    public partial class SettingsWindow : Window, INotifyPropertyChanged
    {
        public bool UseHttpServer
        {
            get
            {
                return Settings.Default.HttpServerEnabled;
            }
            set
            {
                Settings.Default.HttpServerEnabled = value;
                Settings.Default.Save();
            }
        }
        public bool UseTcpServer
        {
            get
            {
                return Settings.Default.TcpServerEnabled;
            }
            set
            {
                Settings.Default.TcpServerEnabled = value;
                Settings.Default.Save();
            }
        }

        #region Storage service

        public bool EnableStorageService
        {
            get
            {
                return dokanDrive != null;
            }
            set
            {
                if (value && dokanDrive == null)
                {
                    dokanDrive = new DokanDrive(bedrockDirectory);
                    dokanDrive.Mount('B');
                }
                else if (!value && dokanDrive != null)
                {
                    dokanDrive.Unmount();
                    dokanDrive = null;
                }
            }
        }

        private BedrockDirectory bedrockDirectory = new BedrockDirectory();
        private DokanDrive dokanDrive;

        #endregion

        public SettingsWindow()
        {
            InitializeComponent();

            DataContext = this;
            Tag = new DependencyManager(this, (s, e) => PropertyChanged?.Invoke(s, e));
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            IntPtr hwnd = new WindowInteropHelper(this).Handle;

            int extendedStyle = User32.GetWindowLong(hwnd, User32.GWL_EXSTYLE);
            User32.SetWindowLong(hwnd, User32.GWL_EXSTYLE, extendedStyle | User32.WS_EX_DLGMODALFRAME);

            User32.SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, User32.SWP_NOMOVE | User32.SWP_NOSIZE | User32.SWP_NOZORDER | User32.SWP_FRAMECHANGED);

            User32.SendMessage(hwnd, User32.WM_SETICON, new IntPtr(1), IntPtr.Zero);
            User32.SendMessage(hwnd, User32.WM_SETICON, IntPtr.Zero, IntPtr.Zero);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName]string property = null)
        {
            if (property != null && PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}