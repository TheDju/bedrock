﻿using System.Threading;
using System.Windows.Forms;

using Bedrock.Common;

namespace Bedrock.Windows
{
    public class WindowsClipboardService : ClipboardService
    {
        public override string Name => "Windows clipboard";
        public override ClipboardData Data
        {
            get
            {
                return data;
            }
            set
            {
                if (value.Value == data.Value)
                    return;

                lock (mutex)
                {
                    data = value;

                    Thread clipboardThread = new Thread(() =>
                    {
                        if (data.Value == null)
                            Clipboard.Clear();
                        else
                        {
                            switch (data.Type)
                            {
                                case ClipboardDataType.Text: Clipboard.SetText(data.Value.ToString()); break;
                            }
                        }
                    });

                    clipboardThread.SetApartmentState(ApartmentState.STA);
                    clipboardThread.Start();
                }

            }
        }

        private ClipboardData data;
        //private KeyboardHook keyboardHook;
        private Thread watcherThread;
        private object mutex = new object();

        public WindowsClipboardService()
        {
            // Hooks
            //keyboardHook = new KeyboardHook();
            //keyboardHook.KeyDown += KeyboardHook_KeyDown;

            // Watcher loop
            watcherThread = new Thread(WatcherThread_Loop);
            watcherThread.SetApartmentState(ApartmentState.STA);
            watcherThread.Start();
        }

        private void KeyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.Control)
                return;

            if (e.KeyCode == Keys.V)
            {
                /*if (Paste != null)
                {
                    Thread t = new Thread(() =>
                    {
                        Thread.Sleep(10);
                        Paste();
                    });
                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();
                }*/
            }
        }
        private void WatcherThread_Loop()
        {
            while (Thread.CurrentThread.ThreadState == System.Threading.ThreadState.Running)
            {
                lock (mutex)
                {
                    if (Clipboard.ContainsText())
                    {
                        string text = Clipboard.GetText();
                        if (data.Type != ClipboardDataType.Text || data.Value?.ToString() != text)
                            OnCopy(new ClipboardData(ClipboardDataType.Text, text));
                    }

                    // TODO: Image, ...
                }

                Thread.Sleep(200);
            }
        }
    }
}