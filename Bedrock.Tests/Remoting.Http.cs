﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Remoting.Http;

namespace Bedrock.Tests
{
    [TestClass]
    public class RemotingTests
    {
        private class TestClass : MarshalByRefObject
        {
            public int Value
            {
                get
                {
                    return value;
                }
                set
                {
                    this.value = value;
                }
            }
            private int value;

            public event EventHandler Event;

            public TestClass(int value)
            {
                this.value = value;
            }

            public string TestMethod() => value.ToString();
            public string TestMethod(string prefix) => prefix + value;

            public void TestEvent(object toto)
            {
                Event?.Invoke(toto, null);
            }
        }

        [TestMethod]
        [TestCategory("Remoting.Http")]
        public void TestRemoteConnection()
        {
            HttpRemotingServer remotingServer = new HttpRemotingServer(1234);
            Assert.AreEqual(remotingServer.Port, 1234);

            remotingServer.AddObject("Object", new TestClass(4321));
            remotingServer.Start();

            HttpRemotingClient remotingClient = new HttpRemotingClient(1234);
            Assert.AreEqual(remotingClient.Port, 1234);

            Task<TestClass> testClassTask = remotingClient.GetObject<TestClass>("Object");
            testClassTask.Wait();

            TestClass testClass = testClassTask.Result;
            Assert.IsNotNull(testClass);

            remotingServer.Stop();
        }

        [TestMethod]
        [TestCategory("Remoting.Http")]
        public void TestRemoteCall()
        {
            HttpRemotingServer remotingServer = new HttpRemotingServer(1234);
            remotingServer.AddObject("Object", new TestClass(4321));
            remotingServer.Start();

            HttpRemotingClient remotingClient = new HttpRemotingClient(1234);

            Task<TestClass> testClassTask = remotingClient.GetObject<TestClass>("Object");
            testClassTask.Wait();

            TestClass testClass = testClassTask.Result;
            Assert.AreEqual(testClass.Value, 4321);

            testClass.Value /= 2;
            Assert.AreEqual(testClass.Value, 2160);

            string result = testClass.TestMethod();
            Assert.AreEqual(result, "2160");

            result = testClass.TestMethod("prefix");
            Assert.AreEqual(result, "prefix2160");

            remotingServer.Stop();
        }

        [TestMethod]
        [TestCategory("Remoting.Http")]
        public void TestRemoteEvent()
        {
            HttpRemotingServer remotingServer = new HttpRemotingServer(1234);
            remotingServer.AddObject("Object", new TestClass(4321));
            remotingServer.Start();

            HttpRemotingClient remotingClient = new HttpRemotingClient(1234);

            Task<TestClass> testClassTask = remotingClient.GetObject<TestClass>("Object");
            testClassTask.Wait();

            TestClass testClass = testClassTask.Result;

            object result = null;
            AutoResetEvent done = new AutoResetEvent(false);

            testClass.Event += (s, e) =>
            {
                result = s;
                done.Set();
            };

            testClass.TestEvent(1234);
            done.WaitOne();
            Assert.AreEqual(result, 1234);

            remotingServer.Stop();
        }
    }
}