﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Bedrock.Server
{
    using Python;
    using Python = Python.Python;

    public class Program
    {
        public static Dictionary<string, string> Options { get; private set; }
        public static List<string> Parameters { get; private set; }

        public static void Main(string[] args)
        {
            Version currentVersion = Assembly.GetExecutingAssembly().GetName().Version;

            Log.Info("-- Bedrock Server - Version {0} --", currentVersion.ToString(3));
            Log.Info("");

            // Arguments parsing
            Options = args.Where(a => a.StartsWith("/"))
                          .Select(a => a.TrimStart('/'))
                          .Select(a => new { Parameter = a.Trim(), Separator = a.Trim().IndexOf(':') })
                          .ToDictionary(a => a.Separator == -1 ? a.Parameter : a.Parameter.Substring(0, a.Separator).ToLower(), a => a.Separator == -1 ? null : a.Parameter.Substring(a.Separator + 1));
            Parameters = args.Where(a => !a.StartsWith("/"))
                             .ToList();

            string serverPath = Parameters.FirstOrDefault();
            if (serverPath == null)
            {
                Log.Error("You must specify a server file to run");
                Exit();
            }

            FileInfo serverInfo = new FileInfo(serverPath);
            if (!serverInfo.Exists)
            {
                Log.Error("Could not find the specified server");
                Exit();
            }

            // Initialize Python
            if (Environment.OSVersion.Platform != PlatformID.Unix)
                Python.Py_NoSiteFlag = true;

            Python.Py_Initialize();

            PythonModule.Builtin.Dictionary["Bedrock"] = new ClrNamespace("Bedrock");
            //PythonModule.Builtin.Dictionary["ClrNamespace"] = null;//TypeManager.ToPython(typeof(ClrNamespace));

            Environment.CurrentDirectory = serverInfo.DirectoryName;

            // Load server file
            Server server = null;

            Log.Info("Loading server from {0}", serverPath);

            try
            {
                switch (serverInfo.Extension.ToLower())
                {
                    case ".json": server = new JsonServer(serverInfo); break;
                    //case ".xml": server = new XmlServer(serverInfo); break;
                }

                Log.Info("Server loaded successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Log.Error("-- {0} --", e.GetType().FullName);

                while (e != null)
                {
                    Log.Error(e.Message);
                    Log.Error(e.StackTrace);
                    Console.WriteLine();

                    e = e.InnerException;
                }

                Exit();
            }

            Log.Info("");
            Log.Info("Starting server ...");

            server.Start();

            Log.Info("Server started successfully");

            Console.CancelKeyPress += Console_CancelKeyPress;
            Thread.CurrentThread.Join();
        }
        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Environment.Exit(0);
        }
        
        private static Type ResolveType(string name, Type neighborType = null)
        {
            if (name == null)
                return null;

            string fullName = name;
            if (neighborType != null && !fullName.Contains('.'))
                fullName = neighborType.Namespace + "." + fullName;

            Type type = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(name, false, true)).FirstOrDefault(t => t != null);
            if (type != null)
                return type;

            type = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.ExportedTypes).FirstOrDefault(t => t.Name == name || t.FullName == fullName);
            if (type != null)
                return type;

            return null;
        }

        public static void Exit()
        {
            if (Debugger.IsAttached || Options.ContainsKey("pause"))
            {
                Console.WriteLine();
                Console.Write("Press any key to exit ...");
                Console.ReadKey(true);
            }

            Environment.Exit(0);
        }
    }
}