﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Utilities.Remoting;

namespace Bedrock.Server
{
    public abstract class BedrockAccessPolicy : RemotingAccessPolicy
    {
        public object Object { get; }
        public Session Session { get; }

        public sealed override RemotingAccessPolicy GetAccessPolicy(MemberInfo member)
        {
            if (member is MethodInfo method)
                return GetAccessPolicy(method);
            else if (member is PropertyInfo property)
                return GetAccessPolicy(property);
            else if (member is FieldInfo field)
                return GetAccessPolicy(field);
            else if (member is EventInfo evnt)
                return GetAccessPolicy(evnt);

            return Denied;
        }

        public abstract RemotingAccessPolicy GetAccessPolicy(MethodInfo method);
        public abstract RemotingAccessPolicy GetAccessPolicy(PropertyInfo property);
        public abstract RemotingAccessPolicy GetAccessPolicy(FieldInfo field);
        public abstract RemotingAccessPolicy GetAccessPolicy(EventInfo field);
    }
}
