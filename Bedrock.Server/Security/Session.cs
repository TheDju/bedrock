﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bedrock.Common;

namespace Bedrock.Server
{
    public class Session
    {
        public User User { get; }
        public DateTime ConnectionDate { get; }


    }
}
