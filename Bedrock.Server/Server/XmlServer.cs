﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

using Bedrock.Common;

using File = System.IO.File;

namespace Bedrock.Server
{
    public class XmlServer : Server
    {
        public XmlServer(FileInfo serverInfo)
        {
            if (serverInfo == null || !serverInfo.Exists)
                throw new FileNotFoundException("The specified server file could not be found");

            List<RemotingServer> remotingServers = new List<RemotingServer>();

            // Load the server
            try
            {
                Log.Info("Loading server {0}...", serverInfo.Name);

                string content = File.ReadAllText(serverInfo.FullName);
                XDocument document = XDocument.Parse(content, LoadOptions.SetLineInfo);
                XElement root = document.Root;

                // Properties
                XElement propertiesElement = root.Element("Properties");
                if (propertiesElement != null)
                {
                    // References
                    XElement[] referenceElements = propertiesElement.Element("References")?.Elements()?.ToArray();
                    if (referenceElements != null)
                    {
                        foreach (XElement referenceElement in referenceElements)
                        {
                            switch (referenceElement.Name.LocalName)
                            {
                                case "Assembly":
                                    string file = referenceElement.Attribute("Path").Value;
                                    string path = file;

                                    if (!Path.IsPathRooted(path))
                                        path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path);

                                    try
                                    {
                                        Assembly assembly = Assembly.LoadFile(path);
                                    }
                                    catch
                                    {
                                        throw new Exception("Could not find referenced assembly " + file);
                                    }
                                    break;

                                case "Script":
                                    throw new NotSupportedException();
                            }
                        }
                    }

                    // Remoting
                    XElement[] remotingElements = propertiesElement.Element("Remoting")?.Elements("Server")?.ToArray();
                    if (remotingElements != null)
                    {
                        foreach (XElement remotingElement in remotingElements)
                        {
                            XAttribute typeAttribute = remotingElement.Attribute("Type");
                            if (typeAttribute == null)
                                throw BuildException("Unable to find Type attribue", typeAttribute);

                            Type type = ResolveType(typeAttribute.Value, typeof(RemotingServer));
                            if (type == null)
                                throw BuildException("Unable to find specified type" + typeAttribute.Value, typeAttribute);

                            if (!typeof(RemotingServer).IsAssignableFrom(type))
                                throw BuildException("Specified type " + type.FullName + " is not a remoting server", typeAttribute);

                            RemotingServer remotingServer = Activator.CreateInstance(type) as RemotingServer;

                            ProcessAttributes(remotingServer, remotingElement, "Type");

                            remotingServers.Add(remotingServer);
                        }
                    }
                }

                // Default remoting server if necessary
                if (remotingServers.Count == 0)
                    remotingServers.Add(new HttpRemotingServer());

                ProcessAttributes(server, root);

                // Load services
                XElement[] serviceElements = root.Element("Services")?.Elements("Service")?.ToArray();
                if (serviceElements != null)
                {
                    foreach (XElement serviceElement in serviceElements)
                    {
                        Service service = LoadService(serviceElement);
                        server.AddService(service);

                        Log.Info("- Loaded service \"{0}\" ({1})", service.Name, service.Id);
                    }
                }

                XElement[] pythonServiceElements = root.Element("Services")?.Elements("PythonService")?.ToArray();
                if (pythonServiceElements != null)
                {
                    foreach (XElement pythonServiceElement in pythonServiceElements)
                    {
                        Service service = LoadPythonService(pythonServiceElement);
                        server.AddService(service);

                        Log.Info("- Loaded python service \"{0}\" ({1})", service.Name, service.Id);
                    }
                }

                // Load devices
                XElement[] deviceElements = root.Element("Devices")?.Elements("Device")?.ToArray();
                if (deviceElements != null)
                {
                    foreach (XElement deviceElement in deviceElements)
                    {
                        Device device = LoadDevice(deviceElement);
                        server.AddDevice(device);

                        Log.Info("- Loaded device \"{0}\" ({1})", device.Name, device.Id);
                    }
                }

                XElement[] remoteDeviceElements = root.Element("Devices")?.Elements("RemoteDevice")?.ToArray();
                if (remoteDeviceElements != null)
                {
                    foreach (XElement remoteDeviceElement in remoteDeviceElements)
                    {
                        Device device = LoadRemoteDevice(remoteDeviceElement);
                        server.AddDevice(device);

                        Log.Info("- Loaded device \"{0}\" ({1})", device.Name, device.Id);
                    }
                }

                Log.Info("Server loaded successfully : {0}", server.Id);
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Log.Error("-- {0} --", e.GetType().FullName);

                while (e != null)
                {
                    Log.Error(e.Message);
                    Log.Error(e.StackTrace);
                    Console.WriteLine();

                    e = e.InnerException;
                }

                Exit();
            }

        }

        private static Device LoadDevice(XElement element)
        {
            Type type;

            XAttribute typeAttribute = element.Attribute("Type");
            if (typeAttribute == null)
                type = typeof(BasicDevice);
            else
            {
                string typeName = typeAttribute.Value;
                if (!typeName.Contains('.'))
                    typeName = typeof(Device).Namespace + "." + typeName;

                type = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName, false, true)).FirstOrDefault(a => a != null);
                if (type == null)
                    throw new Exception("Unable to find service type " + typeAttribute.Value + " at line " + (typeAttribute as IXmlLineInfo).LineNumber);
            }

            Device device = Activator.CreateInstance(type) as Device;
            ProcessAttributes(device, element, "Type");

            if (type == typeof(BasicDevice))
            {
                BasicDevice basicDevice = device as BasicDevice;

                XElement[] serviceElements = element.Element("Services")?.Elements("Service")?.ToArray();
                if (serviceElements != null)
                {
                    foreach (XElement serviceElement in serviceElements)
                    {
                        Service service = LoadService(serviceElement);
                        basicDevice.AddService(service);

                        Log.Info("- Loaded service \"{0}/{1}\" ({2})", device.Name, service.Name, service.Id);
                    }
                }

                XElement[] pythonServiceElements = element.Element("Services")?.Elements("PythonService")?.ToArray();
                if (pythonServiceElements != null)
                {
                    foreach (XElement pythonServiceElement in pythonServiceElements)
                    {
                        Service service = LoadPythonService(pythonServiceElement);
                        basicDevice.AddService(service);

                        Log.Info("- Loaded service \"{0}/{1}\" ({2})", device.Name, service.Name, service.Id);
                    }
                }
            }

            return device;
        }
        private static Device LoadRemoteDevice(XElement element)
        {
            XAttribute methodAttribute = element.Attribute("Method");
            string method = methodAttribute?.Value?.ToLower() ?? "http";

            // Find the type matching the specified method
            Type type = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes())
                                                               .Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(RemoteDevice)) && t.IsDefined(typeof(RemoteDeviceAttribute)))
                                                               .FirstOrDefault(t => t.GetCustomAttribute<RemoteDeviceAttribute>().Method.ToLower() == method);
            if (type == null)
                throw new Exception("Could not find any remote device supporting method " + method);

            XAttribute addressAttribute = element.Attribute("Address");
            if (addressAttribute == null)
                throw new Exception("You must specify a remote device address at line " + (element as IXmlLineInfo).LineNumber);

            // Build remote device and parse attributes
            RemoteDevice device = Activator.CreateInstance(type, new object[] { addressAttribute.Value }) as RemoteDevice;
            ProcessAttributes(device, element, "Method", "Address");
            ProcessElements(device, element);

            // Perform a first connection
            device.Connect();

            return device;
        }

        private static Service LoadService(XElement element)
        {
            XAttribute typeAttribute = element.Attribute("Type");
            if (typeAttribute == null)
                throw new Exception("You must specify a service type at line " + (element as IXmlLineInfo).LineNumber);

            string typeName = typeAttribute.Value;
            if (!typeName.Contains('.'))
                typeName = typeof(Service).Namespace + "." + typeName;

            Type type = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName, false, true)).FirstOrDefault(a => a != null);
            if (type == null)
                throw new Exception("Unable to find service type " + typeAttribute.Value + " at line " + (typeAttribute as IXmlLineInfo).LineNumber);

            Service service = Activator.CreateInstance(type) as Service;
            ProcessAttributes(service, element, "Type");
            ProcessElements(service, element);

            return service;
        }
        private static Service LoadPythonService(XElement element)
        {
            XAttribute moduleAttribute = element.Attribute("Module");
            if (moduleAttribute == null)
                throw new Exception("You must specify a python module path at line " + (element as IXmlLineInfo).LineNumber);

            FileInfo pythonFile = new FileInfo(moduleAttribute.Value);
            if (!pythonFile.Exists)
                throw new Exception("Could not find specified python module " + moduleAttribute.Value + " at line " + (element as IXmlLineInfo).LineNumber);

            XAttribute typeAttribute = element.Attribute("Type");
            if (typeAttribute == null)
                throw new Exception("You must specify a service type at line " + (element as IXmlLineInfo).LineNumber);

            string typeName = typeAttribute.Value;
            if (!typeName.Contains('.'))
                typeName = typeof(Service).Namespace + "." + typeName;

            Type type = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName, false, true)).FirstOrDefault(a => a != null);
            if (type == null)
                throw new Exception("Unable to find service type " + typeAttribute.Value + " at line " + (typeAttribute as IXmlLineInfo).LineNumber);

            XAttribute classAttribute = element.Attribute("Class");

            // Load and build Python type
            PythonModule pythonModule = PythonModule.FromFile(moduleAttribute.Value);
            PythonClass pythonClass;

            if (classAttribute != null)
            {
                pythonClass = pythonModule.GetAttribute(classAttribute.Value) as PythonClass;
                if (pythonClass == null)
                    throw new Exception("Could not find specified class in this python module");
            }
            else
            {
                PythonClass[] pythonClasses = pythonModule.Dictionary.Values.OfType<PythonClass>().ToArray();
                if (pythonClasses.Length == 0)
                    throw new Exception("Could not find any class in this python module");
                if (pythonClasses.Length > 1)
                    throw new Exception("This python module contains more than one class. Please specify the one to use");

                pythonClass = pythonClasses[0];
            }

            Type clrType = TypeManager.FromPython(pythonClass, type);

            Service service = Activator.CreateInstance(clrType) as Service;
            ProcessAttributes(service, element, "Type", "Class", "Module");
            ProcessElements(service, element);

            return service;
        }

        private static Type ResolveType(string name, Type neighborType = null)
        {
            if (name == null)
                return null;

            string fullName = name;
            if (neighborType != null && !fullName.Contains('.'))
                fullName = neighborType.Namespace + "." + fullName;

            Type type = AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(name, false, true)).FirstOrDefault(t => t != null);
            if (type != null)
                return type;

            type = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.ExportedTypes).FirstOrDefault(t => t.Name == name || t.FullName == fullName);
            if (type != null)
                return type;

            return null;
        }
        private static void ProcessAttributes(object obj, XElement element, params string[] propertiesToSkip)
        {
            Type type = obj.GetType();

            foreach (XAttribute attribute in element.Attributes())
            {
                string attributeName = attribute.Name.LocalName;
                if (propertiesToSkip.Any(p => p.ToLower() == attributeName.ToLower()))
                    continue;

                PropertyInfo property = type.GetProperty(attributeName);
                if (property == null)
                    throw new Exception("Could not find property " + attributeName + " on type " + type.Name + " at line " + (attribute as IXmlLineInfo).LineNumber);

                Type propertyType = property.PropertyType;
                object value = attribute.Value;

                if (value.GetType() != propertyType && !value.GetType().IsSubclassOf(propertyType))
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(propertyType);

                    try
                    {
                        if (converter.IsValid(value))
                            value = converter.ConvertFrom(value);
                        else
                            value = Activator.CreateInstance(propertyType, value);
                    }
                    catch
                    {
                        throw new Exception("Unable to convert the property value to type " + propertyType + " at line " + (attribute as IXmlLineInfo).LineNumber);
                    }
                }

                property.SetValue(obj, value);
            }
        }
        private static void ProcessElements(object obj, XElement element)
        {
            Type type = obj.GetType();

            foreach (XElement subElement in element.Elements())
            {
                string elementName = subElement.Name.LocalName;

                PropertyInfo property = type.GetProperty(elementName);
                if (property == null)
                    throw new Exception("Could not find property " + elementName + " on type " + type.Name + " at line " + (subElement as IXmlLineInfo).LineNumber);

                Type propertyType = property.PropertyType;
                object value;

                // Try to decode object type
                XAttribute elementTypeAttribute = subElement.Attribute("Type");
                if (elementTypeAttribute != null)
                {
                    Type objectType = ResolveType(elementTypeAttribute.Value, typeof(Service));
                    if (objectType == null)
                        throw new Exception("Unable to find type " + elementTypeAttribute.Value + " at line " + (elementTypeAttribute as IXmlLineInfo).LineNumber);
                    if (!propertyType.IsAssignableFrom(objectType))
                        throw new Exception("Unable to deserialize type " + type.FullName + " in property " + property.Name + " of type " + propertyType.FullName + " at line " + (elementTypeAttribute as IXmlLineInfo).LineNumber);

                    value = Activator.CreateInstance(objectType);
                    ProcessAttributes(value, subElement, "Type");
                    ProcessElements(value, subElement);
                }

                // Else try to convert the value
                else
                {
                    value = subElement.Value;

                    if (value.GetType() != propertyType && !value.GetType().IsSubclassOf(propertyType))
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(propertyType);

                        try
                        {
                            if (converter.IsValid(value))
                                value = converter.ConvertFrom(value);
                            else
                                value = Activator.CreateInstance(propertyType, value);
                        }
                        catch
                        {
                            throw new Exception("Unable to convert the property value to type " + propertyType + " at line " + (subElement as IXmlLineInfo).LineNumber);
                        }
                    }
                }

                property.SetValue(obj, value);
            }
        }

        private static Exception BuildException(string message, XObject xmlObject = null)
        {
            IXmlLineInfo xmlLineInfo = xmlObject as IXmlLineInfo;

            if (xmlLineInfo != null)
                return new XmlException(message, null, xmlLineInfo.LineNumber, xmlLineInfo.LinePosition);
            else
                return new XmlException(message);
        }
    }
}