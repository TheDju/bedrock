﻿using System;
using System.Collections.Generic;

using Utilities.Remoting;

using Bedrock.Common;

namespace Bedrock.Server
{
    public abstract class SimpleServer : Server
    {
        public override IEnumerable<AccessPoint> AccessPoints
        {
            get
            {
                if (HttpServerEnabled)
                    yield return httpAccessPoint;
                if (TcpServerEnabled)
                    yield return tcpAccessPoint;
            }
        }
        public override RemotingAccessPolicy AccessPolicy { get; } = RemotingAccessPolicy.Allowed;

        public override IEnumerable<Service> Services => services;
        public override IEnumerable<Device> Devices => devices;

        public bool HttpServerEnabled { get; set; } = true;
        public ushort HttpServerPort { get; set; } = 2480;
        public bool TcpServerEnabled { get; set; } = true;
        public ushort TcpServerPort { get; set; } = 2424;

        private List<Service> services = new List<Service>();
        private List<Device> devices = new List<Device>();

        private HttpAccessPoint httpAccessPoint;
        private TcpAccessPoint tcpAccessPoint;

        public void TryAddService(Func<Service> serviceBuilder)
        {
            try
            {
                Service service = serviceBuilder();
                services.Add(service);
            }
            catch { }
        }
        public void TryAddDevice(Func<Device> deviceBuilder)
        {
            try
            {
                Device device = deviceBuilder();
                devices.Add(device);
            }
            catch { }
        }
    }
}