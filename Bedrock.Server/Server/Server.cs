﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Bedrock.Common;

using Utilities.IO;
using Utilities.Remoting;

namespace Bedrock.Server
{
    /// <summary>
    /// A Bedrock server providing devices, services and access points
    /// </summary>
    public abstract class Server : DeviceHub
    {
        public abstract IEnumerable<AccessPoint> AccessPoints { get; }
        public abstract RemotingAccessPolicy AccessPolicy { get; }

        public virtual void Start()
        {
            foreach (AccessPoint accessPoint in AccessPoints)
            {
                accessPoint.Listen();
                Log.Info("> Listening on " + accessPoint);
            }
        }

        public object Authenticate(string username, string hash)
        {
            return null;
        }
        protected virtual void ConnectionCallback(StreamAccessPoint accessPoint, Guid clientId, Stream stream)
        {
            // Get some metrics
            MeteredStream meteredStream = new MeteredStream(stream);

            // Build the client handler
            Dictionary<string, MarshalByRefObject> serverObject = new Dictionary<string, MarshalByRefObject>() { { "Server", this } };
            //BinaryRemotingHandler client = new BinaryRemotingHandler(meteredStream, serverObject);
        }
    }
}