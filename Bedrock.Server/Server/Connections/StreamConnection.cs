﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bedrock.Common;
using Utilities.Remoting;

namespace Bedrock.Server
{
    public abstract class StreamConnection : Connection
    {
        public override async Task<DeviceHub> GetServer()
        {
            Stream serverStream = await GetServerStream();

            BinaryRemotingClient remotingClient = new BinaryRemotingClient(serverStream);
            return await remotingClient.GetObject<DeviceHub>("Server");
        }
        public abstract Task<Stream> GetServerStream();

        public override void Dispose()
        {
            // TODO: Dispose everything

            base.Dispose();
        }
    }
}