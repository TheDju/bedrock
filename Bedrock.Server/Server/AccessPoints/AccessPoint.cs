﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Bedrock.Server
{
    public abstract class AccessPoint
    {
        public Server Server { get; }

        public AccessPoint(Server server)
        {
            Server = server;
        }

        public abstract void Listen();
    }
}