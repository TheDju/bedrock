﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Shared;
using Utilities.Remoting;

namespace Bedrock.Server
{
    public delegate void ConnectionCallback(StreamAccessPoint accessPoint, Guid clientId, Stream stream);

    public abstract class StreamAccessPoint : AccessPoint
    {
        public StreamAccessPoint(Server server) : base(server)
        {
        }

        public virtual ConnectionCallback Connected { get; set; }

        protected virtual void OnConnected(Stream stream)
        {
            // TODO: Handle client authentication

            BedrockRemotingRegistry remotingRegistry = new BedrockRemotingRegistry();

            remotingRegistry.AddBaseObject("Server", Server);
            remotingRegistry.RegisterObject(Server, Server.AccessPolicy);

            BinaryRemotingHandler serverClient = new BinaryRemotingHandler(stream, remotingRegistry);

            Connected?.Invoke(this, Guid.Empty, stream);
        }
    }
}