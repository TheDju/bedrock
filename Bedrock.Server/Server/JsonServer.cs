﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;

using Bedrock.Common;
using Bedrock.Shared;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Python;
using Utilities.Remoting;

using File = System.IO.File;

namespace Bedrock.Server
{
    public class JsonServer : Server
    {
        public override IEnumerable<AccessPoint> AccessPoints => linkPoints;
        public override IEnumerable<Service> Services => services;
        public override IEnumerable<Device> Devices => devices;

        public override RemotingAccessPolicy AccessPolicy { get; } = RemotingAccessPolicy.Allowed;

        private List<AccessPoint> linkPoints = new List<AccessPoint>();
        private List<Service> services = new List<Service>();
        private List<Device> devices = new List<Device>();

        public JsonServer(FileInfo serverInfo)
        {
            if (serverInfo == null || !serverInfo.Exists)
                throw new FileNotFoundException("The specified server file could not be found");

            // Read server
            string serverContent = File.ReadAllText(serverInfo.FullName);
            JObject serverObject = JsonConvert.DeserializeObject(serverContent) as JObject;

            Name = serverObject.Property("Name").Value.Value<string>();
            Description = serverObject.Property("Description").Value.Value<string>();

            // Properties
            JObject propertiesObject = serverObject.Property("Properties")?.Value?.Value<JObject>();
            JArray referencesArray = propertiesObject?.Property("References")?.Value?.Value<JArray>();
            JArray accessPointsArray = propertiesObject?.Property(nameof(AccessPoints))?.Value?.Value<JArray>();

            if (referencesArray != null)
            {
                foreach (JObject referenceObject in referencesArray)
                {
                    string file = referenceObject.Property("Path")?.Value?.Value<string>();
                    string path = file;

                    if (!Path.IsPathRooted(path))
                        path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), path);

                    try
                    {
                        Assembly assembly = Assembly.LoadFile(path);
                    }
                    catch
                    {
                        throw new Exception("Could not find referenced assembly " + file);
                    }
                }
            }

            if (accessPointsArray != null)
            {
                foreach (JObject accessPointObject in accessPointsArray)
                {
                    AccessPoint accessPoint = ReadObject(accessPointObject, typeof(AccessPoint), t => Activator.CreateInstance(t, new object[] { this })) as AccessPoint;
                    linkPoints.Add(accessPoint);
                }
            }

            // Services
            JArray servicesArray = serverObject.Property("Services")?.Value?.Value<JArray>();
            if (servicesArray != null)
            {
                foreach (JObject serviceObject in servicesArray)
                {
                    Service service = ReadObject(serviceObject, typeof(Service)) as Service;
                    services.Add(service);
                }
            }

            // Devices
            JArray devicesArray = serverObject.Property("Devices")?.Value?.Value<JArray>();
            if (devicesArray != null)
            {
                foreach (JObject deviceObject in devicesArray)
                {
                    Device device = ReadObject(deviceObject, typeof(Device)) as Device;
                    devices.Add(device);
                }
            }
        }

        private static object ReadObject(JObject jsonObject, Type baseType)
        {
            return ReadObject(jsonObject, baseType, Activator.CreateInstance);
        }
        private static object ReadObject(JObject jsonObject, Type baseType, Func<Type, object> objectBuilder)
        {
            string source = null;

            JProperty sourceProperty = jsonObject.Property("Source");
            if (sourceProperty != null)
                source = sourceProperty.Value.Value<string>();

            object result;

            if (source == "Python")
                result = ReadPythonObject(jsonObject, baseType);
            else
            {
                string objectTypeString = jsonObject.Property("Type")?.Value?.Value<string>();
                Type objectType = ResolveType(objectTypeString, baseType) ?? GetDefaultType(baseType);
                if (objectType == null)
                    throw new Exception("Could not find specified type " + objectTypeString);

                result = objectBuilder(objectType);
            }

            ReadProperties(result, jsonObject);
            return result;
        }
        private static object ReadPythonObject(JObject jsonObject, Type baseType)
        {
            string moduleName = jsonObject.Property("Module")?.Value?.Value<string>();
            if (moduleName == null)
                throw new Exception("You must specify a python module to load");

            FileInfo pythonFile = new FileInfo(moduleName);
            if (!pythonFile.Exists)
                throw new Exception("Could not find specified python module " + moduleName);

            string typeName = jsonObject.Property("Type")?.Value?.Value<string>();
            if (typeName == null)
                throw new Exception("You must specify an object type to load");

            Type type = ResolveType(typeName, baseType);
            if (type == null)
                throw new Exception("Unable to find service type " + typeName);

            string className = jsonObject.Property("Class")?.Value?.Value<string>();

            // Load and build Python type
            PythonModule pythonModule = PythonModule.FromFile(pythonFile.FullName);
            PythonClass pythonClass;

            if (className != null)
            {
                pythonClass = pythonModule.GetAttribute(className) as PythonClass;
                if (pythonClass == null)
                    throw new Exception("Could not find specified class in this python module");
            }
            else
            {
                PythonClass[] pythonClasses = pythonModule.Dictionary.Values.OfType<PythonClass>().ToArray();

                if (pythonClasses.Length == 0)
                    throw new Exception("Could not find any class in this python module");
                if (pythonClasses.Length > 1)
                    throw new Exception("This python module contains more than one class. Please specify the one to use");

                pythonClass = pythonClasses[0];
            }

            Type clrType = TypeManager.FromPython(pythonClass, type);

            object result = Activator.CreateInstance(clrType);
            return result;
        }

        private static void ReadProperties(object obj, JObject jsonObject)
        {
            Type objectType = obj.GetType();

            foreach (JProperty property in jsonObject.Properties())
            {
                if (property.Name == "Type" || property.Name == "Source")
                    continue;

                PropertyInfo propertyInfo = objectType.GetProperty(property.Name);
                if (propertyInfo == null)
                    throw new Exception("Could not find property " + property.Name + " on type " + objectType.FullName);

                if (property.Value.Type == JTokenType.Null || property.Value.Type == JTokenType.None)
                    propertyInfo.SetValue(obj, null);
                else if (property.Value.Type == JTokenType.Boolean && propertyInfo.PropertyType == typeof(bool))
                    propertyInfo.SetValue(obj, property.Value.Value<bool>());
                else if (property.Value.Type == JTokenType.Integer)
                {
                    object value = null;

                    if (propertyInfo.PropertyType == typeof(sbyte)) value = property.Value.Value<sbyte>();
                    if (propertyInfo.PropertyType == typeof(byte)) value = property.Value.Value<byte>();
                    if (propertyInfo.PropertyType == typeof(short)) value = property.Value.Value<short>();
                    if (propertyInfo.PropertyType == typeof(ushort)) value = property.Value.Value<ushort>();
                    if (propertyInfo.PropertyType == typeof(int)) value = property.Value.Value<int>();
                    if (propertyInfo.PropertyType == typeof(uint)) value = property.Value.Value<uint>();
                    if (propertyInfo.PropertyType == typeof(long)) value = property.Value.Value<long>();
                    if (propertyInfo.PropertyType == typeof(ulong)) value = property.Value.Value<ulong>();
                    if (propertyInfo.PropertyType == typeof(float)) value = property.Value.Value<float>();
                    if (propertyInfo.PropertyType == typeof(double)) value = property.Value.Value<double>();
                    if (propertyInfo.PropertyType == typeof(decimal)) value = property.Value.Value<decimal>();
                    if (propertyInfo.PropertyType == typeof(object)) value = property.Value.Value<int>();

                    if (value == null)
                        throw new Exception("Could not set integer value " + property.Value.ToString() + " to property " + property.Name + " in type " + objectType.FullName);

                    propertyInfo.SetValue(obj, value);
                }
                else if (property.Value.Type == JTokenType.Float)
                {
                    object value = null;

                    if (propertyInfo.PropertyType == typeof(float)) value = property.Value.Value<float>();
                    if (propertyInfo.PropertyType == typeof(double)) value = property.Value.Value<double>();
                    if (propertyInfo.PropertyType == typeof(decimal)) value = property.Value.Value<decimal>();
                    if (propertyInfo.PropertyType == typeof(object)) value = property.Value.Value<float>();

                    if (value == null)
                        throw new Exception("Could not set float value " + property.Value.ToString() + " to property " + property.Name + " in type " + objectType.FullName);

                    propertyInfo.SetValue(obj, value);
                }
                else if (property.Value.Type == JTokenType.String)
                {
                    object value = null;

                    if (propertyInfo.PropertyType == typeof(string)) value = property.Value.Value<string>();
                    if (propertyInfo.PropertyType == typeof(object)) value = property.Value.Value<string>();

                    if (value == null)
                    {
                        string valueString = property.Value.Value<string>();
                        TypeConverter converter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);

                        try
                        {
                            if (converter.IsValid(valueString))
                                value = converter.ConvertFrom(valueString);
                            else
                                value = Activator.CreateInstance(propertyInfo.PropertyType, valueString);
                        }
                        catch { }
                    }

                    if (value == null)
                        throw new Exception("Could not set string value " + property.Value.ToString() + " to property " + property.Name + " in type " + objectType.FullName);

                    propertyInfo.SetValue(obj, value);
                }
                else if (property.Value.Type == JTokenType.Object)
                {
                    object value = ReadObject(property.Value as JObject, propertyInfo.PropertyType);
                    propertyInfo.SetValue(obj, value);
                }
                else if (property.Value.Type == JTokenType.Array)
                {
                    JArray propertyArray = property.Value as JArray;
                    object value = null;

                    if (propertyInfo.PropertyType.IsGenericType)
                    {
                        Type genericTypeDefinition = propertyInfo.PropertyType.GetGenericTypeDefinition();
                        Type genericTypeParameter = propertyInfo.PropertyType.GetGenericArguments()[0];

                        Type arrayType = genericTypeParameter.MakeArrayType();

                        if (propertyInfo.PropertyType.IsAssignableFrom(arrayType))
                        {
                            Array array = Array.CreateInstance(genericTypeParameter, propertyArray.Count);

                            for (int i = 0; i < propertyArray.Count; i++)
                            {
                                object item = ReadObject(propertyArray[i] as JObject, genericTypeParameter);
                                array.SetValue(item, i);
                            }

                            value = array;
                        }
                    }

                    if (value == null)
                        throw new Exception("Could not set array values " + property.Value.ToString() + " to property " + property.Name + " in type " + objectType.FullName);

                    propertyInfo.SetValue(obj, value);
                }
                else
                    throw new Exception("Could not set value of type " + property.Value.Type + " to property " + property.Name + " in type " + objectType.FullName);
            }
        }

        private static Type ResolveType(string name, Type baseType = null)
        {
            if (name == null)
                return null;

            Func<string, Type> findType = n =>
            {
                foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
                {
                    Type type = assembly.GetType(n, false);

                    if (baseType != null && !baseType.IsAssignableFrom(type))
                        continue;

                    return type;
                }

                return null;
            };

            Type resolvedType = findType(name);
            if (resolvedType != null)
                return resolvedType;

            Type[] hintTypes = new Type[]
            {
                typeof(Device),
                typeof(Server),
                baseType
            };

            foreach (Type hintType in hintTypes)
            {
                if (hintType == null)
                    continue;

                resolvedType = findType(hintType.Namespace + "." + name);
                if (resolvedType != null)
                    return resolvedType;
            }

            return null;
        }
        private static Type GetDefaultType(Type baseType)
        {
            if (baseType == typeof(Device)) return typeof(BasicDevice);

            return null;
        }
    }
}