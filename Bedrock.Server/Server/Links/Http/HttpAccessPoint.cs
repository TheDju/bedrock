﻿using System;
using System.Collections.Generic;
using System.Net;

using NHttp;
using Rssdp;
using Utilities.Remoting.Http;

using Bedrock.Common;
using System.Xml.Linq;
using System.IO;
using Utilities.Remoting;

namespace Bedrock.Server
{
    [LinkMethod("http")]
    public class HttpAccessPoint : AccessPoint
    {
        public ushort Port { get; set; } = 8887;

        private SsdpDevicePublisher ssdpDevicePublisher;

        private HttpServer httpServer;
        private Dictionary<string, XmlRemotingHandler> remotingHandlers = new Dictionary<string, XmlRemotingHandler>();

        public HttpAccessPoint(Server server) : base(server)
        {
        }

        public override void Listen()
        {
            // Start local HTTP server
            httpServer = new HttpServer();

            httpServer.EndPoint = new IPEndPoint(IPAddress.Any, Port);
            httpServer.RequestReceived += HttpServer_RequestReceived;

            httpServer.Start();

            // Describe SSDP device
            /*SsdpRootDevice ssdpRootDevice = new SsdpRootDevice(new Uri("http://" + httpServer.EndPoint.Address + ":" + Port + "/Device.xml"), TimeSpan.FromMinutes(1), "")
            {
                FriendlyName = Server.Name
            };
            
            ssdpDevicePublisher = new SsdpDevicePublisher();
            ssdpDevicePublisher.AddDevice(ssdpRootDevice);*/
        }

        private void HttpServer_RequestReceived(object sender, HttpRequestEventArgs e)
        {
            // Identify the connection
            XmlRemotingHandler handler;

            string connectionId = "XXX";//e.Request.Headers["ConnectionId"];
            if (connectionId == null || !remotingHandlers.TryGetValue(connectionId, out handler))
            {
                BedrockRemotingRegistry registry = new BedrockRemotingRegistry();

                registry.AddBaseObject("Server", Server);
                registry.RegisterObject(Server, RemotingAccessPolicy.Allowed);// Server.AccessPolicy);

                handler = new XmlRemotingHandler(registry);
                //connectionId = handler.GetHashCode().ToString();

                remotingHandlers.Add(connectionId, handler);
            }

            //e.Response.Headers["ConnectionId"] = connectionId;

            try
            {
                // Build a reponse
                XDocument responseDocument = null;

                // Process request
                if (e.Request.HttpMethod == "GET")
                {
                    // Find the specified object
                    string name = e.Request.Url.AbsolutePath.Substring(1);
                    responseDocument = handler.ProcessGet(name);
                }
                else
                {
                    string idString = e.Request.Url.AbsolutePath.Substring(1);
                    int id = int.Parse(idString);

                    XDocument requestDocument = XDocument.Load(e.Request.InputStream);
                    responseDocument = handler.ProcessPost(id, requestDocument);
                }

                e.Response.StatusCode = 200;

                using (StreamWriter responseWriter = new StreamWriter(e.Response.OutputStream))
                {
                    string response = responseDocument.ToString();
                    responseWriter.WriteLine(response);
                }
            }
            catch (Exception ex)
            {
                e.Response.StatusCode = 400;

                using (StreamWriter responseWriter = new StreamWriter(e.Response.OutputStream))
                {
                    string response = ex.ToString();
                    responseWriter.WriteLine(response);
                }
            }
        }

        public override string ToString() => $"{nameof(HttpAccessPoint)} {{ Port: {Port} }}";
    }
}