﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bedrock.Common
{
    using Server;
    using Utilities.Remoting.Http;

    [LinkMethod("http")]
    public class HttpConnection : Connection
    {
        public override ConnectionSpeed Speed => ConnectionSpeed.Slow;
        public override ConnectionLatency Latency => ConnectionLatency.High;
        public override ConnectionReliability Reliability => ConnectionReliability.High;

        public string Address { get; }
        public ushort Port { get; }

        private HttpRemotingClient remotingClient;

        public HttpConnection(string address, ushort port)
        {
            Address = address;
            Port = port;

            remotingClient = new HttpRemotingClient(address, port);
        }
        
        public override async Task<DeviceHub> GetServer()
        {
            return await remotingClient.GetObject<DeviceHub>("Server");
        }
    }
}