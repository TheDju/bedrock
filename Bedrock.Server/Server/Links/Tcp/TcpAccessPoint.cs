﻿using System;
using System.Net;
using System.Net.Sockets;
using Bedrock.Common;

namespace Bedrock.Server
{
    [LinkMethod("tcp")]
    public class TcpAccessPoint : StreamAccessPoint
    {
        internal const ushort DiscoveryPort = 8891;
        public ushort Port { get; set; } = 8889;

        private UdpClient udpClient;
        private TcpListener tcpListener;

        public TcpAccessPoint(Server server) : base(server)
        {
        }

        public override void Listen()
        {
            /*udpClient = new UdpClient(DiscoveryPort);
            udpClient.BeginReceive(UdpClient_Receive, null);*/

            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(TcpListener_AcceptTcpClient, null);
        }

        private void UdpClient_Receive(IAsyncResult asyncResult)
        {
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, DiscoveryPort);
            byte[] buffer = udpClient.EndReceive(asyncResult, ref ipEndPoint);

            udpClient.BeginReceive(UdpClient_Receive, null);

            udpClient.Send(new byte[] { 0x12, 0x56, 0x84, 0x3F }, 4, ipEndPoint);
        }
        private void TcpListener_AcceptTcpClient(IAsyncResult asyncResult)
        {
            TcpClient tcpClient = tcpListener.EndAcceptTcpClient(asyncResult);
            tcpListener.BeginAcceptTcpClient(TcpListener_AcceptTcpClient, null);

            OnConnected(tcpClient.GetStream());
        }

        public override string ToString() => $"{nameof(TcpAccessPoint)} {{ Port: {Port} }}";
    }
}