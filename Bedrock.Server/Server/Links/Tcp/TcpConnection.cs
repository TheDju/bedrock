﻿using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

using Bedrock.Common;

namespace Bedrock.Server
{
    [LinkMethod("tcp")]
    public class TcpConnection : StreamConnection
    {
        public override ConnectionLatency Latency => ConnectionLatency.Low;
        public override ConnectionSpeed Speed => ConnectionSpeed.Fast;
        public override ConnectionReliability Reliability => ConnectionReliability.High;

        public string Address { get; }
        public ushort Port { get; }

        public TcpConnection(string address, ushort port)
        {
            Address = address;
            Port = port;
        }

        public override Task<Stream> GetServerStream()
        {
            return Task.Run(() =>
            {
                TcpClient tcpClient = new TcpClient(Address, Port);
                return tcpClient.GetStream() as Stream;
            });
        }
    }
}