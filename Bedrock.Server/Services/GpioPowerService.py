﻿import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

class GpioPowerService:

    def __init__(self):
        self.port = None

    @property
    def Value(self):
        return self.Read() != 0
    @Value.setter
    def Value(self, value):
        self.Write(value)

    @property
    def Port(self):
        return self.port
    @Value.setter
    def Port(self, value):
        self.port = int(value)
        GPIO.setup(self.port, GPIO.OUT)
            
    def Read(self):
        if not self.port:
            raise Exception('No port is set')
        return GPIO.input(self.port)
    def Write(self, value):
        if not self.port:
            raise Exception('No port is set')
        GPIO.output(self.port, value)
        
    def TurnOn(self):
        self.Write(1)
    def TurnOff(self):
        self.Write(0)