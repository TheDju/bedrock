﻿import wmi
w = wmi.WMI()

Data = IoT.Common.Data
Units = IoT.Common.Units
Value = IoT.Common.Value

class TemperatureSensor(Bedrock.IoT.SensorService):

    @property
    def Unit(self):
        return Units.Temperature.Celsius

    def GetData(self):
        value = w.Win32_TemperatureProbe()[0].CurrentReading
        return Data(value, Units.Temperature.Celsius)