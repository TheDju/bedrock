﻿#import cec

#config = cec.libcec_configuration()
#config.strDeviceName = "RPi :)"
#config.bActivateSource = 0
#config.deviceTypes.Add(cec.CEC_DEVICE_TYPE_RECORDING_DEVICE)
#config.clientVersion = cec.LIBCEC_VERSION_CURRENT

#lib = cec.ICECAdapter.Create(config)

#adapters = lib.DetectAdapters()
#adapter = adapters[0].strComName

#lib.Open(adapter)

RemoteKey = Bedrock.Common.RemoteKey

class CecRemoteService:

    #@property
    #def Keys(self):
    #    return lib.GetDevicePowerStatus(cec.CECDEVICE_TV) == cec.CEC_POWER_STATUS_ON

    def Press(self, key):

        if   key == RemoteKey.Number0: print key #lib.Transmit(lib.CommandFromString('10:44:20'))
        elif key == RemoteKey.Number1: print key #lib.Transmit(lib.CommandFromString('10:44:21'))
        elif key == RemoteKey.Number2: print key #lib.Transmit(lib.CommandFromString('10:44:22'))
        elif key == RemoteKey.Number3: print key #lib.Transmit(lib.CommandFromString('10:44:23'))
        elif key == RemoteKey.Number4: print key #lib.Transmit(lib.CommandFromString('10:44:24'))
        elif key == RemoteKey.Number5: print key #lib.Transmit(lib.CommandFromString('10:44:25'))
        elif key == RemoteKey.Number6: print key #lib.Transmit(lib.CommandFromString('10:44:26'))
        elif key == RemoteKey.Number7: print key #lib.Transmit(lib.CommandFromString('10:44:27'))
        elif key == RemoteKey.Number8: print key #lib.Transmit(lib.CommandFromString('10:44:28'))
        elif key == RemoteKey.Number9: print key #lib.Transmit(lib.CommandFromString('10:44:29'))
        else:
            print 'Unknown key ', key