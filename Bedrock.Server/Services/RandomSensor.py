﻿import random

#IoT = ClrNamespace("IoT")

Data = IoT.Common.Data
Units = IoT.Common.Units
Value = IoT.Common.Value

print Units.Temp

class RandomSensor: #(Bedrock.IoT.SensorService):

    @property
    def Unit(self):
        return Units.Temp

    def GetData(self):
        value = random.randint(0, 1024)
        return Data(value, Units.None)