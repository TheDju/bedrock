﻿import cec

config = cec.libcec_configuration()
config.strDeviceName = "RPi :)"
config.bActivateSource = 0
config.deviceTypes.Add(cec.CEC_DEVICE_TYPE_RECORDING_DEVICE)
config.clientVersion = cec.LIBCEC_VERSION_CURRENT

lib = cec.ICECAdapter.Create(config)

adapters = lib.DetectAdapters()
adapter = adapters[0].strComName

lib.Open(adapter)

class CecPowerService:

    @property
    def Value(self):
        return lib.GetDevicePowerStatus(cec.CECDEVICE_TV) == cec.CEC_POWER_STATUS_ON
    @Value.setter
    def Value(self, value):
        if value:
            self.TurnOn()
        else:
            self.TurnOff()

    def TurnOn(self):
        lib.PowerOnDevices(cec.CECDEVICE_TV)
    def TurnOff(self):
        lib.StandbyDevices(cec.CECDEVICE_TV)