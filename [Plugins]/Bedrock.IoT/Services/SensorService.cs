﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;
using IoT.Common;

namespace Bedrock.IoT
{
    public abstract class SensorService : Service
    {
        public abstract Unit Unit { get; }

        public abstract Data GetData();
        public virtual async Task<Data> GetDataAsync()
        {
            return await Task.Run(new Func<Data>(GetData));
        }
    }
}