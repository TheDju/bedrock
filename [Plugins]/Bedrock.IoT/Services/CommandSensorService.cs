﻿using System;
using System.Text.RegularExpressions;

using IoT.Common;

namespace Bedrock.IoT.Services
{
    public abstract class AbstractCommandSensorService : SensorService
    {
        public override Unit Unit => unit;

        protected Unit unit;
    }

    public class CommandSensorService : AbstractCommandSensorService
    {
        public new string Unit
        {
            get => unit.Name;
            set => throw new NotSupportedException();
        }

        public string Command { get; set; }
        public string Regex
        {
            get => regex.ToString();
            set => regex = new Regex(value, RegexOptions.Compiled);
        }

        private Regex regex;

        public CommandSensorService()
        {
            Regex = "[0-9]+(.[0-9]+)?";
        }

        public override Data GetData()
        {
            // TODO: Run the command and extract the value

            return new Data();
        }
    }
}
