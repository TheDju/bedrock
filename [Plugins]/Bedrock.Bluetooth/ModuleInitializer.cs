using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Bedrock.Bluetooth
{
    internal class ResourceAssemblyInfo
    {
        public string File { get; }
        public AssemblyName AssemblyName { get; }

        public ResourceAssemblyInfo(string file, string assemblyName)
        {
            File = file;
            AssemblyName = new AssemblyName(assemblyName);
        }
    }

    internal static class ModuleInitializer
    {
        private const string resourcePrefix = "Bedrock.Bluetooth.Extern";
        private static ResourceAssemblyInfo[] resourceAssemblies =
        {
            new ResourceAssemblyInfo("InTheHand.Net.Personal.dll", "InTheHand.Net.Personal, Version=3.5.605.0, Culture=neutral, PublicKeyToken=ea38caa273134499")
        };

        internal static void Run()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            ResourceAssemblyInfo resourceAssembly = resourceAssemblies.FirstOrDefault(r => r.AssemblyName.FullName == args.Name);
            if (resourceAssembly == null)
                return null;

            try
            {
                using (Stream assemblyStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcePrefix + "." + resourceAssembly.File))
                {
                    if (assemblyStream == null)
                        return null;

                    // Read assembly
                    byte[] assemblyBytes = new byte[assemblyStream.Length];
                    assemblyStream.Read(assemblyBytes, 0, assemblyBytes.Length);

                    // Load assembly
                    Assembly assembly = Assembly.Load(assemblyBytes);
                    return assembly;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}