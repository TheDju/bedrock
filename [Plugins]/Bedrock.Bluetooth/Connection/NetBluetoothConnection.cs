﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;

namespace Bedrock.Bluetooth
{
    using Server;

    public class NetBluetoothConnection : Connection
    {
        public override ConnectionSpeed Speed => ConnectionSpeed.Slow;
        public override ConnectionLatency Latency => ConnectionLatency.High;
        public override ConnectionReliability Reliability => ConnectionReliability.Low;

        private BluetoothClient bluetoothClient;

        public NetBluetoothConnection()
        {
            /*BluetoothAddress

            bluetoothClient = new BluetoothClient();
            bluetoothClient.Connect()*/
        }

        public override Task<Server> GetServer()
        {
            throw new NotImplementedException();
        }
    }
}
