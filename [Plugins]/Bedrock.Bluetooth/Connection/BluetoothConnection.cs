﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bedrock.Common;

namespace Bedrock.Bluetooth
{
    using Server;

    [LinkMethod("bluetooth")]
    public class BluetoothConnection : Connection
    {
        public override ConnectionSpeed Speed => ConnectionSpeed.Slow;
        public override ConnectionLatency Latency => ConnectionLatency.High;
        public override ConnectionReliability Reliability => ConnectionReliability.Low;

        public BluetoothConnection()
        {
            throw new NotImplementedException();
        }

        public override Task<Server> GetServer()
        {
            throw new NotImplementedException();
        }
    }
}
