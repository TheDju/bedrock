﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bedrock.Bluetooth
{
    using Server;

    [LinkMethod("bluetooth")]
    public class BluetoothAccessPoint : StreamAccessPoint
    {
        public override ConnectionCallback Connected
        {
            get
            {
                return bluetoothAccessPoint.Connected;
            }
            set
            {
                bluetoothAccessPoint.Connected = value;
            }
        }

        private StreamAccessPoint bluetoothAccessPoint;

        public BluetoothAccessPoint(Server server) : base(server)
        {
            bool xamarin = Type.GetType("Android.App.Application") != null;

            if (xamarin)
                InitializeXamarin();
            else
                InitializeDotNet();
        }

        public override void Listen()
        {
            bluetoothAccessPoint.Listen();
        }

        private void InitializeDotNet()
        {
            bluetoothAccessPoint = new NetBluetoothAccessPoint(Server);
        }
        private void InitializeXamarin()
        {
            bluetoothAccessPoint = new AndroidBluetoothAccessPoint(Server);
        }

        public override string ToString()
        {
            return bluetoothAccessPoint.ToString();
        }
    }
}