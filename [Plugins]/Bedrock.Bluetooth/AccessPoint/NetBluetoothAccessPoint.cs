﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;

namespace Bedrock.Bluetooth
{
    using Server;

    internal class NetBluetoothAccessPoint : StreamAccessPoint
    {
        public byte[] Address { get; private set; }

        private BluetoothListener bluetoothListener;

        public NetBluetoothAccessPoint(Server server) : base(server) { }

        public override void Listen()
        {
            bluetoothListener = new BluetoothListener(BluetoothService.RFCommProtocol);
            bluetoothListener.Start();

            bluetoothListener.BeginAcceptBluetoothClient(BluetoothListener_AcceptBluetoothClient, null);

            Address = bluetoothListener.LocalEndPoint.Address.ToByteArray();
        }

        private void BluetoothListener_AcceptBluetoothClient(IAsyncResult asyncResult)
        {
            BluetoothClient bluetoothClient = bluetoothListener.EndAcceptBluetoothClient(asyncResult);
            bluetoothListener.BeginAcceptBluetoothClient(BluetoothListener_AcceptBluetoothClient, null);

            Connected?.Invoke(this, Guid.Empty, bluetoothClient.GetStream());
        }

        public override string ToString()
        {
            return nameof(NetBluetoothAccessPoint) + " { Address: " + string.Join(":", Address.Select(b => b.ToString("x2"))) + " }";
        }
    }
}
