﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

using Bedrock.Common;

namespace Bedrock.WindowsRuntime
{
    public class WindowsPushService : PushService
    {
        public override void PushImage(byte[] buffer)
        {
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastImageAndText01);

            // You can use the methods from the XML document to specify the required elements for the toast.
            XmlNodeList imageNodes = toastXml.GetElementsByTagName("image");
            imageNodes[0].Attributes.GetNamedItem("src").NodeValue = "images/toastImageAndText.png";

            XmlNodeList textNodes = toastXml.GetElementsByTagName("text");
            foreach (var textNode in textNodes)
                textNode.AppendChild(toastXml.CreateTextNode("Data"));

            // Create a toast notification from the XML, then create a ToastNotifier object
            // to send the toast.
            var toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public override void PushText(string text)
        {
            // The getTemplateContent method returns a Windows.Data.Xml.Dom.XmlDocument object
            // that contains the toast notification XML content.
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);

            XmlNodeList textNodes = toastXml.GetElementsByTagName("text");
            foreach (var textNode in textNodes)
                textNode.AppendChild(toastXml.CreateTextNode(text));

            // Create a toast notification from the XML, then create a ToastNotifier object
            // to send the toast.
            var toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public override void PushUri(Uri uri)
        {
            throw new NotImplementedException();
        }
    }
}