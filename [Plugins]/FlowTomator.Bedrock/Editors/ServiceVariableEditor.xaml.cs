﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Desktop;

namespace FlowTomator.Bedrock
{
    [VariableEditor(typeof(ServiceInfo))]
    public partial class ServiceVariableEditor : UserControl
    {
        public VariableInfo VariableInfo
        {
            get
            {
                return DataContext as VariableInfo;
            }
        }

        public ServiceVariableEditor()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            DevicesWindow devicesWindow;

            ServiceVariable serviceVariable = VariableInfo.Variable as ServiceVariable;
            if (serviceVariable != null)
                devicesWindow = new DevicesWindow(serviceVariable.ServiceType);
            else
                devicesWindow = new DevicesWindow();

            devicesWindow.ShowDialog();

            await DeviceManager.UpdateServiceInfo(devicesWindow.SelectedService);
            VariableInfo.Value = devicesWindow.SelectedService;
        }
    }
}