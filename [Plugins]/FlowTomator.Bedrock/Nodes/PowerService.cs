﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Common;

namespace FlowTomator.Bedrock
{
    public enum PowerServiceStatus
    {
        On,
        Off,
        Switch
    }

    [Node("Power device", "Bedrock", "Power on/off the specified device")]
    public class SwitchPowerServiceTask : Task
    {
        public override IEnumerable<Variable> Inputs
        {
            get
            {
                yield return powerService;
                yield return status;
            }
        }

        private ServiceVariable<PowerService> powerService = new ServiceVariable<PowerService>("Service", "The power service to operate");
        private Variable<PowerServiceStatus> status = new Variable<PowerServiceStatus>("Status", PowerServiceStatus.Switch, "The new status to apply to the specified power service");

        public SwitchPowerServiceTask()
        {
            Bedrock.Initialize();
        }

        public override NodeResult Run()
        {
            if (powerService.Value == null)
            {
                Log.Error("You must specify a power service to operate");
                return NodeResult.Fail;
            }

            DeviceManager.UpdateServiceInfo(powerService.Value).Wait();
            PowerService service = powerService.Service;

            switch (status.Value)
            {
                case PowerServiceStatus.On: service.PowerOn(); break;
                case PowerServiceStatus.Off: service.PowerOff(); break;
                case PowerServiceStatus.Switch: service.Switch(); break;
            }

            return NodeResult.Success;
        }
    }

    [Node("Device powered", "Bedrock", "Triggers when the specified device power state changes")]
    public class PowerServiceEvent : Event
    {
        public override IEnumerable<Variable> Inputs
        {
            get
            {
                yield return powerService;
                yield return status;
            }
        }

        private ServiceVariable<PowerService> powerService = new ServiceVariable<PowerService>("Service", "The power service to operate");
        private Variable<PowerServiceStatus> status = new Variable<PowerServiceStatus>("Status", PowerServiceStatus.Switch, "The new status to apply to the specified power service");

        public PowerServiceEvent()
        {
            Bedrock.Initialize();
        }

        public override NodeResult Check()
        {
            if (powerService.Value == null)
            {
                Log.Error("You must specify a power service to operate");
                return NodeResult.Fail;
            }

            DeviceManager.UpdateServiceInfo(powerService.Value).Wait();
            PowerService service = powerService.Service;

            return NodeResult.Fail;
        }
    }
}