﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Common;

namespace FlowTomator.Bedrock
{
    [Node("Remote device", "Bedrock", "Send the specified key to a remote service")]
    public class SendRemoteServiceKeyTask : Task
    {
        public override IEnumerable<Variable> Inputs
        {
            get
            {
                yield return remoteService;
                yield return key;
            }
        }

        private ServiceVariable<RemoteService> remoteService = new ServiceVariable<RemoteService>("Service", "The remote service to operate");
        private Variable<RemoteKey> key = new Variable<RemoteKey>("Key", RemoteKey.Ok, "The key to send to the specified service");

        public override NodeResult Run()
        {
            if (remoteService.Value == null)
            {
                Log.Error("You must specify a remote service to work with");
                return NodeResult.Fail;
            }

            DeviceManager.UpdateServiceInfo(remoteService.Value).Wait();
            RemoteService service = remoteService.Service;

            service.Press(key.Value);

            return NodeResult.Success;
        }
    }
}