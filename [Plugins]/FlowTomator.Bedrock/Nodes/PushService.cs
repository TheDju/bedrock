﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Common;

namespace FlowTomator.Bedrock
{
    [Node("Push URI", "Bedrock", "Push the specified URI to the specified device")]
    public class PushUriTask : Task
    {
        public override IEnumerable<Variable> Inputs
        {
            get
            {
                yield return pushService;
                yield return uri;
            }
        }

        private ServiceVariable<PushService> pushService = new ServiceVariable<PushService>("Service", "The push service to operate");
        private Variable<string> uri = new Variable<string>("URI", "", "The URI to push on the specified device");

        public PushUriTask()
        {
            Bedrock.Initialize();
        }

        public override NodeResult Run()
        {
            if (pushService.Value == null)
            {
                Log.Error("You must specify a push service to operate");
                return NodeResult.Fail;
            }
            
            Uri result;
            if (uri.Value == null || !Uri.TryCreate(uri.Value, UriKind.RelativeOrAbsolute, out result))
            {
                Log.Error("You must specify a valid URI to push");
                return NodeResult.Fail;
            }

            DeviceManager.UpdateServiceInfo(pushService.Value).Wait();
            PushService service = pushService.Service;

            service.PushUri(result);
            
            return NodeResult.Success;
        }
    }
}