﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Bedrock;
using FlowTomator.Common;

namespace FlowTomator.Bedrock
{
    public class ServiceVariable : Variable<ServiceInfo>
    {
        public Type ServiceType { get; }
        public Service Service
        {
            get
            {
                return Value.Service;
            }
        }

        public ServiceVariable(string name, Type serviceType) : base(name)
        {
            ServiceType = serviceType;
        }
        public ServiceVariable(string name, Type serviceType, string description) : base(name, null, description)
        {
            ServiceType = serviceType;
        }
    }

    public class ServiceVariable<T> : ServiceVariable where T : Service
    {
        public new T Service
        {
            get
            {
                return Value.Service as T;
            }
        }

        public ServiceVariable(string name) : base(name, typeof(T)) { }
        public ServiceVariable(string name, string description) : base(name, typeof(T), description) { }
    }
}