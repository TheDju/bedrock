﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using Bedrock.Common;
using Bedrock.Shared;
using FlowTomator.Bedrock;
using FlowTomator.Common;

namespace FlowTomator.Bedrock
{
    public static class Bedrock
    {
        static Bedrock()
        {
            SQLiteConnection dbConnection = new SQLiteConnection("Data Source=:memory:;Version=3;New=True;");
            dbConnection.Open();

            DeviceManager.CheckDatabase(dbConnection);
            DeviceManager.Initialize(dbConnection);
        }

        public static void Initialize() { }
    }
}