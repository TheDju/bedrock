﻿namespace Bedrock
{
    export class HttpRemotingClient
    {
        public Host: string;
        public Port: number;

        private remoteIds: number[];
        private remoteProxies: any[];

        constructor(host: string, port: number)
        {
            this.Host = host;
            this.Port = port;

            this.remoteIds = [];
            this.remoteProxies = [];
        }

        getObject(name: string) : any
        {
            var response = this.getQuery('/' + name);

            var result = response.getElementsByTagName('Result')[0].children[0];
            var object = this.unwrapObject(result);

            return object;
        }

        // Proxy members
        private methodProxy(id: number, proxy, name: string, params: any[])
        {
            var callDocument = document.implementation.createDocument(null, 'Call', null);
            var callElement = callDocument.documentElement;

            callElement.setAttribute('Method', name);

            var paramsCount = 0;
            if (params != null)
                for (; paramsCount < 8; paramsCount++)
                    if (params[paramsCount] === undefined)
                        break;

            if (paramsCount > 0)
            {
                for (var i = 0; i < paramsCount; i++)
                {
                    var param = params[i];
                    var paramType = Utils.getClrTypeName(param);

                    if (paramType == null)
                        throw 'Could not embed param of type ' + typeof paramType;

                    var paramElement = callDocument.createElement('Parameter');

                    paramElement.setAttribute('Type', paramType);
                    paramElement.appendChild(this.wrapObject(param, callDocument));

                    callElement.appendChild(paramElement);
                }
            }

            var response = this.postQuery('/' + id, callElement);

            var result = response.documentElement;
            if (result.tagName == 'Exception')
                throw this.unwrapException(result);
            else if (result.tagName == 'Response')
                return this.unwrapObject(result.children[0].children[0]);
        }

        // HTTP queries
        private getQuery(url: string)
        {
            var base = 'http://' + this.Host + ':' + this.Port;

            var request = new XMLHttpRequest();
            request.open('GET', base + url, false);
            request.send(null);

            if (request.status === 200)
                return request.responseXML;
            else
                throw 'Failed get query';
        }
        private postQuery(url: string, content)
        {
            var base = 'http://' + this.Host + ':' + this.Port;
            content = new XMLSerializer().serializeToString(content);

            var request = new XMLHttpRequest();
            request.open('POST', base + url, false);
            request.send(content);

            if (request.status === 200)
                return request.responseXML;
            else
                throw 'Failed post query';
        }

        // Serialization
        private wrapObject(object, document: Document): any
        {
            var jsType = typeof object;
            var clrType = Utils.getClrTypeName(object);

            if (jsType == 'string' || object instanceof ValueType)
            {
                var valueElement = document.createElement('Value');

                valueElement.setAttribute('Type', clrType);
                valueElement.appendChild(document.createTextNode(object));

                return valueElement;
            }

            var index = this.remoteProxies.indexOf(object);
            if (index >= 0)
            {
                var remoteObjectElement = document.createElement('RemoteObject');

                remoteObjectElement.setAttribute('Id', this.remoteIds[index].toString());

                return remoteObjectElement;
            }

            throw 'NotImplemented';
        }
        private unwrapObject(node)
        {
            if (node.tagName == 'Null')
                return null;

            if (node.tagName == 'Value')
            {
                var type = node.getAttribute("Type");
                var content = node.childNodes[0].textContent;

                if (type == 'System.String')
                    return content;
                else if (type == 'System.Guid')
                    return content;
                else if (type == 'System.Int32')
                    return parseInt(content);
                else
                    throw 'Unhandled value type ' + type;
            }

            if (node.tagName == 'Array')
            {
                var type = node.getAttribute("Type");
                var lengthAttribute = node.getAttribute("Length");
                var length = parseInt(lengthAttribute);

                var array = [];

                for (var subNode of node.children)
                    array.push(this.unwrapObject(subNode));

                return array;
            }

            if (node.tagName == 'RemoteObject')
            {
                var idAttribute = node.getAttribute("Id");
                var id = parseInt(idAttribute);

                var index = this.remoteIds.indexOf(id);
                if (index >= 0)
                    return this.remoteProxies[index];

                var parentNode = node.getElementsByTagName('Type')[0];
                var parentTypeName = parentNode.getAttribute('FullName');
                var parentType = Utils.getType(parentTypeName);

                if (parentType == null)
                {
                    for (parentNode of parentNode.getElementsByTagName('Parent'))
                    {
                        if (parentType != null)
                            break;

                        var typeName = parentNode.getAttribute('FullName');
                        var parentType = Utils.getType(typeName);
                    }
                }

                if (parentType == null)
                    throw 'Could not find object type ' + parentTypeName;

                var proxy = this.createProxy(id, parentType);

                this.remoteIds.push(id);
                this.remoteProxies.push(proxy);

                return proxy;
            }

            throw 'Unable to unwrap this ' + node.tagName;
        }
        private unwrapException(node)
        {
            var type = node.getAttribute('Type');

            var messageElement = node.getElementsByTagName('Message')[0];
            var message = messageElement.childNodes[0].textContent;

            var stackTraceElement = node.getElementsByTagName('StackTrace')[0];
            var stackTrace = stackTraceElement.childNodes[0].textContent;

            return type + ': ' + message + '\n' + stackTrace;
        }

        // Utils
        private createProxy(id: number, type)
        {
            // Build an instance
            var instance = Object.call(type);

            // Replace functions
            for (var member of Utils.getProperties(type.prototype))
            {
                if (member == 'constructor')
                    continue;
                    
                var property = Utils.getProperty(type.prototype, member);

                // Method
                if (property.value != null && typeof property.value == 'function')
                {
                    property.value = ((_id, _instance, _member) => (a, b, c, d, e, f, g, h) => this.methodProxy(_id, _instance, _member, [a, b, c, d, e, f, g, h]))(id, instance, member);
                    Object.defineProperty(instance, member, property);
                }

                // Property
                if (property.get != null)
                {
                    property.get = ((_id, _instance, _member) => () => this.methodProxy(_id, _instance, 'get_' + _member, null))(id, instance, member);
                    Object.defineProperty(instance, member, property);
                }
                if (property.set != null)
                {
                    property.set = ((_id, _instance, _member) => (value) => this.methodProxy(_id, _instance, 'set_' + _member, [value]))(id, instance, member);
                    Object.defineProperty(instance, member, property);
                }
            }

            return instance;
        }
    }
}