﻿namespace Bedrock.Common
{
    export class ServiceInfo
    {
        get Id(): string
        {
            return null;
        }
        get Name(): string
        {
            return null;
        }
        get Description(): string
        {
            return null;
        }

        get Device(): Device
        {
            return null;
        }
        get Service(): Service
        {
            return null;
        }

        toString()
        {
            return this.Name;
        }
    }
}