var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        class Device {
            get Id() {
                return null;
            }
            get Name() {
                return null;
            }
            get Description() {
                return null;
            }
            get Services() {
                return null;
            }
            toString() {
                return this.Name;
            }
        }
        Common.Device = Device;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
