﻿namespace Bedrock.Common
{
    export abstract class Device
    {
        get Id(): string
        {
            return null;
        }
        get Name(): string
        {
            return null;
        }
        get Description(): string
        {
            return null;
        }

        get Services(): Service[]
        {
            return null;
        }

        toString()
        {
            return this.Name;
        }
    }
}