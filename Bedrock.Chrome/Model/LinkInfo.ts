﻿namespace Bedrock.Common
{
    export abstract class PathNodeInfo
    {
        get Destination(): DeviceInfo
        {
            return null;
        }
    }

    export class ConnectionInfo extends PathNodeInfo
    {
        get Method(): string
        {
            return null;
        }
        get Address(): string
        {
            return null;
        }
    }

    export class LinkInfo extends PathNodeInfo
    {
        get Source(): DeviceInfo
        {
            return null;
        }
    }
}