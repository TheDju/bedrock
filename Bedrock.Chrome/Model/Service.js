var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        class Service {
            get Id() {
                return null;
            }
            get Name() {
                return null;
            }
            get Description() {
                return null;
            }
            toString() {
                return this.Name;
            }
        }
        Common.Service = Service;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
