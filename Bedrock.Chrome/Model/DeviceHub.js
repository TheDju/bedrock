var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        class DeviceHub extends Common.Device {
            get Devices() {
                return null;
            }
        }
        Common.DeviceHub = DeviceHub;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
