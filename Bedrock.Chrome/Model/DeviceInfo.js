var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        class DeviceInfo {
            constructor(device) {
                //this.Id = device.Id;
                //this.Device = device;
            }
            get Id() {
                return null;
            }
            get Name() {
                return null;
            }
            get Description() {
                return null;
            }
            get Device() {
                return null;
            }
            get Services() {
                return null;
            }
            get Devices() {
                return null;
            }
            toString() {
                return this.Name;
            }
        }
        Common.DeviceInfo = DeviceInfo;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
