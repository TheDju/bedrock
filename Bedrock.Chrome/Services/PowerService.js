var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        class PowerService extends Common.Service {
            PowerOn() { }
            PowerOff() { }
            Switch() { }
        }
        Common.PowerService = PowerService;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
