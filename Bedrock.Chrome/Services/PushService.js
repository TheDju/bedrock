var Bedrock;
(function (Bedrock) {
    var Common;
    (function (Common) {
        /**
         * A simple Bedrock service to push data to a device
         */
        class PushService extends Common.Service {
            PushText(text) { }
            PushUri(uri) { }
        }
        Common.PushService = PushService;
    })(Common = Bedrock.Common || (Bedrock.Common = {}));
})(Bedrock || (Bedrock = {}));
