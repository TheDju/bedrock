﻿namespace Bedrock.Common
{
    export abstract class PowerService extends Service
    {
        PowerOn() { }
        PowerOff() { }
        Switch() { }
    }
}