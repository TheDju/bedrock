﻿namespace Bedrock.Common
{
    /**
     * A simple Bedrock service to push data to a device
     */
    export abstract class PushService extends Service
    {
        PushText(text: string) { }
        PushUri(uri: Uri) { }
    }
}