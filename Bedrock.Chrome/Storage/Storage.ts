﻿namespace Bedrock.Shared
{
    export abstract class Storage
    {
        abstract get Devices(): Bedrock.Common.DeviceInfo[];
        abstract get Services(): Bedrock.Common.ServiceInfo[];
        abstract get Connections(): Bedrock.Common.PathNodeInfo[];

        abstract Load();
        abstract Save();
    }
}