class ValueType {
}
class Uri extends ValueType {
    constructor(value) {
        super();
        this.value = value;
    }
    toString() {
        return this.value;
    }
}
var Utils;
(function (Utils) {
    function getType(name) {
        var parts = name.split('.');
        var type = window;
        for (var part of parts) {
            if (!type.hasOwnProperty(part))
                return null;
            type = type[part];
        }
        return type;
    }
    Utils.getType = getType;
    function getClrTypeName(object) {
        var jsType = typeof object;
        switch (jsType) {
            case 'string': return 'System.String';
            case 'object':
                if (object instanceof Uri)
                    return 'System.Uri';
                break;
        }
        return null;
    }
    Utils.getClrTypeName = getClrTypeName;
    function getProperty(type, name) {
        while (type != null) {
            var property = Object.getOwnPropertyDescriptor(type, name);
            if (property != null)
                return property;
            type = type.__proto__;
        }
        return null;
    }
    Utils.getProperty = getProperty;
    function getProperties(type) {
        var properties = [];
        while (type != null && type != Object.prototype) {
            for (var property of Object.getOwnPropertyNames(type))
                properties.push(property);
            type = type.__proto__;
        }
        return properties;
    }
    Utils.getProperties = getProperties;
})(Utils || (Utils = {}));
