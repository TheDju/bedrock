﻿class ValueType
{
}
class Uri extends ValueType
{
    private value: string;

    constructor(value: string)
    {
        super();
        this.value = value;
    }

    toString(): string
    {
        return this.value;
    }
}

namespace Utils
{
    export function getType(name: string)
    {
        var parts = name.split('.');

        var type = window;
        for (var part of parts)
        {
            if (!type.hasOwnProperty(part))
                return null;

            type = type[part];
        }

        return type;
    }
    export function getClrTypeName(object)
    {
        var jsType = typeof object;

        switch (jsType)
        {
            case 'string': return 'System.String';
            case 'object':

                if (object instanceof Uri) return 'System.Uri';

                break;
        }

        return null;
    }

    export function getProperty(type, name: string): PropertyDescriptor
    {
        while (type != null)
        {
            var property = Object.getOwnPropertyDescriptor(type, name);
            if (property != null)
                return property;

            type = type.__proto__;
        }

        return null;
    }
    export function getProperties(type): string[]
    {
        var properties = [];

        while (type != null && type != Object.prototype)
        {
            for (var property of Object.getOwnPropertyNames(type))
                properties.push(property);

            type = type.__proto__;
        }

        return properties;
    }
}