﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Android.Content;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.IoT;
using Bedrock.Shared;

using IoT.Common;

namespace Bedrock.Android
{
    public class ServiceManager
    {
        public static int GetDrawableForType(Type type)
        {
            if (typeof(PowerService).IsAssignableFrom(type))
                return Resource.Drawable.ic_power_settings_new;
            else if (typeof(RemoteService).IsAssignableFrom(type))
                return Resource.Drawable.ic_settings_remote;
            else if (typeof(SensorService).IsAssignableFrom(type))
                return Resource.Drawable.ic_memory;
            else if (typeof(StorageService).IsAssignableFrom(type))
                return Resource.Drawable.ic_folder_shared;
            else if (typeof(ClipboardService).IsAssignableFrom(type))
                return Resource.Drawable.ic_content_paste;
            else if (typeof(PushService).IsAssignableFrom(type))
                return Resource.Drawable.ic_notifications;

            else
                return 0;
        }

        public static Task TriggerAction(Context context, ServiceInfo serviceInfo)
        {
            if (serviceInfo.Service == null)
                DeviceManager.UpdateServiceInfo(serviceInfo).Wait();

            if (serviceInfo.Service is PowerService)
            {
                return Task.Run(() => (serviceInfo.Service as PowerService).Switch());
            }
            else if (serviceInfo.Service is RemoteService)
            {
                Intent intent = new Intent(context, typeof(RemoteServiceActivity));
                intent.PutExtra("Service.Id", serviceInfo.Id.ToString());

                context.StartActivity(intent);
            }
            else if (serviceInfo.Service is StorageService)
            {
                Intent intent = new Intent(context, typeof(StorageServiceActivity));
                intent.PutExtra("Service.Id", serviceInfo.Id.ToString());

                context.StartActivity(intent);
            }
            else if (serviceInfo.Service is SensorService)
            {
                return ShowSensorForm(context, serviceInfo);
            }
            else
            {
                return Task.Run(() =>
                {
                    throw new Exception("Unknown service type");
                });
            }

            return Task.Run(() => { });
        }

        private static async Task ShowSensorForm(Context context, ServiceInfo serviceInfo)
        {
            //await Task.Run(() =>
            {
                ViewGroup.LayoutParams layoutParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);

                LinearLayout linearLayout = new LinearLayout(context) { Orientation = Orientation.Vertical };
                linearLayout.SetPadding(32, 64, 32, 32);

                TextView sensorText = new TextView(context) { LayoutParameters = layoutParameters };
                linearLayout.AddView(sensorText);

                // Read sensor value
                SensorService service = serviceInfo.Service as SensorService;
                Task<Data> task = service.GetDataAsync();
                if (!task.Wait(2500))
                    throw new Exception("Could not get sensor data");

                sensorText.Text = task.Result.Value.ToString();

                // Build and show dialog
                new global::Android.Support.V7.App.AlertDialog.Builder(context)
                    .SetTitle(serviceInfo.Name)
                    .SetView(linearLayout)
                    .SetPositiveButton("OK", (s, e) => { })
                    .Show();
            }//);
        }
    }
}