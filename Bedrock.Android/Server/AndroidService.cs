using System;

using Android.App;
using Android.Content;
using Android.OS;

using Bedrock.Common;
using System.IO;

namespace Bedrock.Android
{
    using Service = global::Android.App.Service;
    using BedrockService = Bedrock.Common.Service;

    [Service]
    public class AndroidService : Service
    {
        private AndroidServer server;

        public override void OnCreate()
        {
            base.OnCreate();

            // Setup Bedrock android device
            if (server == null)
            {
                server = AndroidServer.Instance;

                Log.Debug("Android server : {0}", server.Id);

                server.TryAddService(() => new StorageService() { Name = "Shared storage", Description = "Device shared storage", Storage = new BasicStorage(new DirectoryInfo(".")) });
                server.TryAddService(() => new AndroidClipboardService(this) { Name = "Clipboard", Description = "Shared clipboard" });
                server.TryAddService(() => new AndroidPushService(this) { Name = "Notifications", Description = "Push notifications service" });
                server.TryAddService(() => new LightSensor(this) { Name = "Ambient light" });
                server.TryAddService(() => new PressureSensor(this) { Name = "Atmospheric pressure" });
                server.TryAddService(() => new AccelerometerSensor(this) { Name = "Accelerometer" });
                server.TryAddService(() => new BatteryLevelSensor(this) { Name = "Battery level", Description = "Device current battery level" });

                server.Start();
            }
        }
        public override void OnDestroy()
        {
            base.OnDestroy();

            // Stop the remoting server
            server.Stop();
            server = null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            // Show notification and set foreground service
            //Notification notification = new Notification(Resource.Mipmap.ic_launcher, "Bedrock service is running");
            //notification.Priority = (int)NotificationPriority.Min;

            //Intent defaultActivity = new Intent(this, typeof(DevicesActivity));
            //PendingIntent defaultIntent = PendingIntent.GetActivity(this, 0, defaultActivity, 0);
            //notification.SetLatestEventInfo(this, "Bedrock", "Bedrock service is running", defaultIntent);

            //StartForeground((int)NotificationFlags.ForegroundService, notification);

            return StartCommandResult.Sticky;
        }
        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }
    }
}