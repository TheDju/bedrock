﻿using Android.Content;
using Android.Hardware;

using IoT.Common;

namespace Bedrock.Android
{
    public class LightSensor : AndroidSensor
    {
        public override Unit Unit => Units.Luminance.Lux;

        public LightSensor(Context context) : base(context, SensorType.Light) { }
    }
}