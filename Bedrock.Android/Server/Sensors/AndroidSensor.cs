﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Hardware;
using Android.Runtime;
using Bedrock.Common;
using Bedrock.IoT;
using Bedrock.Shared;
using IoT.Common;

namespace Bedrock.Android
{
    public class SensorEventListener : Java.Lang.Object, ISensorEventListener
    {
        public delegate void AccuracyChangedCallback(Sensor sensor, SensorStatus accuracy);
        public delegate void SensorChangedCallback(SensorEvent e);

        public event AccuracyChangedCallback AccuracyChanged;
        public event SensorChangedCallback SensorChanged;

        public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
        {
            AccuracyChanged?.Invoke(sensor, accuracy);
        }
        public void OnSensorChanged(SensorEvent e)
        {
            SensorChanged?.Invoke(e);
        }
    }

    public abstract class AndroidSensor : SensorService
    {
        public Sensor Sensor { get; protected set; }

        protected SensorManager sensorManager;
        protected float[] sensorValues;
        protected ManualResetEvent sensorEvent = new ManualResetEvent(false);

        private SensorEventListener sensorEventListener;

        public AndroidSensor(Context context)
        {
            sensorManager = (SensorManager)context.GetSystemService(Context.SensorService);
        }
        public AndroidSensor(Context context, SensorType sensorType)
        {
            sensorManager = (SensorManager)context.GetSystemService(Context.SensorService);
            Sensor = sensorManager.GetDefaultSensor(sensorType);

            sensorEventListener = new SensorEventListener();
            sensorEventListener.AccuracyChanged += OnAccuracyChanged;
            sensorEventListener.SensorChanged += OnSensorChanged;

            if (!sensorManager.RegisterListener(sensorEventListener, Sensor, SensorDelay.Normal))
                throw new NotSupportedException("No sensor of type " + sensorType + " is available on this device");

            Description = Sensor.Name;
        }

        public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
        {
        }
        public void OnSensorChanged(SensorEvent e)
        {
            sensorValues = e.Values.ToArray();
            sensorEvent.Set();
        }

        public override Data GetData()
        {
            sensorEvent.WaitOne();
            return GetDataFromValues(sensorValues);
        }
        public virtual Data GetDataFromValues(float[] values)
        {
            return new Data(GetValueFromValues(values), Unit);
        }
        public virtual double GetValueFromValues(float[] values)
        {
            return values[0];
        }

        public void Dispose()
        {
            sensorManager.UnregisterListener(sensorEventListener);
        }
    }
}