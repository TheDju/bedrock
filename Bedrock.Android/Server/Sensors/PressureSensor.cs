﻿using Android.Content;
using Android.Hardware;

using IoT.Common;

namespace Bedrock.Android
{
    public class PressureSensor : AndroidSensor
    {
        public override Unit Unit => Units.Luminance.Lux;

        public PressureSensor(Context context) : base(context, SensorType.Pressure) { }
    }
}