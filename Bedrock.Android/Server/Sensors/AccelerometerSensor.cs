﻿using Android.Content;
using Android.Hardware;

using IoT.Common;

namespace Bedrock.Android
{
    public class AccelerometerSensor : AndroidSensor
    {
        public override Unit Unit => Units.Size.Meter;

        public AccelerometerSensor(Context context) : base(context, SensorType.Accelerometer) { }
    }
}