﻿using Android.Content;
using Android.Hardware;
using Android.OS;

using Bedrock.IoT;
using IoT.Common;

namespace Bedrock.Android
{
    public class BatteryLevelSensor : SensorService
    {
        public Sensor Sensor { get; protected set; }
        public override Unit Unit => Units.Base.Percent;

        private Context context;

        public BatteryLevelSensor(Context context)
        {
            this.context = context;
        }

        public override Data GetData()
        {
            Intent batteryIntent = context.RegisterReceiver(null, new IntentFilter(Intent.ActionBatteryChanged));
            int level = batteryIntent.GetIntExtra(BatteryManager.ExtraLevel, -1);
            int scale = batteryIntent.GetIntExtra(BatteryManager.ExtraScale, -1);

            return new Data((float)level / scale, Unit);
        }
    }
}