using System;
using System.Collections.Generic;

using Bedrock.Common;

namespace Bedrock.Android
{
    public class AndroidServer : DeviceHub
    {
        public static AndroidServer Instance { get; } = new AndroidServer();

        public override IEnumerable<Service> Services => services;
        public override IEnumerable<Device> Devices => devices;

        public bool HttpServerEnabled { get; set; } = true;
        public ushort HttpServerPort { get; set; } = 2480;
        public bool TcpServerEnabled { get; set; } = true;
        public ushort TcpServerPort { get; set; } = 2424;

        private List<Service> services = new List<Service>();
        private List<Device> devices = new List<Device>();
        
        private AndroidServer()
        {
            Name = Environment.MachineName;
        }

        public void TryAddService(Func<Service> serviceBuilder)
        {
            try
            {
                Service service = serviceBuilder();
                services.Add(service);
            }
            catch { }
        }
        public void TryAddDevice(Func<Device> deviceBuilder)
        {
            try
            {
                Device device = deviceBuilder();
                devices.Add(device);
            }
            catch { }
        }
    }
}