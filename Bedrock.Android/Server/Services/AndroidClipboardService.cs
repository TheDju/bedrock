using System;
using System.Threading;

using Android.Content;

using Bedrock.Common;

namespace Bedrock.Android
{
    public class AndroidClipboardService : ClipboardService
    {
        public override ClipboardData Data
        {
            get
            {
                lock (mutex)
                    return new ClipboardData(ClipboardDataType.Text, value);
            }
            set
            {
                if (value.Type != ClipboardDataType.Text)
                    throw new NotSupportedException("Android does not support non-text data in its clipboard manager");

                lock (mutex)
                    clipboardManager.Text = value.Value.ToString();
            }
        }

        private ClipboardManager clipboardManager;
        private string value;
        private Thread watcherThread;
        private object mutex = new object();

        public AndroidClipboardService(Context context)
        {
            clipboardManager = (ClipboardManager)context.GetSystemService(Context.ClipboardService);
            lock (mutex)
                value = clipboardManager.HasText ? clipboardManager.Text : null;

            // Watcher loop
            watcherThread = new Thread(WatcherThread_Loop);
            watcherThread.SetApartmentState(ApartmentState.STA);
            watcherThread.Start();
        }

        private void WatcherThread_Loop()
        {
            while (Thread.CurrentThread.ThreadState == ThreadState.Running)
            {
                lock (mutex)
                {
                    if (!clipboardManager.HasText)
                        continue;

                    string text = clipboardManager.Text;
                    if (value != text)
                    {
                        value = text;
                        OnCopy(new ClipboardData(ClipboardDataType.Text, text));
                    }
                }

                Thread.Sleep(200);
            }
        }
    }
}