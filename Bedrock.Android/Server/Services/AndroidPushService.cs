using System;

using Android.App;
using Android.Content;
using Android.Support.V4.App;

using Bedrock.Common;

namespace Bedrock.Android
{
    public class AndroidPushService : PushService
    {
        private Context context;

        public AndroidPushService(Context context)
        {
            this.context = context;
        }

        public override void PushText(string text)
        {
            new Notification.Builder(context)
                .SetContentText(text)
                //.AddAction(0, "Copier", )

                .Build();
        }
        public override void PushUri(Uri uri)
        {
            NotificationManager notificationManager = context.GetSystemService(Context.NotificationService) as NotificationManager;

            Intent intent = new Intent(Intent.ActionView);
            intent.SetData(global::Android.Net.Uri.Parse(uri.ToString()));

            PendingIntent pendingIntent = PendingIntent.GetActivity(context, 0, intent, PendingIntentFlags.UpdateCurrent);

            Notification notification = new NotificationCompat.Builder(context)
                .SetContentTitle("Bedrock push notification")
                .SetContentText(uri.ToString())
                .SetContentIntent(pendingIntent)
                .SetAutoCancel(true)
                .SetSmallIcon(Resource.Drawable.ic_device_hub)
                .Build();

            notificationManager.Notify(Util.Hash(uri.ToString()), notification);
        }
        public override void PushImage(byte[] buffer)
        {
            throw new NotImplementedException();
        }
    }
}