using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Graphics.Drawable;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Android
{
    [BroadcastReceiver(Label = "Service", Exported = true)]
    [IntentFilter(new[] { AppWidgetManager.ActionAppwidgetUpdate })]
    [MetaData("android.appwidget.provider", Resource = "@xml/servicewidget")]
    public class ServiceWidget : AppWidgetProvider
    {
        public const string ActionTriggerService = "net.thedju.Bedrock.ACTION_TRIGGER_SERVICE";

        private static ComponentName componentName;
        public static ComponentName GetComponentName(Context context)
        {
            var javaClass = Java.Lang.Class.FromType(typeof(ServiceWidget));
            return componentName ?? (componentName = new ComponentName(context, javaClass.CanonicalName));
        }

        public override void OnUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
        {
            Update(context, appWidgetManager, appWidgetIds);
        }
        public override void OnReceive(Context context, Intent intent)
        {

            if (intent?.Action == ActionTriggerService)
            {
                string serviceIdText = intent.GetStringExtra("Service.Id");
                Guid serviceId = Guid.Parse(serviceIdText);

                ServiceInfo serviceInfo = DeviceManager.FindServiceById(serviceId);
                ServiceManager.TriggerAction(context, serviceInfo).ContinueWith(t =>
                {
                    if (t.IsFaulted)
                    {
                        Exception[] exceptions = t.Exception.InnerExceptions.GetAllExceptions().ToArray();
                        string exceptionMessages = exceptions.Select(e => e.Message).Join(System.Environment.NewLine);
                        
                        //Toast.MakeText(context, "Unable to use service " + serviceInfo.Name + System.Environment.NewLine + exceptionMessages, ToastLength.Long)
                        //     .Show();
                    }
                });
            }
            else
                base.OnReceive(context, intent);
        }

        public static void Update(Context context, AppWidgetManager appWidgetManager)
        {
            ComponentName widgetComponent = GetComponentName(context);
            int[] appWidgetIds = appWidgetManager.GetAppWidgetIds(widgetComponent);

            Update(context, appWidgetManager, appWidgetIds);
        }
        public static void Update(Context context, AppWidgetManager appWidgetManager, params int[] appWidgetIds)
        {
            foreach (int appWidgetId in appWidgetIds)
            {
                ServiceInfo serviceInfo = DeviceManager.FindServiceByWidgetId(appWidgetId);
                if (serviceInfo == null)
                    continue;

                RemoteViews remoteViews = new RemoteViews(context.PackageName, Resource.Layout.ServiceWidget);

                Intent intent = new Intent(context, typeof(ServiceWidget));
                intent.SetAction(ActionTriggerService);
                intent.PutExtra("Service.Id", serviceInfo.Id.ToString());

                PendingIntent pendingIntent = PendingIntent.GetBroadcast(context, serviceInfo.Id.GetHashCode(), intent, 0);
                remoteViews.SetOnClickPendingIntent(Resource.Id.ServiceWidget_Button, pendingIntent);

                // Update widget UI
                remoteViews.SetImageViewResource(Resource.Id.ServiceWidget_Icon, ServiceManager.GetDrawableForType(serviceInfo.Type));
                remoteViews.SetTextViewText(Resource.Id.ServiceWidget_Name, serviceInfo.Device.Name);

                appWidgetManager.UpdateAppWidget(appWidgetId, remoteViews);
            }
        }
    }
}