using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Android
{
    using PopupMenu = global::Android.Support.V7.Widget.PopupMenu;

    public enum ServicesViewType
    {
        LargeServices,
        SmallServices
    }

    public class ServiceViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Icon { get; private set; }
        public TextView Name { get; private set; }
        public TextView Description { get; private set; }
        public TextView Alt { get; private set; }
        public View Menu { get; private set; }

        public ServiceViewHolder(View itemView) : base(itemView)
        {
            Icon = itemView.FindViewById<ImageView>(Resource.Id.ServiceItem_Icon);
            Name = itemView.FindViewById<TextView>(Resource.Id.ServiceItem_Name);
            Description = itemView.FindViewById<TextView>(Resource.Id.ServiceItem_Description);
            Alt = itemView.FindViewById<TextView>(Resource.Id.ServiceItem_Alt);
            Menu = itemView.FindViewById(Resource.Id.ServiceItem_Menu);
        }
    }

    public class ServicesAdapter : RecyclerView.Adapter, View.IOnClickListener
    {
        public override int ItemCount
        {
            get
            {
                return services.Length;
            }
        }
        public ServicesViewType ViewType { get; }

        public event EventHandler<ServiceInfo> ServiceClick;

        private ServiceInfo[] services;
        private List<ServiceViewHolder> viewHolders = new List<ServiceViewHolder>();
        private object refreshMutex = new object();
        
        public ServicesAdapter(IEnumerable<ServiceInfo> services, ServicesViewType viewType = ServicesViewType.LargeServices)
        {
            this.services = services.ToArray();
            ViewType = viewType;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView;
            
            if (ViewType == ServicesViewType.LargeServices)
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ServiceItem, parent, false);
            else
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.SmallServiceItem, parent, false);

            itemView.SetOnClickListener(this);
            return new ServiceViewHolder(itemView);
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ServiceViewHolder viewHolder = holder as ServiceViewHolder;
            ServiceInfo serviceInfo = services[position];

            viewHolder.Name.Text = serviceInfo.Name;
            viewHolder.Description.Text = serviceInfo.Description ?? "No description";
            viewHolder.Icon.SetImageResource(ServiceManager.GetDrawableForType(serviceInfo.Type));

            if (ViewType == ServicesViewType.LargeServices)
            {
                viewHolder.Alt.Text = "";
                viewHolder.Menu.Click += Menu_Click;
            }

            if (serviceInfo.Type == typeof(PowerService))
            {
                viewHolder.Icon.SetImageResource(Resource.Drawable.ic_power_settings_new);

                if (ViewType == ServicesViewType.LargeServices)
                {
                    viewHolder.Menu.Visibility = ViewStates.Visible;
                    viewHolder.Alt.Text = !serviceInfo.Data.ContainsKey(nameof(PowerService.Value)) ? "" : ("Currently " + ((bool)serviceInfo.Data[nameof(PowerService.Value)] == true ? "on" : "off"));
                }
            }
            else if (serviceInfo.Type == typeof(RemoteService))
                viewHolder.Icon.SetImageResource(Resource.Drawable.ic_settings_remote);

            viewHolders.Add(viewHolder);
        }

        public void OnClick(View view)
        {
            ServiceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view);
            ServiceInfo serviceInfo = services[viewHolder.AdapterPosition];

            if (ViewType == ServicesViewType.LargeServices)
            {
                Task task = ServiceManager.TriggerAction(view.Context, serviceInfo);
                
                task.ContinueWith(t =>
                    {
                        DeviceManager.UpdateServiceInfo(serviceInfo).Wait();
                    }, TaskContinuationOptions.OnlyOnRanToCompletion)
                    .ContinueWith(t =>
                    {
                        lock (refreshMutex)
                        {
                            viewHolder.Menu.Click -= Menu_Click;
                            NotifyItemChanged(viewHolder.AdapterPosition);
                        }
                    }, TaskContinuationOptions.OnlyOnRanToCompletion);

                task.ContinueWith(t =>
                    {
                        Exception[] exceptions = t.Exception.InnerExceptions.GetAllExceptions().ToArray();
                        string exceptionMessages = exceptions.Select(e => e.Message).Join(System.Environment.NewLine);

                        Snackbar.Make(view, "Unable to use service " + System.Environment.NewLine + exceptionMessages, Snackbar.LengthShort)
                                .SetAction("Retry", v => OnClick(view))
                                .Show();
                    }, TaskContinuationOptions.OnlyOnFaulted);
            }
            else
            {
                ServiceClick?.Invoke(this, serviceInfo);
            }
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            View view = sender as View;
            ServiceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            ServiceInfo service = services[viewHolder.AdapterPosition];

            PopupMenu menu = new PopupMenu(view.Context, view);
            menu.MenuItemClick += (s, a) => Menu_MenuItemClick(view, a);

            if (service.Service is PowerService)
            {
                menu.Menu.Add(1, Resource.Id.PowerServiceMenu_TurnOn, 1, "Turn on");
                menu.Menu.Add(1, Resource.Id.PowerServiceMenu_TurnOff, 2, "Turn off");
                menu.Menu.Add(1, Resource.Id.PowerServiceMenu_Switch, 3, "Switch");
            }

            menu.Show();
        }
        private void Menu_MenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            View view = sender as View;
            ServiceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            ServiceInfo service = services[viewHolder.AdapterPosition];

            Task.Run(() =>
                {
                    if (service.Service is PowerService)
                    {
                        PowerService powerService = service.Service as PowerService;

                        if (e.Item.ItemId == Resource.Id.PowerServiceMenu_TurnOn) powerService.PowerOn();
                        if (e.Item.ItemId == Resource.Id.PowerServiceMenu_TurnOff) powerService.PowerOff();
                        if (e.Item.ItemId == Resource.Id.PowerServiceMenu_Switch) powerService.Switch();
                    }

                    DeviceManager.UpdateServiceInfo(service).ContinueWith(u =>
                    {
                        lock (refreshMutex)
                        {
                            viewHolder.Menu.Click -= Menu_Click;
                            NotifyItemChanged(viewHolder.AdapterPosition);
                        }
                    });
                })
                .ContinueWith(t =>
                {
                    if (t.IsFaulted)
                        Snackbar.Make(view, "Error while using " + service.Name + ". " + t.Exception.Message, Snackbar.LengthShort).Show();
                });
        }
    }
}