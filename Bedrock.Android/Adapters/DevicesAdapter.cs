using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Android
{
    using global::Android.Utilities;
    using PopupMenu = global::Android.Support.V7.Widget.PopupMenu;

    public enum DevicesViewType
    {
        LargeDevices,
        SmallDevices,
        SmallDevicesAndServices
    }

    public class DeviceViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Icon { get; private set; }
        public TextView Name { get; private set; }
        public TextView Description { get; private set; }
        public ImageView Menu { get; private set; }
        public ImageView Expander { get; private set; }
        public RecyclerView Services { get; private set; }

        public DeviceViewHolder(View itemView) : base(itemView)
        {
            Icon = itemView.FindViewById<ImageView>(Resource.Id.DeviceItem_Icon);
            Name = itemView.FindViewById<TextView>(Resource.Id.DeviceItem_Name);
            Description = itemView.FindViewById<TextView>(Resource.Id.DeviceItem_Description);
            Menu = itemView.FindViewById<ImageView>(Resource.Id.DeviceItem_Menu);
            Expander = itemView.FindViewById<ImageView>(Resource.Id.DeviceItem_Expander);
            Services = itemView.FindViewById<RecyclerView>(Resource.Id.DeviceItem_ServiceList);
        }
    }

    public class DevicesAdapter : RecyclerView.Adapter, View.IOnClickListener
    {
        public override int ItemCount
        {
            get
            {
                return devices.Length;
            }
        }
        public DevicesViewType ViewType { get; }

        public event EventHandler<DeviceInfo> DeviceClick;

        private DeviceInfo[] devices;
        private List<DeviceViewHolder> viewHolders = new List<DeviceViewHolder>();
        
        public DevicesAdapter(IEnumerable<DeviceInfo> devices, DevicesViewType viewType = DevicesViewType.LargeDevices)
        {
            this.devices = devices.ToArray();
            ViewType = viewType;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = null;

            if (ViewType == DevicesViewType.LargeDevices)
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.DeviceItem, parent, false);
            else
                itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.SmallDeviceItem, parent, false);

            itemView.SetOnClickListener(this);
            return new DeviceViewHolder(itemView);
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            DeviceViewHolder viewHolder = holder as DeviceViewHolder;
            DeviceInfo deviceInfo = devices[position];

            if (ViewType == DevicesViewType.LargeDevices)
            {
                viewHolder.Icon.SetImageResource(deviceInfo.Hub ? Resource.Drawable.ic_devices_other : Resource.Drawable.ic_computer);
                viewHolder.Name.Text = deviceInfo.Name;
                viewHolder.Description.Text = deviceInfo.Description ?? "No description";
                viewHolder.Menu.Click += Menu_Click;
            }
            else
            {
                viewHolder.Icon.SetImageResource(deviceInfo.Hub ? Resource.Drawable.ic_devices_other : Resource.Drawable.ic_computer);
                viewHolder.Name.Text = deviceInfo.Name;

                if (ViewType == DevicesViewType.SmallDevices)
                    viewHolder.Expander.Visibility = ViewStates.Gone;
                else
                {
                    viewHolder.Services.SetLayoutManager(new LinearLayoutManager(viewHolder.ItemView.Context));
                    viewHolder.Services.SetLayoutManager(new WrapLayoutManager(viewHolder.ItemView.Context));
                    viewHolder.Services.AddItemDecoration(new DividerItemDecoration(viewHolder.ItemView.Context, LinearLayoutManager.Vertical));
                    viewHolder.Services.SetAdapter(new ServicesAdapter(deviceInfo.Services));
                }
            }

            viewHolders.Add(viewHolder);
        }

        public void OnClick(View view)
        {
            DeviceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view);
            DeviceInfo deviceInfo = devices[viewHolder.AdapterPosition];

            if (ViewType == DevicesViewType.LargeDevices)
            {
                Intent intent = new Intent(view.Context, typeof(DeviceActivity));
                intent.PutExtra("Device.Id", deviceInfo.Id.ToString());

                view.Context.StartActivity(intent);
            }
            else if (ViewType == DevicesViewType.SmallDevices)
            {
                if (DeviceClick != null)
                    DeviceClick(this, deviceInfo);
            }
            else if (ViewType == DevicesViewType.SmallDevicesAndServices)
            {
                bool expanded = viewHolder.Services.Visibility == ViewStates.Visible;
                expanded = !expanded;

                viewHolder.Services.Visibility = expanded ? ViewStates.Visible : ViewStates.Gone;
                viewHolder.Expander.SetImageResource(expanded ? Resource.Drawable.ic_expand_less : Resource.Drawable.ic_expand_more);
            }
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            View view = sender as View;
            DeviceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            DeviceInfo device = devices[viewHolder.AdapterPosition];

            PopupMenu menu = new PopupMenu(view.Context, view);
            menu.MenuItemClick += (s, a) => Menu_MenuItemClick(view, a);

            menu.Menu.Add(0, 0, 0, "Favorite");
            menu.Menu.Add(0, 1, 1, "Share");

            menu.Show();
        }
        private void Menu_MenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            View view = sender as View;
            DeviceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            DeviceInfo device = devices[viewHolder.AdapterPosition];
        }
        private void Favorite_Click(object sender, EventArgs e)
        {
            View view = sender as View;
            DeviceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            DeviceInfo device = devices[viewHolder.AdapterPosition];

            device.Favorite = !device.Favorite;
            DeviceManager.Save();

            viewHolder.Menu.Click -= Favorite_Click;
            NotifyItemChanged(viewHolder.AdapterPosition);
        }
    }


    public class NewDevicesAdapter : GenericAdapter<DeviceInfo>
    {
        public NewDevicesAdapter(IEnumerable<DeviceInfo> devices) : base(devices, Resource.Layout.DeviceItem) { }

        protected override void OnBind(View view, DeviceInfo deviceInfo)
        {
            ImageView icon = view.FindViewById<ImageView>(Resource.Id.DeviceItem_Icon);
            icon.SetImageResource(deviceInfo.Hub ? Resource.Drawable.ic_devices_other : Resource.Drawable.ic_computer);

            TextView name = view.FindViewById<TextView>(Resource.Id.DeviceItem_Name);
            name.Text = deviceInfo.Name;

            TextView description = view.FindViewById<TextView>(Resource.Id.DeviceItem_Description);
            description.Text = deviceInfo.Description ?? "No description";

            ImageView menu = view.FindViewById<ImageView>(Resource.Id.DeviceItem_Menu);
            menu.Click += Menu_Click;
        }
        protected override void OnUnbind(View view, DeviceInfo item)
        {
            ImageView menu = view.FindViewById<ImageView>(Resource.Id.DeviceItem_Menu);
            menu.Click -= Menu_Click;
        }

        protected override void OnClick(View view, DeviceInfo deviceInfo)
        {
            Intent intent = new Intent(view.Context, typeof(DeviceActivity));
            intent.PutExtra("Device.Id", deviceInfo.Id.ToString());

            view.Context.StartActivity(intent);
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            /*View view = sender as View;
            DeviceViewHolder viewHolder = viewHolders.First(vh => vh.ItemView == view.Parent);
            DeviceInfo device = devices[viewHolder.AdapterPosition];

            PopupMenu menu = new PopupMenu(view.Context, view);
            menu.MenuItemClick += (s, a) => Menu_MenuItemClick(view, a);

            menu.Menu.Add(0, 0, 0, "Favorite");
            menu.Menu.Add(0, 1, 1, "Share");

            menu.Show();*/
        }
    }

    /*public class NewSmallDevicesAdapter : GenericAdapter<DeviceInfo>
    {

    }*/
}