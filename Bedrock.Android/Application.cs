﻿using System;

using Android.App;
using Android.Runtime;
using Android.Utilities;

using Bedrock.Shared.Storage;

using Microsoft.Extensions.DependencyInjection;

namespace Bedrock.Android
{
    [Application]
    public class BedrockApplication : BaseApplication
    {
        public override string Name => "Bedrock";

        public BedrockApplication(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer) { }

        public override void OnCreate()
        {
            base.OnCreate();

            ServiceProvider services;

            Storage storage = new AdoNetStorage(Database);
            storage.Load();

#if DEBUG
            if (storage.Devices.Count == 0)
            {
                storage.FillDebugData(true);
                storage.Save();
                storage.Load();
            }
#endif

            // Run local service
            StartService(typeof(AndroidService));
        }

        protected override void OnDatabaseVersionUpgrade(AndroidDatabaseConnection connection, int oldVersion, int newVersion)
        {
#if RELEASE
            DeviceManager.CheckDatabase(connection);
#endif
        }
    }
}