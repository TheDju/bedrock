﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Bedrock.Common;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading;
using Android.Utilities;
using System.Threading.Tasks;
using Bedrock.Shared;

namespace Bedrock.Android
{
    [Activity(Theme = "@style/AppTheme.NoActionBar")]
    public class DeviceActivity : AppCompatActivity
    {
        private Guid deviceId;
        private DeviceInfo deviceInfo;

        private SwipeRefreshLayout swipeRefresh;
        private Snackbar snackbar;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DeviceActivity);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Handle bundle parameter
            Bundle extras = Intent.Extras;
            if (extras != null && extras.ContainsKey("Device.Id"))
            {
                // Parse device path
                try
                {
                    deviceId = Guid.Parse(extras.GetString("Device.Id"));
                }
                catch (Exception e)
                {
                    Toast.MakeText(Parent, "Wrong device path", ToastLength.Short).Show();
                    Finish();
                }

                // Try to find device
                //deviceInfo = DeviceManager.FindDeviceById(deviceId);
            }

            if (deviceInfo == null)
                throw new Exception("Could not find any device matching the specified path");

            // Refresh widget
            swipeRefresh = FindViewById<SwipeRefreshLayout>(Resource.Id.DeviceActivity_SwipeRefresh);
            swipeRefresh.Refresh += (s, e) => deviceInfo.Update().ContinueWith(t => RunOnUiThread(() => OnRefreshed(t)));
            swipeRefresh.SetColorSchemeResources(Resource.Color.colorAccent);

            // Init UI
            RecyclerView servicesView = FindViewById<RecyclerView>(Resource.Id.DeviceActivity_ServiceList);
            servicesView.HasFixedSize = true;
            servicesView.SetLayoutManager(new WrapLayoutManager(this));
            servicesView.AddItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.Vertical));

            RecyclerView devicesView = FindViewById<RecyclerView>(Resource.Id.DeviceActivity_DeviceList);
            devicesView.HasFixedSize = true;
            devicesView.SetLayoutManager(new WrapLayoutManager(this));
            devicesView.AddItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.Vertical));

            if (!(deviceInfo.Hub))
            {
                TextView devicesLabel = FindViewById<TextView>(Resource.Id.DeviceActivity_DeviceListLabel);
                devicesLabel.Visibility = ViewStates.Gone;
            }

            // Fill the view
            OnRefreshed();
            Refresh();
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            if (drawer.IsDrawerOpen(GravityCompat.Start))
                drawer.CloseDrawer(GravityCompat.Start);
            else
                base.OnBackPressed();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.DeviceMenu, menu);

            for (int i = 0; i < menu.Size(); i++)
            {
                IMenuItem item = menu.GetItem(i);

                //if (item.ItemId == Resource.Id.DeviceMenu_Favorite)
                //    item.SetIcon(deviceInfo.Favorite ? Resource.Drawable.ic_star : Resource.Drawable.ic_star_outline);
            }

            return true;
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    OnBackPressed();
                    break;

                /*case Resource.Id.DeviceMenu_Favorite:
                    deviceInfo.Favorite = !deviceInfo.Favorite;
                    item.SetIcon(deviceInfo.Favorite ? Resource.Drawable.ic_star : Resource.Drawable.ic_star_outline);

                    if (deviceInfo.Favorite)
                        Toast.MakeText(this, "Added to favorites", ToastLength.Short).Show();
                    else
                        Toast.MakeText(this, "Removed to favorites", ToastLength.Short).Show();

                    break;*/
            }

            return base.OnOptionsItemSelected(item);
        }

        private void Refresh()
        {
            snackbar?.Dismiss();
            swipeRefresh.Post(() => swipeRefresh.Refreshing = true);

            deviceInfo.Update().ContinueWith(t => RunOnUiThread(() => OnRefreshed(t)));
        }
        private void OnRefreshed(Task task = null)
        {
            swipeRefresh.Post(() => swipeRefresh.Refreshing = false);

            if (task != null)
            {
                if (task.IsFaulted)
                {
                    Exception[] exceptions = task.Exception.InnerExceptions.GetAllExceptions().ToArray();
                    string exceptionMessages = exceptions.Select(e => e.Message).Join(System.Environment.NewLine);

                    snackbar = Snackbar.Make(swipeRefresh, "Unable to connect to device" + System.Environment.NewLine + exceptionMessages, Snackbar.LengthLong)
                                       .SetAction("Retry", v => Refresh());
                    snackbar.Show();
                }
            }

            TextView nameView = FindViewById<TextView>(Resource.Id.DeviceActivity_Name);
            TextView descriptionView = FindViewById<TextView>(Resource.Id.DeviceActivity_Description);
            RecyclerView devicesView = FindViewById<RecyclerView>(Resource.Id.DeviceActivity_DeviceList);
            RecyclerView servicesView = FindViewById<RecyclerView>(Resource.Id.DeviceActivity_ServiceList);

            nameView.Text = deviceInfo.Name;
            descriptionView.Text = deviceInfo.Description;

            if (deviceInfo.Device is DeviceHub)
                devicesView.SetAdapter(new DevicesAdapter(deviceInfo.Devices));

            servicesView.SetAdapter(new ServicesAdapter(deviceInfo.Services));

            TextView deviceLabel = FindViewById<TextView>(Resource.Id.DeviceActivity_DeviceListLabel);
            TextView serviceLabel = FindViewById<TextView>(Resource.Id.DeviceActivity_ServiceListLabel);

            deviceLabel.Visibility = deviceInfo.Devices.Any() ? ViewStates.Visible : ViewStates.Gone;
            serviceLabel.Visibility = deviceInfo.Services.Any() ? ViewStates.Visible : ViewStates.Gone;
        }
    }
}