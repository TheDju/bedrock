﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading;
using Android.Utilities;
using System.Threading.Tasks;

namespace Bedrock.Android
{
    [Activity(Theme = "@style/AppTheme.NoActionBar")]
    public class RemoteServiceActivity : AppCompatActivity
    {
        private Guid serviceId;
        private ServiceInfo serviceInfo;
        private object mutex = new object();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RemoteServiceActivity);
            Title = "Remote service";

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Handle bundle parameter
            Bundle extras = Intent.Extras;
            if (extras != null && extras.ContainsKey("Service.Id"))
            {
                // Parse device path
                try
                {
                    serviceId = Guid.Parse(extras.GetString("Service.Id"));
                }
                catch (Exception e)
                {
                    Toast.MakeText(Parent, "Wrong service id", ToastLength.Short).Show();
                    Finish();
                }

                // Try to find device
                serviceInfo = DeviceManager.FindServiceById(serviceId);
            }

            if (serviceInfo == null)
                throw new Exception("Could not find any service matching the specified id");

            // Register UI elements
            RegisterButton(Resource.Id.RemoteServiceActivity_Num0Button, RemoteKey.Number0);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num1Button, RemoteKey.Number1);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num2Button, RemoteKey.Number2);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num3Button, RemoteKey.Number3);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num4Button, RemoteKey.Number4);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num5Button, RemoteKey.Number5);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num6Button, RemoteKey.Number6);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num7Button, RemoteKey.Number7);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num8Button, RemoteKey.Number8);
            RegisterButton(Resource.Id.RemoteServiceActivity_Num9Button, RemoteKey.Number9);

            RegisterButton(Resource.Id.RemoteServiceActivity_LeftButton, RemoteKey.Left);
            RegisterButton(Resource.Id.RemoteServiceActivity_UpButton, RemoteKey.Up);
            RegisterButton(Resource.Id.RemoteServiceActivity_RightButton, RemoteKey.Right);
            RegisterButton(Resource.Id.RemoteServiceActivity_DownButton, RemoteKey.Down);
            RegisterButton(Resource.Id.RemoteServiceActivity_OkButton, RemoteKey.Ok);

            //Register(Resource.Id.RemoteServiceActivity_ProgramUpButton);
            //Register(Resource.Id.RemoteServiceActivity_ProgramDownButton);
            RegisterButton(Resource.Id.RemoteServiceActivity_VolumeUpButton, RemoteKey.VolumeUp);
            RegisterButton(Resource.Id.RemoteServiceActivity_VolumeDownButton, RemoteKey.VolumeDown);
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            if (drawer.IsDrawerOpen(GravityCompat.Start))
                drawer.CloseDrawer(GravityCompat.Start);
            else
                base.OnBackPressed();
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void RegisterButton(int buttonId, RemoteKey key)
        {
            Button button = FindViewById<Button>(buttonId);
            button.Click += (s, e) => Button_Click(s, key);
        }
        private void Button_Click(object sender, RemoteKey key)
        {
            Task.Run(() =>
            {
                lock (mutex)
                {
                    RemoteService remoteService = serviceInfo.Service as RemoteService;
                    remoteService.Press(key);
                }
            });
        }
    }
}