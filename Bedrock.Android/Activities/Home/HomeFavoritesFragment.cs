﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Utilities;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Bedrock.Android
{
    public class HomeFavotiresFragment : TabFragment
    {
        public override string Title => "Favorites";

        /*public IEnumerable<DeviceInfo> devices;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.DevicesActivity);
            Title = "Favorites";

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.SetDrawerListener(toggle);
            toggle.SyncState();

            NavigationView navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.SetNavigationItemSelectedListener(this);

            // Initialize recycler
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.DevicesActivity_DeviceList);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this));
            recyclerView.HasFixedSize = true;
            recyclerView.SetLayoutManager(new WrapLayoutManager(this));
            recyclerView.AddItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.Vertical));

            // Handle UI elements
            View addDeviceButton = FindViewById<FloatingActionButton>(Resource.Id.fab);
            addDeviceButton.Click += AddDeviceButton_Click;

            devices = DeviceManager.Devices.Where(f => f.Favorite);
            Refresh();
        }
        protected override void OnResume()
        {
            base.OnResume();
            Refresh();
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            if (drawer.IsDrawerOpen(GravityCompat.Start))
                drawer.CloseDrawer(GravityCompat.Start);
            else
                base.OnBackPressed();
        }
        public virtual bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.SideMenu_Favorites:
                    Title = "Favorite devices";
                    devices = DeviceManager.Devices.Where(d => d.Favorite);
                    Refresh();
                    break;

                case Resource.Id.SideMenu_Neighbors:
                    Title = "Neighbors";
                    devices = DeviceManager.Devices.Where(d => d.Device is RemoteDevice);
                    Refresh();
                    break;

                case Resource.Id.SideMenu_Known:
                    Title = "Known devices";
                    devices = DeviceManager.Devices;
                    Refresh();
                    break;
            }

            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);

            return true;
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.MainMenu, menu);
            return true;
        }

        private void AddDeviceButton_Click(object sender, EventArgs e)
        {
            ViewGroup.LayoutParams layoutParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);

            LinearLayout linearLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            linearLayout.SetPadding(32, 64, 32, 32);

            TextInputLayout addressInputLayout = new TextInputLayout(this) { LayoutParameters = layoutParameters };
            EditText addressText = new EditText(this) { Hint = "Address", LayoutParameters = layoutParameters };
            addressInputLayout.AddView(addressText);
            linearLayout.AddView(addressInputLayout);

            TextInputLayout portInputLayout = new TextInputLayout(this) { LayoutParameters = layoutParameters };
            EditText portText = new EditText(this) { Hint = "Port", InputType = InputTypes.ClassNumber, Text = HttpRemoteDevice.DefaultPort.ToString(), LayoutParameters = layoutParameters };
            portInputLayout.AddView(portText);
            linearLayout.AddView(portInputLayout);

            new global::Android.Support.V7.App.AlertDialog.Builder(this)
                .SetTitle("New device")
                .SetView(linearLayout)
                .SetPositiveButton("OK", (s, ea) =>
                {
                    string address = addressText.Text;
                    ushort port = ushort.Parse(portText.Text);

                    DeviceInfo deviceInfo = new DeviceInfo()
                    {
                        Hub = true,
                        Device = new HttpRemoteDevice(address, port, "Server")
                    };

                    try
                    {
                        deviceInfo.Id = deviceInfo.Device.Id;
                        deviceInfo.Name = deviceInfo.Device.Name;
                        deviceInfo.Description = deviceInfo.Device.Description;

                        if (DeviceManager.FindDeviceById(deviceInfo.Id) != null)
                            throw new Exception("This device is already registered");
                    }
                    catch (Exception ex)
                    {
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                        return;
                    }

                    DeviceManager.Register(deviceInfo);

                    Intent intent = new Intent(this, typeof(DeviceActivity));
                    intent.PutExtra("Device.Id", deviceInfo.Id.ToString());

                    StartActivity(intent);
                })
                .SetNegativeButton("Cancel", (s, ea) => { })
                .Show();
        }

        private void Refresh()
        {
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.DevicesActivity_DeviceList);
            recyclerView.SetAdapter(new DevicesAdapter(devices.ToArray()));
        }*/
    }
}