using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Utilities;
using Android.Views;
using Android.Widget;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using SearchView = Android.Support.V7.Widget.SearchView;

using System.Threading.Tasks;

using static Android.Support.V7.Widget.SearchView;
using Android.Views.InputMethods;
using Android.Graphics.Drawables;

namespace Bedrock.Android
{
    [Activity(Theme = "@style/AppTheme.NoActionBar", LaunchMode = LaunchMode.SingleTask)]
    public class HomeActivity : NavigationActivity
    {
        private SearchView searchView;

        /*private HomeFavoritesFragment favoritesFragment = new HomeFavoritesFragment();
        private HomeLinesFragment linesFragment = new HomeLinesFragment();
        private HomeStopsFragment stopsFragment = new HomeStopsFragment();
        private ViewPager viewPager;
        private TabFragmentsAdapter fragmentsAdapter;

        private string lastSearch = "";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            NavigationItemId = Resource.Id.SideMenu_Home;
            OnCreate(savedInstanceState, Resource.Layout.HomeActivity);

            // Tabs
            viewPager = FindViewById<ViewPager>(Resource.Id.MainActivity_ViewPager);
            viewPager.OffscreenPageLimit = 2;
            viewPager.Adapter = fragmentsAdapter = new TabFragmentsAdapter(SupportFragmentManager, favoritesFragment, linesFragment, stopsFragment);
            viewPager.PageSelected += ViewPager_PageSelected;

            TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.MainActivity_Tabs);
            tabLayout.SetupWithViewPager(viewPager);

            if (App.Config.ShowFavorites && App.Config.FavoriteStops.Any())
                viewPager.SetCurrentItem(0, true);
            else
                viewPager.SetCurrentItem(1, true);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.MainMenu, menu);

            for (int i = 0; i < menu.Size(); i++)
            {
                IMenuItem item = menu.GetItem(i);

                if (item.ItemId == Resource.Id.MainMenu_Search)
                {
                    SearchManager searchManager = (SearchManager)GetSystemService(SearchService);

                    searchView = item.ActionView as SearchView;
                    searchView.SetSearchableInfo(searchManager.GetSearchableInfo(ComponentName));
                    searchView.QueryHint = "Rechercher";
                    searchView.QueryTextChange += SearchView_QueryTextChange;
                }
            }

            return base.OnCreateOptionsMenu(menu);
        }*/
    }
}