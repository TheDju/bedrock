﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Utilities;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using Service = Bedrock.Common.Service;
using Android.Appwidget;

namespace Bedrock.Android
{
    [Register("net.thedju.Bedrock.WidgetActivity")]
    [Activity(Theme = "@style/AppTheme.NoActionBar")]
    public class WidgetActivity : AppCompatActivity
    {
        private int appWidgetId = AppWidgetManager.InvalidAppwidgetId;
        private ServicesAdapter adapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.WidgetActivity);
            Title = "Select a service";

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Check intent args
            Bundle extras = Intent.Extras;
            if (extras != null)
                appWidgetId = extras.GetInt(AppWidgetManager.ExtraAppwidgetId, AppWidgetManager.InvalidAppwidgetId);

            // If they gave us an intent without the widget id, just bail.
            if (appWidgetId == AppWidgetManager.InvalidAppwidgetId)
            {
                Finish();
                return;
            }

            // Initialize recycler
            RecyclerView recyclerView = FindViewById<RecyclerView>(Resource.Id.WidgetActivity_List);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this));
            recyclerView.SetLayoutManager(new WrapLayoutManager(this));
            recyclerView.AddItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.Vertical));
            recyclerView.SetAdapter(adapter = new ServicesAdapter(DeviceManager.Services, ServicesViewType.SmallServices));

            // Register UI events
            adapter.ServiceClick += Adapter_ServiceClick;
        }

        public override void OnBackPressed()
        {
            SetResult(Result.Canceled);
            base.OnBackPressed();
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void Adapter_ServiceClick(object sender, ServiceInfo serviceInfo)
        {
            DeviceManager.RegisterWidget(appWidgetId, serviceInfo);

            AppWidgetManager appWidgetManager = AppWidgetManager.GetInstance(this);
            ServiceWidget.Update(this, appWidgetManager, appWidgetId);

            Intent result = new Intent();
            result.PutExtra(AppWidgetManager.ExtraAppwidgetId, appWidgetId);
            SetResult(Result.Ok, result);

            Finish();
        }
    }
}