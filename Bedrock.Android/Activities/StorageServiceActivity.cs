﻿using System;
using System.Collections.Generic;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Database;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Bedrock.Common;
using Bedrock.Shared;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading;
using Android.Utilities;
using System.Threading.Tasks;

namespace Bedrock.Android
{
    [Activity(Theme = "@style/AppTheme.NoActionBar")]
    public class StorageServiceActivity : AppCompatActivity
    {
        private Guid serviceId;
        private ServiceInfo serviceInfo;
        private object mutex = new object();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.StorageServiceActivity);
            Title = "Storage service";

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            // Handle bundle parameter
            Bundle extras = Intent.Extras;
            if (extras != null && extras.ContainsKey("Service.Id"))
            {
                // Parse device path
                try
                {
                    serviceId = Guid.Parse(extras.GetString("Service.Id"));
                }
                catch (Exception e)
                {
                    Toast.MakeText(Parent, "Wrong service id", ToastLength.Short).Show();
                    Finish();
                }

                // Try to find device
                serviceInfo = DeviceManager.FindServiceById(serviceId);
            }

            if (serviceInfo == null)
                throw new Exception("Could not find any service matching the specified id");
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            if (drawer.IsDrawerOpen(GravityCompat.Start))
                drawer.CloseDrawer(GravityCompat.Start);
            else
                base.OnBackPressed();
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    OnBackPressed();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}