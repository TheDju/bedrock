﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Bedrock.Common;

namespace Bedrock.Shared
{
    public delegate void NewDeviceCallback(DeviceSearcher searcher, DeviceInfo device);
    public delegate void NewServiceCallback(DeviceSearcher searcher, ServiceInfo service);
    public delegate void NewConnectionCallback(DeviceSearcher searcher, LinkInfo connection);

    public class DeviceSearcher
    {
        public event NewDeviceCallback NewDevice;
        public event NewServiceCallback NewService;
        public event NewConnectionCallback NewConnection;

        public IEnumerable<DeviceInfo> Devices => devices.AsReadOnly();
        public IEnumerable<ServiceInfo> Services => services.AsReadOnly();
        public IEnumerable<LinkInfo> Connections => connections.AsReadOnly();

        private List<DeviceInfo> devices = new List<DeviceInfo>();
        private List<ServiceInfo> services = new List<ServiceInfo>();
        private List<LinkInfo> connections = new List<LinkInfo>();

        public DeviceSearcher(IEnumerable<DeviceInfo> devices)
        {
            this.devices = devices.ToList();
            /*services = devices.SelectMany(d => d.Services);*/
        }

        public async Task Search()
        {
            await Task.Run(() =>
            {
                foreach (DeviceInfo device in devices.ToArray())
                    Browse(device);
            });
        }
        private void Browse(DeviceInfo device)
        {
            device.Update()
                  .ContinueWith(t =>
                  {
                      foreach (DeviceInfo d in device.Devices)
                          OnNewDevice(device, d);

                      foreach (ServiceInfo s in device.Services)
                          OnNewService(s);
                  }, TaskContinuationOptions.OnlyOnRanToCompletion)
                  .Wait();
        }

        private void OnNewDevice(DeviceInfo source, DeviceInfo destination)
        {
            if (!devices.Contains(destination))
            {
                Browse(destination);

                devices.Add(destination);
                NewDevice?.Invoke(this, destination);
            }

            LinkInfo connection = new LinkInfo(source, destination);
            connections.Add(connection);
            NewConnection?.Invoke(this, connection);
        }
        private void OnNewService(ServiceInfo service)
        {
            if (services.Contains(service))
                return;

            services.Add(service);
            NewService?.Invoke(this, service);
        }
    }
}