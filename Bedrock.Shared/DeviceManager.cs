﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Bedrock.Common;
using Bedrock.Shared;

namespace Bedrock.Shared
{
    using Service = Bedrock.Common.Service;

    public class DeviceManager
    {
        public static IEnumerable<DeviceInfo> Devices
        {
            get
            {
                return devices.AsReadOnly();
            }
        }
        public static IEnumerable<ServiceInfo> Services
        {
            get
            {
                return services.AsReadOnly();
            }
        }

        private static DbConnection connection;

        private static List<DeviceInfo> devices = new List<DeviceInfo>();
        private static List<ConnectionInfo> connections = new List<ConnectionInfo>();
        private static List<NeighborInfo> neighbors = new List<NeighborInfo>();

        private static List<ServiceInfo> services = new List<ServiceInfo>();

        #region Database loads and saves

        public static void Initialize(DbConnection connection)
        {
            DeviceManager.connection = connection;

            LoadData();
            BuildPaths();
        }
        public static async Task Save()
        {
            await Task.Run(() =>
            {
                //SaveData();
            });
        }

        private static void LoadData()
        {
            // Load devices
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT id, name, description, hub, favorite FROM devices";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        DeviceInfo deviceInfo = new DeviceInfo()
                        {
                            Id = Guid.Parse(reader.GetString(0)),
                            Name = reader.GetString(1),
                            Description = reader.IsDBNull(2) ? null : reader.GetString(2),
                            Hub = reader.GetInt32(3) != 0,
                            Favorite = reader.GetInt32(4) != 0
                        };

                        if (deviceInfo.Hub)
                            deviceInfo.Device = new DeviceHubProxy(deviceInfo);
                        else
                            deviceInfo.Device = new DeviceProxy(deviceInfo);

                        devices.Add(deviceInfo);
                    }
                }
            }

            // Load connections
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT source_id, destination_id, method, availability FROM device_connections";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid sourceId = Guid.Parse(reader.GetString(0));
                        Guid destinationId = Guid.Parse(reader.GetString(1));
                        ConnectionMethod? method = reader.IsDBNull(2) ? null : (ConnectionMethod?)Enum.Parse(typeof(ConnectionMethod), reader.GetString(2));
                        ConnectionAvailability? availability = reader.IsDBNull(3) ? null : (ConnectionAvailability?)Enum.Parse(typeof(ConnectionAvailability), reader.GetString(3));

                        ConnectionInfo connectionInfo = new ConnectionInfo(FindDeviceById(sourceId), FindDeviceById(destinationId), method, availability);
                        connections.Add(connectionInfo);
                    }
                }
            }

            // Load neighbors
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT id, method, address, param1, param2, param3, param4 FROM device_neighbors";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid id = Guid.Parse(reader.GetString(0));
                        string address = reader.GetString(2);
                        string param1 = reader.IsDBNull(3) ? null : reader.GetString(3);
                        string param2 = reader.IsDBNull(4) ? null : reader.GetString(4);
                        string param3 = reader.IsDBNull(5) ? null : reader.GetString(5);
                        string param4 = reader.IsDBNull(6) ? null : reader.GetString(6);

                        string methodName = reader.GetString(1).ToLower();
                        Type methodType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => { try { return a.GetTypes(); } catch { return Enumerable.Empty<Type>(); } })
                                                                                 .Where(t => !t.IsAbstract && t.IsDefined(typeof(RemoteDeviceAttribute), false))
                                                                                 .FirstOrDefault(t => t.GetCustomAttribute<RemoteDeviceAttribute>().Method.ToLower() == methodName);

                        if (methodType == null)
                        {
                            Log.Warning("Method {0} could not be found", methodName);
                            continue;
                        }

                        NeighborInfo neighborInfo = new NeighborInfo(FindDeviceById(id), methodType, address, param1, param2, param3, param4);
                        neighbors.Add(neighborInfo);
                    }
                }
            }

            // Load services
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT device_id, id, name, description, type, favorite FROM device_services";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid deviceId = Guid.Parse(reader.GetString(0));
                        Guid serviceId = Guid.Parse(reader.GetString(1));
                        string name = reader.GetString(2);
                        string description = reader.IsDBNull(3) ? null : reader.GetString(3);
                        string typeName = reader.IsDBNull(4) ? null : reader.GetString(4);
                        bool favorite = reader.GetInt32(5) != 0;

                        Type type = typeName == null ? null : AppDomain.CurrentDomain.GetAssemblies().Select(a => a.GetType(typeName, false, true)).FirstOrDefault(t => t != null);

                        DeviceInfo deviceInfo = FindDeviceById(deviceId);
                        ServiceInfo serviceInfo = new ServiceInfo(deviceInfo, serviceId, name, description, favorite, type);

                        services.Add(serviceInfo);
                        deviceInfo.Services.Add(serviceInfo);
                    }
                }
            }
        }
        private static void SaveData()
        {
            List<string> queries = new List<string>();

            // Clean everything
            queries.Add("DELETE FROM devices");
            queries.Add("DELETE FROM device_neighbors");
            queries.Add("DELETE FROM device_connections");
            queries.Add("DELETE FROM device_services");

            // Store every devices
            foreach (DeviceInfo device in devices)
            {
                queries.Add(string.Format("INSERT INTO devices (id, name, description, hub, favorite) VALUES ('{0}', {1}, {2}, {3}, {4})",
                    device.Id,
                    ProtectValue(device.Name),
                    ProtectValue(device.Description),
                    device.Hub ? 1 : 0,
                    device.Favorite ? 1 : 0));
            }

            // Store neighbor devices
            foreach (DeviceInfo device in devices.Where(d => d.Device is RemoteDevice))
            {
                RemoteDevice remoteDevice = device.Device as RemoteDevice;

                queries.Add(string.Format("INSERT INTO device_neighbors (id, method, address, param1, param2, param3, param4) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                    device.Id,
                    remoteDevice.GetType().GetCustomAttribute<RemoteDeviceAttribute>().Method,
                    remoteDevice.Address,
                    remoteDevice.Param1,
                    remoteDevice.Param2,
                    remoteDevice.Param3,
                    remoteDevice.Param4));
            }

            // Store connections
            foreach (ConnectionInfo connection in connections)
            {
                queries.Add(string.Format("INSERT INTO device_connections (source_id, destination_id) VALUES ('{0}', '{1}')",
                    connection.Source.Id,
                    connection.Destination.Id));
            }

            // Store services
            foreach (ServiceInfo service in services)
            {
                queries.Add(string.Format("INSERT INTO device_services (device_id, id, name, description, type, favorite) VALUES ('{0}', '{1}', {2}, {3}, {4}, {5})",
                    service.Device.Id,
                    service.Id,
                    ProtectValue(service.Name),
                    ProtectValue(service.Description),
                    ProtectValue(service.Type?.FullName),
                    service.Favorite ? 1 : 0));
            }

            // Execute commands
            foreach (string query in queries)
            {
                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                }
            }
        }
        private static void BuildPaths()
        {
            // Clear device links
            foreach (DeviceInfo device in devices)
                device.Paths.Clear();

            // Compute neighbor devices
            foreach (NeighborInfo neighbor in neighbors)
                neighbor.Destination.Paths.Add(new LinkInfo[] { neighbor });

            // Find device paths
            List<KeyValuePair<DeviceInfo, LinkInfo[]>> devicesPaths = new List<KeyValuePair<DeviceInfo, LinkInfo[]>>();
            List<DeviceInfo> knownDevices = devices.Where(d => d.Paths.Count > 0).ToList();
            List<DeviceInfo> unknownDevices = devices.Except(knownDevices).ToList();

            // Register sub-devices
            foreach (DeviceInfo device in knownDevices)
            {
                device.devicesInfo.Clear();

                ConnectionInfo[] deviceConnections = connections.Where(c => c.Source == device).ToArray();
                foreach (ConnectionInfo connection in deviceConnections)
                    device.devicesInfo.Add(connection.Destination);
            }

            // Build every paths
            while (unknownDevices.Count > 0)
            {
                List<DeviceInfo> passDevices = new List<DeviceInfo>();

                foreach (DeviceInfo device in knownDevices)
                {
                    ConnectionInfo[] deviceConnections = connections.Where(c => c.Source == device && unknownDevices.Contains(c.Destination)).ToArray();

                    foreach (ConnectionInfo connection in deviceConnections)
                    {
                        List<LinkInfo[]> devicePaths = Enumerable.Concat(devicesPaths.Where(p => p.Key == device).Select(p => p.Value),
                                                                         neighbors.Where(n => n.Destination == device).Select(n => new LinkInfo[] { n }))
                                                                 .ToList();

                        if (devicePaths.Count == 0)
                            devicePaths.Add(new LinkInfo[0]);

                        foreach (LinkInfo[] devicePath in devicePaths)
                            devicesPaths.Add(new KeyValuePair<DeviceInfo, LinkInfo[]>(connection.Destination, devicePath.Concat(new[] { connection }).ToArray()));

                        passDevices.Add(connection.Destination);
                    }
                }

                if (passDevices.Count == 0)
                    break; // FIXME: Something wrong happenned

                foreach (DeviceInfo device in passDevices.Distinct())
                {
                    knownDevices.Add(device);
                    unknownDevices.Remove(device);
                }
            }

            // Save paths
            foreach (var devicePath in devicesPaths)
                devicePath.Key.Paths.Add(devicePath.Value);
        }

        private static string ProtectValue(string value)
        {
            if (value == null)
                return "NULL";

            return "'" + value.Replace("\\", "\\\\").Replace("'", "''") + "'";
        }

        #endregion
        #region Queries and registrations

        public static DeviceInfo FindDeviceById(Guid id)
        {
            return devices.FirstOrDefault(d => d.Id == id);
        }
        public static ServiceInfo FindServiceById(Guid id)
        {
            return services.FirstOrDefault(d => d.Id == id);
        }
        public static ServiceInfo FindServiceByWidgetId(int widgetId)
        {
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = $"SELECT service_id FROM widgets WHERE id = {widgetId}";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    if (!reader.Read())
                        return null;

                    Guid serviceId = Guid.Parse(reader.GetString(0));
                    return FindServiceById(serviceId);
                }
            }
        }

        public static void RegisterWidget(int widgetId, ServiceInfo service)
        {
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = $"INSERT INTO widgets (id, service_id) VALUES ({widgetId}, '{service.Id}')";
                command.ExecuteNonQuery();
            }
        }
        public static void Register(DeviceInfo device, params ConnectionInfo[] connectionsInfo)
        {
            if (!devices.Any(d => d.Id == device.Id))
                devices.Add(device);

            foreach (ConnectionInfo connectionInfo in connectionsInfo)
                if (!connections.Any(c => c.Source == connectionInfo.Source && c.Destination == connectionInfo.Destination))
                    connections.Add(connectionInfo);

            BuildPaths();
        }

        public static async Task UpdateDeviceInfo(DeviceInfo deviceInfo)
        {
            await Task.Run(async () =>
            {
                //deviceInfo.Name = deviceInfo.Device.Name;
                deviceInfo.Description = deviceInfo.Device.Description;

                foreach (Service service in deviceInfo.Device.Services)
                {
                    Guid id = service.Id;

                    ServiceInfo serviceInfo = deviceInfo.Services.FirstOrDefault(s => s.Id == id);
                    if (serviceInfo == null)
                    {
                        serviceInfo = new ServiceInfo(deviceInfo, id);

                        services.Add(serviceInfo);
                        deviceInfo.Services.Add(serviceInfo);
                    }

                    serviceInfo.Service = service;
                    await UpdateServiceInfo(serviceInfo);
                }

                DeviceHub deviceHub = deviceInfo.Device as DeviceHub;
                if (deviceHub != null)
                {
                    deviceInfo.devicesInfo.Clear();

                    foreach (Device device in deviceHub.Devices)
                    {
                        Guid subDeviceId = device.Id;
                        DeviceInfo subDeviceInfo = FindDeviceById(subDeviceId);

                        if (subDeviceInfo == null)
                        {
                            subDeviceInfo = new DeviceInfo();

                            subDeviceInfo.Id = subDeviceId;
                            subDeviceInfo.Name = device.Name;
                            subDeviceInfo.Description = device.Description;
                            subDeviceInfo.Device = device;
                            subDeviceInfo.Hub = device is DeviceHub;
                        }

                        deviceInfo.devicesInfo.Add(subDeviceInfo);
                        Register(subDeviceInfo, new ConnectionInfo(deviceInfo, subDeviceInfo));
                    }
                }

                Save();
            });
        }
        public static async Task<ServiceInfo> UpdateServiceInfo(ServiceInfo serviceInfo, bool onlyData = false)
        {
            return await Task.Run(() =>
            {
                if (serviceInfo.Service == null)
                    serviceInfo.Service = serviceInfo.Device.Device.Services.FirstOrDefault(s => s.Id == serviceInfo.Id);

                if (serviceInfo.Service == null)
                    throw new Exception("Unable to access the service " + serviceInfo.Name);

                if (serviceInfo.Type == null)
                {
                    if (serviceInfo.Service is PowerService) serviceInfo.Type = typeof(PowerService);
                    else if (serviceInfo.Service is RemoteService) serviceInfo.Type = typeof(RemoteService);
                    else if (serviceInfo.Service is StorageService) serviceInfo.Type = typeof(StorageService);
                    else
                        serviceInfo.Type = serviceInfo.Service.GetType();
                }

                if (!onlyData)
                {
                    serviceInfo.Name = serviceInfo.Service.Name;
                    serviceInfo.Description = serviceInfo.Service.Description;
                }

                if (serviceInfo.Service is PowerService)
                    serviceInfo.Data[nameof(PowerService.Value)] = (serviceInfo.Service as PowerService).Value;

                return serviceInfo;
            });
        }

        #endregion

        public static void CheckDatabase(DbConnection connection)
        {
            bool shouldReset = false;

#if DEBUG
            shouldReset = true;
#endif

            string[] checkQueries = new[]
            {
                "SELECT id, name, description, hub, favorite FROM devices LIMIT 0",
                "SELECT source_id, destination_id, method, availability FROM device_connections LIMIT 0",
                "SELECT id, method, address, param1, param2, param3, param4 FROM device_neighbors LIMIT 0",
                "SELECT device_id, id, name, description, type, favorite FROM device_services LIMIT 0",
                "SELECT id, service_id FROM widgets LIMIT 0"
            };

            try
            {
                foreach (string query in checkQueries)
                {
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch
            {
                shouldReset = true;
            }

            if (shouldReset)
            {
                string[] createQueries = new[]
                {
                    "DROP TABLE IF EXISTS devices",
                    "CREATE TABLE devices (id TEXT NOT NULL, name TEXT NOT NULL, description TEXT, hub INTEGER NOT NULL, favorite INTEGER NOT NULL, PRIMARY KEY (id))",

                    "DROP TABLE IF EXISTS device_connections",
                    "CREATE TABLE device_connections (source_id TEXT NOT NULL, destination_id TEXT NOT NULL, method TEXT, availability INTEGER)",

                    "DROP TABLE IF EXISTS device_neighbors",
                    "CREATE TABLE device_neighbors (id TEXT NOT NULL, method TEXT NOT NULL, address TEXT NOT NULL, param1 TEXT, param2 TEXT, param3 TEXT, param4 TEXT)",

                    "DROP TABLE IF EXISTS device_services",
                    "CREATE TABLE device_services (device_id TEXT NOT NULL, id TEXT NOT NULL, name TEXT NOT NULL, description TEXT, type TEXT, favorite INTEGER NOT NULL)",

                    "DROP TABLE IF EXISTS widgets",
                    "CREATE TABLE widgets (id TEXT NOT NULL, service_id TEXT NOT NULL)",
                };

                foreach (string query in createQueries)
                {
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                }

#if DEBUG
                string[] queries = new[]
                {
                    "DELETE FROM devices",
                    "INSERT INTO devices VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', 'Julien-Pi', NULL, 1, 1)",
                    "INSERT INTO devices VALUES ('56eb27b8-a1cb-bfc3-ccd1-329761dd0000', 'Saloon''s light', NULL, 0, 1)",
                    "INSERT INTO devices VALUES ('56eb27b8-a1cb-bfc3-ccd1-e63d5e1b0000', 'Saloon''s TV', NULL, 0, 1)",
                    "INSERT INTO devices VALUES ('2214aafc-b00c-050e-e72a-6581172e0000', 'Julien-Bureau', NULL, 1, 1)",
                    "INSERT INTO devices VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', 'Julien-Android', NULL, 1, 1)",

                    "DELETE FROM device_connections",
                    "INSERT INTO device_connections VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', '56eb27b8-a1cb-bfc3-ccd1-329761dd0000', NULL, NULL)",
                    "INSERT INTO device_connections VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', '56eb27b8-a1cb-bfc3-ccd1-e63d5e1b0000', NULL, NULL)",

                    "DELETE FROM device_neighbors",
                    "INSERT INTO device_neighbors VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', 'remoting.http', '192.168.1.11', 8889, 'Server', NULL, NULL)",
                    "INSERT INTO device_neighbors VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', 'remoting.http', 'iot.thedju.net', 80, 'Server', NULL, NULL)",
                    "INSERT INTO device_neighbors VALUES ('2214aafc-b00c-050e-e72a-6581172e0000', 'remoting.http', '192.168.1.24', 8889, 'Server', NULL, NULL)",
                    "INSERT INTO device_neighbors VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', 'local', 'local', NULL, NULL, NULL, NULL)",
                    "INSERT INTO device_neighbors VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', 'remoting.http', '127.0.0.1', 8889, 'Server', NULL, NULL)",

                    "DELETE FROM device_services",
                    "INSERT INTO device_services VALUES ('56eb27b8-a1cb-13a6-bcf6-a140d8f50000', '56eb27b8-a1cb-9d75-44dd-e021d52f0000', 'Storage', 'Raspberry Pi shared storage', 'Bedrock.Common.StorageService', 1)",
                    "INSERT INTO device_services VALUES ('56eb27b8-a1cb-bfc3-ccd1-329761dd0000', '56eb27b8-a1cb-737f-cd0d-e5989b040000', 'Power', 'Light''s power', 'Bedrock.Common.PowerService', 1)",
                    "INSERT INTO device_services VALUES ('56eb27b8-a1cb-bfc3-ccd1-e63d5e1b0000', '56eb27b8-a1cb-c8b1-2bfd-e5989b040000', 'Power', 'TV''s power', 'Bedrock.Common.PowerService', 1)",
                    "INSERT INTO device_services VALUES ('56eb27b8-a1cb-bfc3-ccd1-e63d5e1b0000', '56eb27b8-a1cb-d0ee-1e9a-66e4af910000', 'Remote', 'TV''s controller', 'Bedrock.Common.RemoteService', 1)",
                    "INSERT INTO device_services VALUES ('2214aafc-b00c-050e-e72a-6581172e0000', '2214aafc-b00c-bf3e-981a-000000000000', 'Clipboard', 'Computer''s clipboard', 'Bedrock.Common.ClipboardService', 1)",
                    "INSERT INTO device_services VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', '2f931ccb-5182-7af6-0000-000000000000', 'Storage', 'Android''s shared storage', 'Bedrock.Common.StorageService', 1)",
                    "INSERT INTO device_services VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', '461adf05-ddce-33ec-0000-000000000000', 'Notifications', 'Android''s push service', 'Bedrock.Common.PushService', 1)",
                    "INSERT INTO device_services VALUES ('5a57bb0e-c68d-3b46-0000-000000000000', '1733dc87-39dc-6fda-0000-000000000000', 'Clipboard', 'Android''s clipboard', 'Bedrock.Common.ClipboardService', 1)",

                    "DELETE FROM widgets",
                    "INSERT INTO widgets VALUES (79, '56eb27b8-a1cb-737f-cd0d-e5989b040000')",
                };

                foreach (string query in queries)
                {
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                }
#endif
            }
        }
    }
}