﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

using Bedrock.Common;

namespace Bedrock.Shared.Storage
{
    internal class AdoNetStorage : Storage
    {
        public override ICollection<DeviceInfo> Devices { get; } = new List<DeviceInfo>();
        public override ICollection<ServiceInfo> Services { get; } = new List<ServiceInfo>();
        public override ICollection<PathNodeInfo> Connections { get; } = new List<PathNodeInfo>();

        private DbConnection connection;
        private const int version = 1;

        public AdoNetStorage(DbConnection connection)
        {
            this.connection = connection;

            int currentVersion = GetCurrentVersion();

            if (currentVersion == 0)
                RebuildStructure();
            else if (currentVersion < version)
                UpdateDatabase(currentVersion);
            else if (currentVersion > version)
                throw new NotSupportedException("Database is too recent");
        }

        private int GetCurrentVersion()
        {
            int currentVersion;

            try
            {
                using (DbCommand command = connection.CreateCommand("SELECT value FROM settings WHERE key = 'Version'"))
                    currentVersion = int.Parse((string)command.ExecuteScalar());
            }
            catch
            {
                return 0;
            }

            if (currentVersion != version)
                return currentVersion;

            bool consistent = true;

            consistent &= CheckConsistency("devices", "id", "name", "description", "hub");
            consistent &= CheckConsistency("device_connections", "device_id", "method", "address", "parameters");
            consistent &= CheckConsistency("device_links", "source_id", "destination_id");
            consistent &= CheckConsistency("device_services", "device_id", "id", "name", "description");
            consistent &= CheckConsistency("settings", "key", "value");

            return consistent ? version : 0;
        }
        private bool CheckConsistency(string table, params string[] fields)
        {
            try
            {
                using (DbCommand command = connection.CreateCommand($"SELECT * FROM {table} LIMIT 0"))
                {
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        if (reader.FieldCount != fields.Length)
                            return false;

                        for (int i = 0; i < fields.Length; i++)
                            if (reader.GetName(i) != fields[i])
                                return false;
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void RebuildStructure()
        {
            string[] queries = new[]
            {
                "DROP TABLE IF EXISTS devices",
                "CREATE TABLE devices (id TEXT NOT NULL, name TEXT NOT NULL, description TEXT, hub INTEGER NOT NULL, PRIMARY KEY (id))",

                "DROP TABLE IF EXISTS device_connections",
                "CREATE TABLE device_connections (device_id TEXT NOT NULL, method TEXT NOT NULL, address TEXT NOT NULL, parameters TEXT)",

                "DROP TABLE IF EXISTS device_links",
                "CREATE TABLE device_links (source_id TEXT NOT NULL, destination_id TEXT NOT NULL)",

                "DROP TABLE IF EXISTS device_services",
                "CREATE TABLE device_services (device_id TEXT NOT NULL, id TEXT NOT NULL, name TEXT NOT NULL, description TEXT)",
                
                "DROP TABLE IF EXISTS settings",
                "CREATE TABLE settings (key TEXT NOT NULL, value TEXT)",

                $"INSERT INTO settings VALUES ('Version', {version})"
            };

            foreach (string query in queries)
            {
                using (DbCommand command = connection.CreateCommand(query))
                    command.ExecuteNonQuery();
            }
        }
        private void UpdateDatabase(int currentVersion)
        {
        }

        public override void Load()
        {
            Devices.Clear();
            Connections.Clear();
            Services.Clear();

            var connectionsData = new List<(Guid DeviceId, string Method, string Address, object Parameters)>();

            // Load connections
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT device_id, method, address, parameters FROM device_connections";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid deviceId = Guid.Parse(reader.GetString(0));
                        string method = reader.GetString(1);
                        string address = reader.GetString(2);
                        object parameters = reader.IsDBNull(3) ? null : reader.GetString(3);

                        try
                        {
                            XDocument document = XDocument.Parse((string)parameters);

                            parameters = document.Root.Attributes()
                                .Select(a => new KeyValuePair<string, object>(a.Name.LocalName, a.Value))
                                .ToDictionary();
                        }
                        catch { }

                        connectionsData.Add((deviceId, method, address, parameters));
                    }
                }
            }

            // Load devices
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT id, name, description, hub FROM devices";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid id = Guid.Parse(reader.GetString(0));
                        string name = reader.GetString(1);
                        string description = reader.IsDBNull(2) ? null : reader.GetString(2);
                        bool hub = reader.GetInt32(3) != 0;

                        DeviceInfo deviceInfo;
                        var matchingConnections = connectionsData.Where(c => c.DeviceId == id).ToArray();

                        if (matchingConnections.Length == 0)
                            deviceInfo = new DeviceInfo(id, name, description, hub);
                        else
                        {
                            deviceInfo = new ServerInfo(id, name, description, hub);

                            List<ConnectionInfo> connectionsInfo = matchingConnections.Select(c => new ConnectionInfo(deviceInfo, c.Method, c.Address, c.Parameters)).ToList();
                            Connections.AddRange(connectionsInfo);

                            FieldInfo connectionsField = typeof(ServerInfo).GetField("connections", BindingFlags.NonPublic | BindingFlags.Instance);
                            connectionsField.SetValue(deviceInfo, connectionsInfo);
                        }

                        /*if (deviceInfo.Hub)
                            deviceInfo.Device = new DeviceHubProxy(deviceInfo);
                        else
                            deviceInfo.Device = new DeviceProxy(deviceInfo);*/

                        Devices.Add(deviceInfo);
                    }
                }
            }
            
            // Load links
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT source_id, destination_id FROM device_links";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid sourceId = Guid.Parse(reader.GetString(0));
                        Guid destinationId = Guid.Parse(reader.GetString(1));

                        DeviceInfo sourceDeviceInfo = Devices.FirstOrDefault(d => d.Id == sourceId);
                        DeviceInfo destinationDeviceInfo = Devices.FirstOrDefault(d => d.Id == destinationId);

                        LinkInfo linkInfo = new LinkInfo(sourceDeviceInfo, destinationDeviceInfo);
                        Connections.Add(linkInfo);
                    }
                }
            }

            // Load services
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT device_id, id, name, description FROM device_services";

                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Guid deviceId = Guid.Parse(reader.GetString(0));
                        Guid serviceId = Guid.Parse(reader.GetString(1));
                        string name = reader.GetString(2);
                        string description = reader.IsDBNull(3) ? null : reader.GetString(3);

                        DeviceInfo deviceInfo = Devices.FirstOrDefault(d => d.Id == deviceId);

                        ServiceInfo serviceInfo = new ServiceInfo(deviceInfo, serviceId, name, description);
                        Services.Add(serviceInfo);
                    }
                }
            }
        }
        public override void Save()
        {
            // Clean everything
            string[] queries = new[]
            {
                "DELETE FROM devices",
                "DELETE FROM device_connections",
                "DELETE FROM device_links",
                "DELETE FROM device_services"
            };

            foreach (string query in queries)
            {
                using (DbCommand command = connection.CreateCommand(query))
                    command.ExecuteNonQuery();
            }

            // Save devices
            foreach (DeviceInfo deviceInfo in Devices)
            {
                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"INSERT INTO devices VALUES ('{deviceInfo.Id}', '{deviceInfo.Name}', '{deviceInfo.Description}', {(deviceInfo.Hub ? 1 : 0)})";
                    command.ExecuteNonQuery();
                }
            }

            // Save path nodes
            foreach (PathNodeInfo pathNodeInfo in Connections)
            {
                if (pathNodeInfo is ConnectionInfo connectionInfo)
                {
                    XDocument document = new XDocument(new XElement("Parameters"));

                    foreach (var pair in connectionInfo.Parameters)
                        document.Root.Add(new XAttribute(pair.Key, pair.Value));

                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"INSERT INTO device_connections VALUES ('{connectionInfo.Destination.Id}', '{connectionInfo.Method}', '{connectionInfo.Address}', '{document}')";
                        command.ExecuteNonQuery();
                    }
                }
                else if (pathNodeInfo is LinkInfo linkInfo)
                {
                    using (DbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = $"INSERT INTO device_links VALUES ('{linkInfo.Source.Id}', '{linkInfo.Destination.Id}')";
                        command.ExecuteNonQuery();
                    }
                }
            }

            // Save services
            foreach (ServiceInfo serviceInfo in Services)
            {
                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"INSERT INTO device_services VALUES ('{serviceInfo.Device.Id}', '{serviceInfo.Id}', '{serviceInfo.Name}', '{serviceInfo.Description}')";
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}