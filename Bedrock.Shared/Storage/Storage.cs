﻿using System;
using System.Collections.Generic;

using Bedrock.Common;

namespace Bedrock.Shared.Storage
{
    internal abstract class Storage
    {
        public abstract ICollection<DeviceInfo> Devices { get; }
        public abstract ICollection<ServiceInfo> Services { get; }
        public abstract ICollection<PathNodeInfo> Connections { get; }

        public abstract void Load();
        public abstract void Save();
    }
}