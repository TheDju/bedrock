﻿using System;

using Bedrock.Common;

namespace Bedrock.Shared.Storage
{
#if DEBUG
    internal static class StorageExtensions
    {
        public static void FillDebugData(this Storage storage, bool clear = true)
        {
            if (clear)
            {
                storage.Devices.Clear();
                storage.Services.Clear();
                storage.Connections.Clear();
            }

            // Raspberry Pi
            DeviceInfo raspberryPiDeviceInfo = new DeviceInfo(new Guid("bd8bacc4-550e-4970-95e4-f060d5def112"), "Raspberry Pi 3", "", true)
            {
                Devices = new[]
                {
                    new DeviceInfo(new Guid("3482bbb6-fdf9-4cd3-bbd1-00fd26342228"), "Saloon's Light", "", false),
                    new DeviceInfo(new Guid("faf11df4-9357-4347-a170-2850fcf7a808"), "Saloon's TV", "", false),
                }
            };

            // Julien-Bureau
            {
                ServerInfo julienBureauServerInfo = new ServerInfo(new Guid("62d7ac00-4f73-050e-e72a-6581172e0000"), "Julien-Bureau", "", true);

                ConnectionInfo[] julienBureauConnectionsInfo = new[]
                {
                    new ConnectionInfo(julienBureauServerInfo, "http", "127.0.0.1", new { Port = 2480 }),
                    new ConnectionInfo(julienBureauServerInfo, "tcp", "127.0.0.1", new { Port = 2424 }),
                };

                foreach (ConnectionInfo connectionInfo in julienBureauConnectionsInfo)
                    storage.Connections.Add(connectionInfo);

                julienBureauServerInfo.Services = new[]
                {
                    new ServiceInfo(julienBureauServerInfo, new Guid("62d7ac00-4f73-bf3e-981a-65b4e13c0000"), "Windows clipboard"),
                    new ServiceInfo(julienBureauServerInfo, new Guid("62d7ac00-4f73-1bcc-9418-a7ca6e130000"), "Windows power"),
                    new ServiceInfo(julienBureauServerInfo, new Guid("62d7ac00-4f73-ffe2-bb77-4cf9ca270000"), "CPU temperature sensor")
                };

                storage.Devices.Add(julienBureauServerInfo);
            }
        }
    }
#endif
}