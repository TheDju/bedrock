﻿using System;
using System.Collections.Generic;
using System.Text;

#if STORAGE_COUCHBASE_LITE

using Couchbase.Lite;

namespace Bedrock.Shared.Storage
{
    internal class CouchbaseStorage
    {
        public List<DeviceInfo> Devices { get; } = new List<DeviceInfo>();
        public List<ServiceInfo> Services { get; } = new List<ServiceInfo>();
        public List<LinkInfo> Connections { get; } = new List<LinkInfo>();
        public List<NeighborInfo> Neighbors { get; } = new List<NeighborInfo>();

        public List<DeviceInfo> FavoriteDevices { get; } = new List<DeviceInfo>();
        public List<ServiceInfo> FavoriteServices { get; } = new List<ServiceInfo>();

        private Database database;

        public CouchbaseStorage()
        {
            Manager manager = Manager.SharedInstance;
            database = manager.GetDatabase("bedrock");

            /*Document settingsDocument = database.GetDocument("settings");
            settingsDocument.Update(r =>
            {
                r.Document.PutProperties(new Dictionary<string, object>()
                {
                    ["settings"]
                });
            })*/
        }

        public void Load()
        {
            Devices.Clear();

            // Load devices
            
        }


        public static void SaveDevices(IEnumerable<DeviceInfo> devices)
        {
        }
        public static void SaveServices(IEnumerable<ServiceInfo> services)
        {
        }
        public static void SaveConnections(IEnumerable<ConnectionInfo> connections)
        {
        }
        public static void SaveNeighbors(IEnumerable<NeighborInfo> neighbors)
        {
        }
    }
}

#endif