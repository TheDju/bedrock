﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Bedrock.Common;

namespace Bedrock.Shared
{
    public class DeviceHubProxy : DeviceHub
    {
        public DeviceInfo Info { get; }

        public override string Name
        {
            get
            {
                return TryGet(d => d.Name);
            }
            set
            {
                TrySet(d => d.Name = value);
            }
        }
        public override string Description
        {
            get
            {
                return TryGet(d => d.Description);
            }
            set
            {
                TrySet(d => d.Description = value);
            }
        }
        public override Guid Id
        {
            get
            {
                return TryGet(d => d.Id);
            }
        }
        public bool Connected
        {
            get
            {
                return deviceHub != null;
            }
        }

        public override IEnumerable<Service> Services
        {
            get
            {
                return TryGet(d => d.Services);
            }
        }
        public override IEnumerable<Device> Devices
        {
            get
            {
                return TryGet(d => d.Devices);
            }
        }

        private DeviceHub deviceHub;

        public DeviceHubProxy(DeviceInfo info)
        {
            Info = info;
        }

        private void Connect()
        {
            if (Connected)
                return;

            /*foreach (LinkInfo[] path in Info.Paths)
            {
                NeighborInfo first = path[0] as NeighborInfo;
                //if (first.Destination.Device != null && !(first.Destination.Device is RemoteDevice))
                //    continue;

                if (path.Length == 1)
                {
                    if (first.Method == null)
                        throw new Exception("The connection has an invalid method");

                    RemoteDevice remoteDevice = Activator.CreateInstance(first.Method, new object[] { first.Address }) as RemoteDevice;

                    remoteDevice.Param1 = first.Param1;
                    remoteDevice.Param2 = first.Param2;
                    remoteDevice.Param3 = first.Param3;
                    remoteDevice.Param4 = first.Param4;

                    remoteDevice.Connect();
                    if (remoteDevice.Connected)
                    {
                        deviceHub = remoteDevice;
                        return;
                    }
                }
                else
                {
                    RemoteDevice remoteDevice = first.Destination.Device as RemoteDevice;
                    deviceHub = remoteDevice.FindDevice(path.Skip(1).Select(l => l.Destination.Id).ToArray()) as DeviceHub;
                    
                    if (deviceHub != null)
                        return;
                }
            }*/
        }

        private T TryGet<T>(Func<DeviceHub, T> getter)
        {
            Connect();

            try
            {
                return getter(deviceHub);
            }
            catch (Exception e)
            {
                deviceHub = null;

                Connect();
                return getter(deviceHub);
            }
        }
        private void TrySet(Action<DeviceHub> setter)
        {
            Connect();

            try
            {
                setter(deviceHub);
            }
            catch (Exception e)
            {
                deviceHub = null;

                Connect();
                setter(deviceHub);
            }
        }
    }
}