﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Bedrock.Common;

namespace Bedrock.Shared
{
    public class DeviceProxy : Device
    {
        public DeviceInfo Info { get; }

        public override string Name
        {
            get
            {
                return TryGet(d => d.Name);
            }
            set
            {
                TrySet(d => d.Name = value);
            }
        }
        public override string Description
        {
            get
            {
                return TryGet(d => d.Description);
            }
            set
            {
                TrySet(d => d.Description = value);
            }
        }
        public override Guid Id
        {
            get
            {
                return TryGet(d => d.Id);
            }
        }
        public bool Connected
        {
            get
            {
                return device != null;
            }
        }

        public override IEnumerable<Service> Services
        {
            get
            {
                return TryGet(d => d.Services);
            }
        }

        private Device device;

        public DeviceProxy(DeviceInfo info)
        {
            Info = info;
        }

        private void Connect()
        {
            if (Connected)
                return;

            /*foreach (LinkInfo[] path in Info.Paths)
            {
                NeighborInfo first = path[0] as NeighborInfo;
                
                DeviceHub deviceHub = first.Destination.Device as DeviceHub;
                device = deviceHub.FindDevice(path.Skip(1).Select(l => l.Destination.Id).ToArray());

                if (device != null)
                    return;
            }*/
        }

        private T TryGet<T>(Func<Device, T> getter)
        {
            Connect();

            try
            {
                return getter(device);
            }
            catch (Exception e)
            {
                device = null;

                Connect();
                return getter(device);
            }
        }
        private void TrySet(Action<Device> setter)
        {
            Connect();

            try
            {
                setter(device);
            }
            catch (Exception e)
            {
                device = null;

                Connect();
                setter(device);
            }
        }
    }
}